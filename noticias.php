<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="">
	<meta name="keywords" content="" />
    <meta name="author" content="dhsign">
	<meta name="robots" content="index, follow" />
	<meta name="revisit-after" content="3 days" />
	
    <title>Notícias | Enginstrel Engematic</title>

	<link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php 
$menu="noticias";
$link_es = "/es/noticias";
$link_en = "/en/news";
include("include/header.php");
include("include/conexao.php");
  ?>

      <!-- Page Title -->
      <section class="page-title noticia_topo">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Notícias</h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="index.php">Home</a></li>
				<li class="active">Notícias e Eventos</li>
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Blog -->
      <section class="main-body top-a">
	    <div class="container">
          <div class="row">
		    <div class="col-sm-12 col-md-12">
              <div itemtype="http://schema.org/Blog" itemscope="" class="blog">
				<div class="row clearfix">
				  
					<?php 
			        $sql_noticias="select titulo,resumo,slug,imagem from noticia where ativo = 'SIM' and idioma='PT' order by dt_hr desc";
			        $result_noticias=mysqli_query($con,$sql_noticias);
			        while($row_noticias = mysqli_fetch_array($result_noticias)){
			        ?>
				  <div class="col-sm-3">
				    <article itemtype="http://schema.org/BlogPosting" itemscope="" itemprop="blogPost" class="item ">
					  <div class="">
					    <h2 itemprop="name" style="font-size: 18px;">
					      <a itemprop="url" href="noticias/<?=$row_noticias['slug']?>"><?=$row_noticias['titulo']?></a>
					    </h2>
					  </div>
                      <a href="noticias/<?=$row_noticias['slug']?>">
                      <div style="background: url(&quot;admin/public/img/<?=$row_noticias['imagem']?>&quot;) center center;background-size: cover;width: 100%;height: 116px;background-repeat: no-repeat;margin-bottom: 10px;" class="borda-evento-foto"> </div>
                      </a>
                      <p style="height: 120px;"><?=$row_noticias['resumo']?></p>
                      <p class="readmore">
	                    <a itemprop="url" href="noticias/<?=$row_noticias['slug']?>" class="btn btn-default">Leia mais ...	</a>
                      </p>
                      <hr>
				    </article>
				  </div>
				  <?php } ?>

				  
				</div><!-- end row -->
<!--
				<div class="pagination-wrapper">
				  <p class="counter"> Page 1 of 2 </p>
				  <ul class="pagination">
					<li class="active"><a>1</a></li>
					<li><a title="2" href="blog2.html" class="">2</a></li>
					<li><a title="»" href="blog2.html" class="next">»</a></li>
					<li><a title="End" href="blog2.html" class="">End</a></li>
			      </ul>
				</div>-->
	          </div>
            </div>
		  </div>
	    </div>
	  </section>
	  <!-- /Blog -->
<?php include("include/footer.php"); ?>


    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
