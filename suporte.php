<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="">
	<meta name="keywords" content="" />
	
    <title>Suporte | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />
	<link href="assets/css/quotes.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php 
$menu="suporte";
$link_en = "/en/support";
$link_es = "/es/suporte";
include("include/header.php");
include("include/conexao.php");
 ?>


      <!-- Page Title -->
      <section class="page-title suporte_topo">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Serviço Especializado</h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="index.php">Home</a></li>
				<li class="active">Suporte</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
          <?php 
          $sql_psuporte="select * from psuporte where idioma='PT'";
          $result_psuporte=mysqli_query($con,$sql_psuporte);
          $i = 0;
          while($row_psuporte = mysqli_fetch_array($result_psuporte)){ 
            if(++$i>1)
              echo '<div><br><br><hr></div>';
          ?>
          <div class="module title3">
            <h3 class="module-title"><?=$row_psuporte['titulo']?></h3>
            <div class="module-content">
                    <br>
            <div class="uk-grid uk-margin-bottom">
                          <?php 
                          $sql_fotos = "select * from foto_psuporte where caminho<>'' and id_psuporte='".$row_psuporte['id']."'";
                          $result_fotos=mysqli_query($con,$sql_fotos);
                          while($row_fotos = mysqli_fetch_array($result_fotos)){
                          ?>
                      <div class="uk-width-medium-1-3">
                        <a href="admin/public/img/<?=$row_fotos['caminho']?>" data-uk-lightbox="{group:'<?=$row_psuporte['id']?>'}" title="<?=$row_psuporte['titulo']?>">
                            <img src="admin/public/img/<?=$row_fotos['caminho']?>">
                        </a>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
          </div>
          <?php } ?>
			</div>
      <div class="col-sm-2 col-md-2"></div>
      <div class="col-sm-8 col-md-8"><center><p>&nbsp;</p><p>&nbsp;</p>
        <h3>Solicite nosso suporte</h3>            
              <form class="form-horizontal" id="form_suporte" method="post" autocomplete="off">
                <fieldset>
            <legend>Temos uma equipe preparada para atender as suas necessidades.</legend>         
                  <div class="form-group">
                    <div class="col-md-6">
                      <input class="form-control" placeholder="Nome" name="nome" id="nome" type="text">
                    </div>

                    <div class="col-md-6">
                      <input class="form-control" placeholder="Email" name="email" id="email" type="email">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-6">
                      <input class="form-control" placeholder="Empresa" name="empresa" id="empresa" type="text">
                    </div>

                    <div class="col-md-6">
                      <input class="form-control" placeholder="Telefone" name="telefone" id="telefone" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12">
                      <textarea class="form-control" placeholder="Mensagem" name="mensagem" id="mensagem"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label label-left col-xs-3"></label>     
                    <div class="col-md-12">
            <button class="btn btn-primary validate" type="submit" aria-invalid="false" id="enviar">Enviar</button>
            <div class="mensagem_retorno"></div>
                    </div>
                  </div>
                </fieldset>       
              </form>         
            </center></div>
            <div class="col-sm-2 col-md-2"></div>

		  </div>
		</div>
	  </section>	  
	  <!-- /Top A -->

	  
<?php include("include/footer.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/formatter.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dotanimation.min.js"></script>
<script>
    $(document).ready(function() {
      $('#form_suporte').validate({
        rules: {
          mensagem:         {required: true,minlength: 3},
          empresa:         {required: true},
          nome:         {required: true,minlength: 3},
          email:         {required: true,email: true},
          telefone:         {required: true,minlength: 14},       
        },
        // Define as mensagens de erro para cada regra
        messages:{
        },
        highlight: function(element) {
            $(element).closest('input').removeClass('success').addClass('error');
            $(element).closest('select').removeClass('success').addClass('error');
            $(element).closest('textarea').removeClass('success').addClass('error');
        },
        success: function(element) {
            element
            .addClass('valid')
            .closest('input').removeClass('error').addClass('success');
            
            element
            .addClass('valid')
            .closest('select').removeClass('error').addClass('success');
            
            element
            .addClass('valid')
            .closest('textarea').removeClass('error').addClass('success');          
        },
        submitHandler: function( form ){
            $.ajax({
            url: 'suporte_envia.php',
            type: 'POST',
            data: $('#form_suporte').serialize(),
            beforeSend: function(){
              $('#enviar').html("Enviando Aguarde").dotAnimation({
                speed: 150,
                dotElement: '.',
                numDots: 5
              });
              console.log("Enviando...");
            },
            success: function(resposta){
              console.log(resposta);
                $('#enviar').trigger('stopDotAnimation');
                $('#enviar').html('Enviar');
                $('.mensagem_retorno').html(resposta);
                $('.mensagem_retorno').show();
                setTimeout("$('#form_suporte').get(0).reset();", 1500);
                setTimeout("$('.mensagem_retorno').hide();",8500);  
            },
            error : function(jqXHR, textStatus, errorThrown){
              console.log("jqXHR: "+jqXHR.status);
              console.log("textStatus: "+textStatus);
              console.log("errorThrown: "+errorThrown);
            }
            });
            return false;
        },  
      });
    });
    </script>
<script>
      var formatted = new Formatter(document.getElementById('telefone'), {
          'patterns': [
              {'^[0-9][0-9]9': '({{99}}) {{99999}}-{{9999}}'},
              {'*': '({{99}}) {{9999}}-{{9999}}'},
          ],
          'persistent': false
      });
      </script>
  </body>
</html>
