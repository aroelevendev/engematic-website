<?php
$con=mysqli_connect("localhost","engemati_db","@roEleven3ngematic","engemati_db");
mysqli_set_charset($con,'utf8'); 

date_default_timezone_set('America/Sao_Paulo');
setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );

function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
}
  
function sanitize($input,$conn) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val,$conn);
        }
    }
    else {
        // if (get_magic_quotes_gpc()) {
        //     $input = stripslashes($input);
        // }
        $input  = cleanInput($input);
        $output = mysqli_real_escape_string($conn,$input);
    }
    return $output;
}

$_POST = sanitize($_POST,$con);
$_GET  = sanitize($_GET,$con);

function utf8_encode_deep(&$input) {
	if (is_string($input)) {
		if (preg_match('!!u', $input))
		{
		   //$input = utf8_decode($input);
		}
		else 
		{
		   $input = utf8_encode($input);
		}

	} else if (is_array($input)) {
		foreach ($input as &$value) {
			utf8_encode_deep($value);
		}
		
		unset($value);
	} else if (is_object($input)) {
		$vars = array_keys(get_object_vars($input));
		
		foreach ($vars as $var) {
			utf8_encode_deep($input->$var);
		}
	}
}

?>