<?php
$con=mysqli_connect("localhost","engemati_db","@roEleven3ngematic","engemati_db");
$sql_certificado = "SELECT arquivo FROM certificado WHERE dt_hr = (SELECT max(dt_hr) FROM certificado WHERE ativo=1)";
$result_certificado=mysqli_query($con,$sql_certificado);
$row_certificado = mysqli_fetch_array($result_certificado); 
 ?>
 
<!-- Top Bar -->	
<div class="menu_fixo">
	<section class="top-bar" id="top-bar">
	<div class="container">
	  <div class="row">

			<div class="col-sm-12 col-md-12 top1" align="right">

			<div  style="position: relative; display: inline-block;">
            	<ul class="social__icones">
                	<li>
                        <a href="https://www.facebook.com/EnginstrelEngematic" target="_blank">
                           <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/enginstrel-engematic-instrumenta%C3%A7%C3%A3o-ltda." target="_blank">
                           <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/enginstrelengematic" target="_blank">
                            <i class="fa fa-youtube-play"></i>
                        </a>
                    </li>
            	</ul>
            </div>
               
            <a class="logo_iso" href="http://www.engematic.com.br/certificado/<?= $row_certificado['arquivo'] ?>" target="_blank" >
                <img src="images/logo_iso9001.jpg" alt="" style="width: 40px; height: auto;">                    
            </a>              

	    	<div  style="position: relative; display: inline-block; transform: translateY(-14%);">
		    	<ul class="contact-info" style="float: none;">
				    <li>
					  <i class="uk-icon-phone"></i>  (15) 3228-3686 / (15) 3228-4165
					</li>
					<li>
					  <i class="uk-icon-envelope"></i> <a href="mailto:vendas@engematic.com.br">vendas@engematic.com.br</a>
					</li>
			  	</ul>
	    	</div>   	
				<?php 

					if($link_pt=="")
						$link_pt = "http://www.engematic.com.br";

					if($link_en=="")
						$link_en = "http://www.engematic.com.br/en/";

					if($link_es=="")
						$link_es = "http://www.engematic.com.br/es/";
				?>
		  	<div class="flags">
			  <a href="<?=$link_es?>"><img src="images/flag_es.png" width="20"></a>
			  <a href="<?=$link_en?>"><img src="images/flag_en.png" width="20"></a>
			  <a href="<?=$link_pt?>"><img src="images/flag_pt.png" width="20"></a>
			</div>

		</div>
	  </div>
	</div>
	</section>
	<!-- /Top Bar -->
	<!-- Sticky Menu -->	  
	<div id="header-sticky-wrapper" class="sticky-wrapper" style="height: 86px;">
		<header id="header" class="header">
		  <div class="container">
		    <div class="row" style="position: relative;">
			  <div class="col-xs-8 col-sm-3 col-md-3" id="logo" >
				<a href="index.php" class="logo">
				  <h1>
				    <img src="images/presets/preset1/logo.jpg" class="default-logo">
                    <img width="190" height="35" alt="Engematic" src="images/presets/preset1/logo.jpg" class="retina-logo">				
				  </h1>
				</a>
			  </div>
			  <div class="col-xs-4 col-sm-9 col-md-9" id="menu" style="padding: 1px;">
                  <div class="hidden-md hidden-lg"><a href="#" id="offcanvas-toggler"><div class="uk-icon-bars" style="margin-right:20px"></div></a></div>
		        <div>
				  <ul class="megamenu-parent menu-zoom hidden-xs">
				  	<li class="menu-item <?php if($menu=="index") echo "current-item active"; ?>">
					  <a href="en/index.php"><i class="fa fa-home" aria-hidden="true" style="font-size: 30px; vertical-align: middle;"></i></a>
					</li>
		            <li class="menu-item <?php if($menu=="empresa") echo "current-item active"; ?>"><a href="en/company.php">Company</a></li>
		            <li class="menu-item <?php if($menu=="produtos") echo "current-item active"; ?>"><a href="en/products.php">Products</a></li>
		            <li class="menu-item <?php if($menu=="servicos") echo "current-item active"; ?>"><a href="en/services.php">Services</a></li>
		            <li class="menu-item <?php if($menu=="artigos-tecnicos") echo "current-item active"; ?>"><a href="en/technical-articles.php">Technical Articles</a></li>
		            <li class="menu-item <?php if($menu=="suporte") echo "current-item active"; ?>"><a href="en/support.php">Support</a></li>
		            <li class="menu-item <?php if($menu=="noticias") echo "current-item active"; ?>"><a href="en/news.php">News</a></li>
		            <li class="menu-item <?php if($menu=="cases") echo "current-item active"; ?>"><a href="en/cases.php">Cases</a></li>
		            <li class="menu-item <?php if($menu=="business-area") echo "current-item active"; ?>"><a href="en/business-area.php">Business Area</a></li>
		            <li class="menu-item <?php if($menu=="contato") echo "current-item active"; ?>"><a href="en/contact-us.php">Contact Us</a></li>
				  </ul>
				</div>				
			  </div>
			</div>
		  </div>
		</header>
	</div>
</div>
<!-- /Sticky Menu -->	
<div class="menu_fixo_margin"></div>