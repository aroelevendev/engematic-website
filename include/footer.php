         <!-- Bottom B -->

         <section class="bottom-b">

            <div class="container">

               <div class="row">

                  <div class="col-sm-3 col-md-3">

                     <div class="module title3">

                        <h3 class="module-title">Sobre nós</h3>

                        <div class="module-content">

                           <p><strong>A Enginstrel Engematic</strong> foi fundada em 17 de janeiro de 1973 e desde então veio revolucionando a<strong> indústria de instrumentação</strong> </p>

                           <!--<p>

                              <a href="#" class="uk-icon-button uk-icon-facebook"></a>

                              <a href="#" class="uk-icon-button uk-icon-google-plus"></a>

                              <a href="#" class="uk-icon-button uk-icon-youtube"></a>

                              <a href="#" class="uk-icon-button uk-icon-linkedin"></a>

                           </p>-->

                        </div>

                     </div>

                  </div>

                  <div class="col-sm-6 col-md-6">

                     <div class="module title3">

                        <h3 class="module-title">Produtos e Serviços</h3>

                        <div class="module-content">

                            <p>Nós temos uma grande variedade de <span title="Conheça nossos produtos" data-uk-tooltip=""><a href="produtos.php">produtos</a></span> trazendo as melhores soluções do mercado.</p>

                            <p>Contamos com uma equipe especializada para prestar <span title="Conheça nossos serviços" data-uk-tooltip=""><a href="servicos.php">serviços</a></span> com qualidade e rapidez.</p>

                        </div>

                     </div>

                  </div>

                  <div class="col-sm-3 col-md-3">

                     <div class="module title3">

                        <h3 class="module-title">Contato</h3>

                        <div class="module-content">

                           <p><i class="uk-icon-envelope"></i>&nbsp;&nbsp;Email: <a href="mailto:enginstrel@engematic.com.br">enginstrel@engematic.com.br</a></p>

                           <p><i class="uk-icon-phone"></i>&nbsp;&nbsp;<strong style="font-size: 20px;">(15) 3228-3686</strong></p>

                           <p><i class="uk-icon-building-o"></i>&nbsp;&nbsp; RUA PILAR DO SUL, 43 à 63<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JARDIM  LEOCÁDIA<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SOROCABA, SP.</p>

                        </div>

                     </div>

                  </div>

               </div>

            </div>

         </section>

         <!-- /Bottom B -->

         <!-- Footer Top -->

         <section class="footer-top">

         </section>

         <!-- /Footer Top -->   

         <!-- Footer -->

         <footer class="footer">

            <div class="container">

               <div class="row">

                  <div class="col-sm-6 col-md-6" id="footer1">

                     <span class="copyright"> &copy; <?=date('Y')?> Enginstrel Engematic. Todos os direitos reservados.</span>

                  </div>

                  <div class="col-sm-6 col-md-6 footer2" id="footer2">

                     <!--<div class="module ">

                        <div class="module-content">

                           <a href="#" class="uk-icon-button uk-icon-chevron-up" data-uk-smooth-scroll=""></a>

                        </div>

                     </div>-->

                  </div>

               </div>

            </div>

         </footer>

         <!-- Footer -->

<!-- Off Canvas Menu -->

<div class="offcanvas-menu">

<a class="close-offcanvas" href="#"><i class="uk-icon-remove"></i></a>

<div class="offcanvas-inner">

 <div class="module">

   <h3 class="module-title">Menu</h3>

<div class="module-content">

  <ul class="nav menu">

<li class="menu-item current-item active">

<a href="index.php"><i class="fa fa-home" aria-hidden="true" style="font-size: 30px; vertical-align: middle;"></i></a>

</li>

<li class="menu-item"><a href="/empresa">Empresa</a></li>
<li class="menu-item"><a href="/produtos">Produtos</a></li>
<li class="menu-item"><a href="/servicos">Serviços</a></li>
<li class="menu-item"><a href="/artigos-tecnicos">Artigos Técnicos</a></li>
<li class="menu-item"><a href="/suporte">Suporte</a></li>
<li class="menu-item"><a href="/noticias">Notícias</a></li>
<li class="menu-item"><a href="/contato">Contato</a></li>

  </ul>

   </div>

</div>

</div>

</div>     

<!-- /Off Canvas Menu -->