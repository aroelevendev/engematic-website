<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="">


	<base href="/">
    <title>Produtos | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
	  <!-- Top Bar -->	
    <?php 
    $menu="produtos";
    $link_pt = "/produtos";
	$link_es = "/es/productos";
    include("../include/header_en.php");
    include("../include/conexao.php"); 
    ?>

      <!-- Page Title -->
      <section class="page-title produto_topo">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Produtos</h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="en/">Home</a></li>
				<li class="active">Products</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
			  <div class="module title3">
			    <div class="module-content">
                    <?php
                    $sql_arquivo="select * from arquivo where idioma = 'EN' and pag_produto=1 and arquivo <> ''";
                    $result_arquivo=mysqli_query($con,$sql_arquivo);
                    $i = 0;
                    while($row_arquivo = mysqli_fetch_array($result_arquivo)){ ?>
                        <div align="center" class="bt_10">
                            <a href="/admin/public/img/<?=$row_arquivo['arquivo']?>" target="_blank"><button class="uk-button uk-button-small" type="button" style="padding: 5px 20px;width: 100%;max-width: 300px;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;<?=$row_arquivo['descricao']?></button></a>
                        </div>
                    <?php } ?>

			    	<?php
		                $sql_segmento=" SELECT * FROM segmentoproduto ORDER BY pos asc";
		                $result_segmento=mysqli_query($con,$sql_segmento);
		                $i = 0;
		                while($row_segmento = mysqli_fetch_array($result_segmento)){
		                	if(!is_float($i/3)){
								echo '<div class="uk-grid uk-grid-divider">';
							}
		                	echo '<div class="uk-width-medium-1-3 text-center">
                     <a href="en/products/'.$row_segmento['slug_en'].'">
						<div style="background: url(&quot;admin/public/img/'.$row_segmento['imagem2'].'&quot;) center bottom;background-size: contain;width: 100%;height: 230px;background-repeat: no-repeat;margin-bottom: 20px;" class="borda-evento-foto"> </div>
                      <h4>
                        <strong>'.$row_segmento['titulo_en'].'</strong>
                      </h4>
                    </a>
                    </div>';
							$i++;
							if(!is_float($i/3))
							echo '</div><hr class="uk-grid-divider">';
		                }
		            ?>

                </div>
			  </div>
			</div>
		  </div>
		</div>
	  </section>	  

	  
	  <!-- Bottom B -->
      <?php include("../include/footer_en.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
