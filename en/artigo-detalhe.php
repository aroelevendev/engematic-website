
<?php include("../include/conexao.php"); 

$sql_artigo="select * from `case` where ativo_en = 'SIM' and slug_en = '".$_GET['id']."'";
$result_artigo=mysqli_query($con,$sql_artigo);
$row_artigo = mysqli_fetch_array($result_artigo);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="/">
	<!-- Meta -->
    <meta name="description" content="<?=$row_artigo['resumo_en']?>">
	
    <title><?=$row_artigo['titulo_en']?> | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />
	<link href="assets/css/quotes.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php $menu="artigos-tecnicos";include("../include/header_en.php"); ?>

<style type="text/css">
em, b, strong{color: inherit;}
  tr:nth-child(even) {
             background-color: rgb(244, 244, 244);
            }

            tr:nth-child(1){
             background-color:rgb(231, 230, 230);
            }
            table,th,td {
            border: 1px solid #f2f2f2;
            padding: 10px;
            }
}
</style>
      <!-- Page Title -->
      <section  style="background-color: #f5f5f5;">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
              <ol class="breadcrumb" style="padding: 20px 20px 20px 0px; margin-bottom: 0px;">
				<li><a class="pathway" href="en/index.php">Home</a></li>
        <li><a class="pathway" href="en/technical-articles">Articles</a></li>
				<li class="active"><?=$row_artigo['titulo_en']?></li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-9 col-md-9">

        <div class="module title3">
          
          <div class="module-content">
                  <h3 class="module-title" style="display: inline-block;"><?=$row_artigo['titulo_en']?></h3>
                  <br><br>
          <?=$row_artigo['descricao_en']?>
                </div>
        </div>


			</div>
      <div class="col-sm-3 col-md-3 right">

      	<?php
		if($row_artigo['imagem'] <> ""){
		?>
      	<a href="admin/public/img/<?=$row_artigo['imagem']?>" target="_blank" class="uk-button uk-button-primary"><i class="fa fa-download" aria-hidden="true"></i>
 Download em PDF</a>
        <?php } ?>


        <div class="module _menu">
        <h3>Outros Artigos</h3>
        <ul class="nav menu">
        <?php 
        $sql_outros="select * from `case` where ativo_en = 'SIM' and slug_en <>'".$_GET['id']."'";
        $result_outros=mysqli_query($con,$sql_outros);
        $i = 0;
        while($row_outros = mysqli_fetch_array($result_outros)){
        ?>
        <li><a href="en/technical-articles/<?=$row_outros['slug_en']?>"><?=$row_outros['titulo_en']?></a></li>
        <?php } ?>
        </ul>
        </div>
        </div>
		  </div>
		</div>
	  </section>	  
	  <!-- /Top A -->

	  
<?php include("../include/footer_en.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
