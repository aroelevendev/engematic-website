<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="">
	<meta name="keywords" content="" />
    <meta name="author" content="dhsign">
	<meta name="robots" content="index, follow" />
	<meta name="revisit-after" content="3 days" />
	
    <title>Artigos Técnicos | Enginstrel Engematic</title>

	<link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php 
$menu="artigos-tecnicos";
$link_pt = "/artigos-tecnicos";
$link_es = "/es/articulos-tecnicos";
include("../include/header_en.php");
include("../include/conexao.php");
?>

      <!-- Page Title -->
      <section class="page-title artigo_topo">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Artigos Técnicos</h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="en/">Home</a></li>
				<li class="active">Articles</li>
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Blog -->
      <section class="main-body top-a">
	    <div class="container">
          <div class="row">
		    <div class="col-sm-12 col-md-12">
              <div itemscope="" class="blog">
				<div class="row clearfix">
				  <div class="col-sm-4">
				    <article itemscope="" itemprop="blogPost" class="item ">
				    <?php 
			        $sql_artigo_imagem="select caminho from foto_caseimagem where id = 1";
			        $result_artigo_imagem=mysqli_query($con,$sql_artigo_imagem);
			        $row_artigo_imagem = mysqli_fetch_array($result_artigo_imagem);
			        ?>
					  <img src="admin/public/img/<?=$row_artigo_imagem['caminho']?>" class="img-responsive">
				    </article>
				  </div>

				  <div class="col-sm-8">
				    <?php 
			        $sql_artigo="select titulo_en,resumo_en,slug_en from `case` where ativo_en = 'SIM'";
			        $result_artigo=mysqli_query($con,$sql_artigo);
			        while($row_artigo = mysqli_fetch_array($result_artigo)){
			        ?>
				    <article itemscope="" itemprop="blogPost" class="item " style="margin-bottom: 0px;">
					  <div class="">
					    <h2 itemprop="name">
					      <a itemprop="url" href="en/technical-articles/<?=$row_artigo['slug_en']?>"><?=$row_artigo['titulo_en']?></a>
					    </h2>
					  </div>
                      <p><?=$row_artigo['resumo_en']?></p>
                      <p class="readmore">
	                    <a itemprop="url" href="en/technical-articles/<?=$row_artigo['slug_en']?>" class="btn btn-default">Read more ...</a>
                      </p>
                      <hr style="margin-top: 30px;">
				    </article>
				    <?php } ?>

				  </div>


	          </div>
            </div>
		  </div>
	    </div>
	  </section>
	  <!-- /Blog -->
<?php include("../include/footer_en.php"); ?>


    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
