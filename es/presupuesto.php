<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="/">
	<!-- Meta -->
    <meta name="description" content="">
	<meta name="keywords" content="" />
    <meta name="author" content="dhsign">
	<meta name="robots" content="index, follow" />
	<meta name="revisit-after" content="3 days" />
	
    <title>Orçamento | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script>
	  "use strict";
	  var myCenter=new google.maps.LatLng(-23.472885,-47.441749);
      function initialize() {
        var mapCanvas = document.getElementById('mapCanvas');
        var mapOptions = {
          center: myCenter,
          zoom: 14,
		  scrollwheel: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
        //var iconBase = 'https://maps.google.com/mapfiles/kml/pushpin/';
		var iconBase = 'images/elements/';
        var marker = new google.maps.Marker({
          position: mapOptions.center,
          map: map,
		  icon: iconBase + 'helmet.png'
        });	
        marker.setMap(map);
        var infowindow = new google.maps.InfoWindow({
          content:"<div id='legend' style='color: #000 !important; text-align: center;'>Enginstrel Engematic</div>"
        });
        infowindow.open(map,marker);		
      }
      google.maps.event.addDomListener(window, 'load', initialize);  
    </script>	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
	<?php 
  $menu="contato";
  $link_pt = "/orcamento";
  $link_en = "/en/quotation";
  include("../include/header_es.php");
  include("../include/conexao.php"); ?>

      <!-- Page Title -->
      <section class="page-title orcamento_topo">
    <div class="container">   
        <div class="row">
        <div class="col-sm-12 col-md-12 title">
        <h2>Orçamento</h2>
              <ol class="breadcrumb">
        <li><a class="pathway" href="en/">Home</a></li>
        <li class="active">Pedido de Orçamento</li>
          </ol>
      </div>
      </div>
    </div>
    </section>
	  <!-- /Page Title -->
	  
	  <!-- Contact Us -->
      <section class="main-body top-a">
	    <div class="container">
          <div class="row">
		    <div class="col-sm-3 col-md-3" id="left">
			  <div class="module title6">
			    <div class="module-content">
                  <h4><strong>Enginstrel Engematic Instrumentação Ltda</strong></h4>
                  <hr>
                  <i class="pe-7s-map-marker pe-3x uk-text-primary"></i> <h4>Rua Pilar Do Sul, 43 À 63  -  Jardim  Leocádia - Sorocaba - S.P.</h4>
                  <hr>
                  <i class="pe-7s-call pe-3x uk-text-primary"></i> <h4>(15) 3228.4165</h4>
                  <hr>
                  <i class="pe-7s-mail pe-3x uk-text-primary"></i> <h4>contato@engistrel.com.br</h4>
                  <hr>
                </div>
			  </div>
			</div>
			<div class="col-sm-9 col-md-9">

			  <h3>Formulário para pedido de orçamento</h3>					  
              <form class="form-horizontal" method="post" id="sheepItForm">
                <fieldset>
                  <div class="form-group">
                    <div> 
              <div class="col-md-12 col-sm-12 no-padding-left-sm">
                                  <div id="sheepItForm_add" style="border: 1px solid rgb(227, 227, 227); text-align: center; padding: 6px; max-width: 225px; margin-bottom: 18px; cursor: pointer;"><a><span><i class="fa fa-plus" style="font-size: 26px; vertical-align: middle; margin-right: 5px;"></i>
 Adicionar outros Produtos</span></a></div>
                            </div>
              <div class="col-md-12">
                              <!-- Form template-->
                                 
                                <div id="sheepItForm_template" style="clear: both;">
                                  
                                  <div class="col-md-9 col-sm-12">
                                    <div class="form-group">
                                    <select id="sheepItForm_#index#_phone" name="produto[#index#]" style="margin-bottom:10px;">
                                    <option value="" selected="selected">-- Escolha o Produto --</option>
                                    <?php
                                      $sql_segmento = "select * from segmentoproduto where ativo_es = 'SIM'";
                                      $i=0;
                                      $result_segmento=mysqli_query($con,$sql_segmento);
                                      while ($row_segmento = mysqli_fetch_array($result_segmento)) {
                                        echo '<optgroup label="'.$row_segmento['titulo_es'].'">';
                                                      ?>
                                                      
                                                      <?php 
                                      $sql_produtos = "select p.titulo_es,p.slug_es from produto p
                                              join segmentoproduto sp on find_in_set(sp.id,p.segmento) <> 0
                                              where sp.slug_es = '".$row_segmento['slug_es']."' and p.ativo_es='SIM'";
                                      $result_produtos=mysqli_query($con,$sql_produtos);
                                      
                                      while ($row_produtos = mysqli_fetch_array($result_produtos)) {
                                        if($_GET['id']==$row_produtos['slug_es'])
                                          $selected = "selected";
                                        else
                                          $selected = "";
                                        echo '<option value="'.$row_produtos['titulo_es'].'" '.$selected.'>'.$row_produtos['titulo_es'].'</option>';
                                      }
                                        echo '</optgroup>';
                                      }
                                      ?>
                                    </select>
                                    </div>
                                  </div>

                                  <div class="col-md-3 col-sm-12 no-padding-left-sm">
                                    <div class="form-group">
                                    <a id="sheepItForm_remove_current">
                                          <img class="delete" src="images/cross.png" width="16" height="16" border="0" style="vertical-align: middle; margin-top: 13px; margin-left: 10px;">
                                        </a>
                                    </div>
                                  </div>
                                  

                                </div>
                                <!-- /Form template-->
                                 
                                <!-- No forms template -->
                                <div id="sheepItForm_noforms_template">Sem nenhum produto</div>
                                <!-- /No forms template-->
                            </div>
                        </div>
                    <div class="col-md-6">
                      <input class="form-control" id="nome" placeholder="Nome" name="nome" type="text">
                    </div>

                    <div class="col-md-6">
                      <input class="form-control" id="email" placeholder="Email" name="email" type="email">
                    </div>
                  </div>
                  <div class="form-group">
                  	<div class="col-md-6">
                      <input class="form-control" id="telefone" placeholder="Telefone" name="telefone" type="text">
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" id="cidade" placeholder="Cidade" name="cidade" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                  	<div class="col-md-6">
                      <input class="form-control" id="cargo" placeholder="Cargo" name="cargo" type="text">
                    </div>
                    <div class="col-md-6">
                      <input class="form-control" id="empresa" placeholder="Empresa" name="empresa" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-12">
                      <textarea class="form-control" id="mensagem" placeholder="Mensagem (opcional)" name="mensagem"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label label-left col-xs-3"></label>     
                    <div class="col-md-12">
            <button class="btn btn-primary validate" type="submit" aria-invalid="false" id="enviar">Enviar</button>
            <div class="mensagem_retorno"></div>
                    </div>
                  </div>
                </fieldset>       
              </form>         
            </div>
		  </div>
	    </div>
	  </section>
	  <!-- /Contact Us -->

	  
<?php include("../include/footer_es.php"); ?>
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/formatter.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.dotanimation.min.js"></script>
    <script src="assets/js/jquery.sheepItPlugin-1.1.1.min.js"></script>
    <script>
    $(document).ready(function() {
      var sheepItForm = $('#sheepItForm').sheepIt({
            separator: '',
            allowRemoveLast: true,
            allowRemoveCurrent: true,
            allowRemoveAll: true,
            allowAdd: true,
            allowAddN: true,
            minFormsCount: 1,
            iniFormsCount: 1
        });

      $('#sheepItForm').validate({
        rules: {
          sheepItForm_0_phone: {required: true},
          empresa:         {required: true},
          cidade:         {required: true},
          cargo:         {required: true},
          nome:         {required: true,minlength: 3},
          email:         {required: true,email: true},
          telefone:         {required: true,minlength: 14},       
        },
        // Define as mensagens de erro para cada regra
        messages:{
        },
        highlight: function(element) {
            $(element).closest('input').removeClass('success').addClass('error');
            $(element).closest('select').removeClass('success').addClass('error');
            $(element).closest('textarea').removeClass('success').addClass('error');
        },
        success: function(element) {
            element
            .addClass('valid')
            .closest('input').removeClass('error').addClass('success');
            
            element
            .addClass('valid')
            .closest('select').removeClass('error').addClass('success');
            
            element
            .addClass('valid')
            .closest('textarea').removeClass('error').addClass('success');          
        },
        submitHandler: function( form ){
            $.ajax({
            url: 'orcamento_envia.php',
            type: 'POST',
            data: $('#sheepItForm').serialize(),
            beforeSend: function(){
              $('#enviar').html("Enviando Aguarde").dotAnimation({
                speed: 150,
                dotElement: '.',
                numDots: 5
              });
              console.log("Enviando...");
            },
            success: function(resposta){
              console.log(resposta);
                $('#enviar').trigger('stopDotAnimation');
                $('#enviar').html('Enviar');
                $('.mensagem_retorno').html(resposta);
                $('.mensagem_retorno').show();
                setTimeout("$('#sheepItForm').get(0).reset();", 1500);
                setTimeout("$('.mensagem_retorno').hide();",8500);  
            },
            error : function(jqXHR, textStatus, errorThrown){
              console.log("jqXHR: "+jqXHR.status);
              console.log("textStatus: "+textStatus);
              console.log("errorThrown: "+errorThrown);
            }
            });
            return false;
        },  
      });
    });
    </script>
      <script>
      var formatted = new Formatter(document.getElementById('telefone'), {
          'patterns': [
              {'^[0-9][0-9]9': '({{99}}) {{99999}}-{{9999}}'},
              {'*': '({{99}}) {{9999}}-{{9999}}'},
          ],
          'persistent': false
      });
      </script>
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
