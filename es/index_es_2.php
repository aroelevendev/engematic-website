<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Meta -->
      <meta name="description" content="ENGINSTREL ENGEMATIC INSTRUMENTAÇÃO">

      <title>Enginstrel Engematic</title>
      <!-- Styles -->
      <!-- Uikit CSS -->
      <link href="assets/css/uikit.min.css" rel="stylesheet">
      <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
      <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
      <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
      <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
      <!-- Bootstrap core CSS -->
      <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
      <!-- Animate CSS -->
      <link href="assets/css/animate.css" rel="stylesheet" />
      <!-- Sprocket Strips CSS -->
      <link href="assets/css/product.css" rel="stylesheet" />
      <link href="assets/css/strips.css" rel="stylesheet" />
      <link href="assets/css/quotes.css" rel="stylesheet" />
      <!-- Font Awesome --> 
      <link href="assets/css/font-awesome.min.css" rel="stylesheet">
      <!-- Pe-icon-7-stroke Fonts --> 
      <link href="assets/css/helper.css" rel="stylesheet">
      <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">
      <!-- Template CSS -->
      <link href="assets/css/template.css" rel="stylesheet">
      <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">
      <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->  
   </head>
   <body>
      <!-- Wrap all page content -->
      <div class="body-innerwrapper" id="page-top">
         <?php $menu="index";
         $link_en = "/en/";
         $link_pt = "/";
         include("../include/header_2.php");
         include("../include/conexao.php");
         function addhttp($url) {
             if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
                 $url = "http://" . $url;
             }
             return $url;
         }
         ?>
         <!-- Slider -->
         <section id="ukSlider" class="uk-slider-section">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 padding-0">
                     <div class="uk-slidenav-position" data-uk-slideshow="{animation: 'swipe', autoplay:true}">
                        <ul class="uk-slideshow uk-overlay-active">
                           <?php
                            $sql_slider="select * from banner where idioma='ES' and ativo='SIM' order by rand()";
                            $result_slider=mysqli_query($con,$sql_slider);
                            $i = 0;
                            while($row_slider = mysqli_fetch_array($result_slider)){
                              if(++$i==1)
                                 $ativo1 = "uk-active";
                              else
                                 $ativo1 = "";

                              if($row_slider['link']=="")
                                 $img = '<img width="1550" height="400" alt="" src="admin/public/img/'.$row_slider['imagem'].'">';
                              else
                                 $img = '<a href="'.addhttp($row_slider['link']).'"><img width="1550" height="400" alt="" src="admin/public/img/'.$row_slider['imagem'].'"></a>';
                              
                              echo '<li aria-hidden="false" class="'.$ativo1.'" style="animation-duration: 500ms; width: 100%; height: auto;">'.$img.'</li>';
                           }
                           ?>
                        </ul>
                        <a data-uk-slideshow-item="previous" class="uk-slidenav  uk-slidenav-previous uk-hidden-touch" href="#"></a>
                        <a data-uk-slideshow-item="next" class="uk-slidenav  uk-slidenav-next uk-hidden-touch" href="#"></a>        
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Slider -->
         <!-- Main Services -->   
         <section class="top-a">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 col-md-12">
                     <div class="module title1">
                        <h3 class="module-title">Services</h3>
                        <div class="module-content">
                           <div class="sprocket-strips">
                              <ul data-strips-items="" class="sprocket-strips-container cols-3">
                           <?php 
                            $sql_bloco="select * from bloco where idioma='ES' order by rand()";
                            $result_bloco=mysqli_query($con,$sql_bloco);
                            $i = 0;
                            while($row_bloco = mysqli_fetch_array($result_bloco)){
                           ?>
                                 <li>
                                    <div style="background-image: url('admin/public/img/<?=$row_bloco['imagem']?>');" class="sprocket-strips-item">
                                       <div class="sprocket-strips-content">
                                          <h4 class="sprocket-strips-title">
                                             <a href="#"><?=$row_bloco['titulo']?></a>
                                          </h4>
                                          <span class="sprocket-strips-text"><?=$row_bloco['descricao']?></span>
                                          <?php if($row_bloco['link']<>""){ ?><a class="readon" href="<?=addhttp($row_bloco['link'])?>"></a><?php } ?>
                                       </div>
                                    </div>
                                 </li>
                           <?php } ?>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Main Services -->
                  <!-- Latest Construction News -->    
         <section class="main-body position-a">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 col-md-12">
                     <div class="module title1">
                        <div class="module-content sprocket-strips-s">
                           <div data-uk-slideset="{small: 1, medium: 2, large: 3, autoplay: 1, autoplayInterval: 5000}">
                              <ul class="uk-grid uk-slideset">
                                 <?php
                                     $sql_produtos="select fp.caminho,c.titulo_es as segmento_nome,c.slug_es as segmento_slug,p.titulo_es as produto_nome,p.slug_es as produto_slug from produto p join segmentoproduto c on c.id = p.segmento join foto_produto fp on fp.id_produto = p.id where p.ativo_es = 'SIM' and fp.caminho<>'' and p.titulo_es<>'' group by p.id order by p.pos";
                                     $result_produtos=mysqli_query($con,$sql_produtos);
                                     while($row_produtos = mysqli_fetch_array($result_produtos)){
                                       echo '<li class="sprocket-strips-s-block" style="opacity: 1;">
                                                <div class="sprocket-strips-s-item">
                                                   <a href="es/productos/'.$row_produtos['segmento_slug'].'/'.$row_produtos['produto_slug'].'">
                                                      <div style="background:rgba(236, 236, 236, 0.0) url(&quot;admin/public/img/'.$row_produtos['caminho'].'&quot;) center center;background-size: contain;width: 100%;height: 160px;background-repeat: no-repeat;" class="borda-evento-foto"> </div>
                                                   </a>
                                                   <br>
                                                   <div class="sprocket-strips-s-content">
                                                      <h4 class="sprocket-strips-s-title">
                                                         <a href="es/productos/'.$row_produtos['segmento_slug'].'/'.$row_produtos['produto_slug'].'">'.$row_produtos['produto_nome'].'</a>
                                                      </h4>
                                                      <span class="sprocket-strips-s-text">'.$row_produtos['segmento_nome'].'</span>
                                                   </div>
                                                </div>
                                             </li>';
                                    }
                                 ?>
                                 


                              </ul>
                              <div class="uk-grid">
                                 <div class="uk-width-small-1-5 uk-push-2-5 uk-flex uk-flex-center" >
                                    <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>             
                                    <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>             
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Latest Construction News -->
         <!-- Portfolio: Recent Projects 
         <section class="main-body">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 col-md-12">
                     <article itemtype="http://schema.org/Article" itemscope="" class="item item-page">
                        <meta content="en-GB" itemprop="inLanguage">
                        <div itemprop="articleBody">
                           <div class="module title1">
                              <h3 class="module-title">Produtos</h3>
                           </div>
                           <hr>
                           <ul class="uk-subnav uk-subnav-pill uk-flex-center subnav-project" id="project">
                              <li data-uk-filter="" class="project-filter uk-active">Todos</li>
                              <li data-uk-filter="medidor" class="project-filter">Medidor de Vazão</li>
                              <li data-uk-filter="transmissor" class="project-filter">Transmissor de Pressão</li>
                              <li data-uk-filter="conversor" class="project-filter">Conversor</li>
                              <li data-uk-filter="analisador" class="project-filter">Analisador</li>
                              <li data-uk-filter="posicionador" class="project-filter">Posicionador</li>
                              <li data-uk-filter="valvula" class="project-filter">Válvula</li>
                           </ul>
                           <div data-uk-grid="{gutter: ' 20',controls: '#project'}" class="uk-grid-width-1-1 uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-3" style="margin-left: -10px; margin-right: -10px; margin-bottom: 25px;">
                              <div data-uk-filter="medidor" data-grid-prepared="true" class="project-box" style="top: 0px; left: 0px;" aria-hidden="false">
                                 <div class="uk-panel project-member">
                                    <figure class="uk-overlay uk-overlay-hover project-member-padding ">
                                       <img alt="VDT com placa de orifício" src="images/elements/portfolio1.jpg">
                                       <figcaption class="uk-overlay-panel uk-overlay-background">
                                          <div class="uk-text-left">
                                             <span class="project-member-tag">Medidor de Vazão</span>
                                          </div>
                                          <div class="mosaic-text uk-text-justify">Texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto.</div>
                                          <div class="uk-text-left">
                                             <a href="#" class="mosaic-read-on uk-text-left"> Saiba mais </a>
                                          </div>
                                          
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div data-uk-filter="transmissor" data-grid-prepared="true" class="project-box" style="top: 0px; left: 376.983px;" aria-hidden="false">
                                 <div class="uk-panel project-member">
                                    <figure class="uk-overlay uk-overlay-hover project-member-padding ">
                                       <img alt="VDU" src="images/elements/portfolio2.jpg">
                                       <figcaption class="uk-overlay-panel uk-overlay-background">
                                          <div class="uk-text-left">              
                                             <span class="project-member-tag">Transmissor de Pressão</span>
                                          </div>
                                          <div class="mosaic-text uk-text-justify">Texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto.</div>
                                          <div class="uk-text-left">              
                                             <a href="#" class="mosaic-read-on uk-text-left"> Saiba mais </a>
                                          </div>
                                          
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div data-uk-filter="conversor" data-grid-prepared="true" class="project-box" style="top: 0px; left: 753.966px;" aria-hidden="false">
                                 <div class="uk-panel project-member">
                                    <figure class="uk-overlay uk-overlay-hover project-member-padding ">
                                       <img alt="I/P FC600" src="images/elements/portfolio3.jpg">
                                       <figcaption class="uk-overlay-panel uk-overlay-background">
                                          <div class="uk-text-left">              
                                             <span class="project-member-tag">Conversor</span>
                                          </div>
                                          <div class="mosaic-text uk-text-justify">Texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto.</div>
                                          <div class="uk-text-left">              
                                             <a href="#" class="mosaic-read-on uk-text-left"> Saiba mais </a>
                                          </div>
                                          
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div data-uk-filter="analisador" data-grid-prepared="true" class="project-box" style="top: 264.2px; left: 0px;" aria-hidden="false">
                                 <div class="uk-panel project-member">
                                    <figure class="uk-overlay uk-overlay-hover project-member-padding ">
                                       <img alt="Analisador de Condutividade" src="images/elements/portfolio4.jpg">
                                       <figcaption class="uk-overlay-panel uk-overlay-background">
                                          <div class="uk-text-left">              
                                             <span class="project-member-tag">Analisador</span>
                                          </div>
                                          <div class="mosaic-text uk-text-justify">Texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto.</div>
                                          <div class="uk-text-left">              
                                             <a href="#" class="mosaic-read-on uk-text-left"> Saiba mais </a>
                                          </div>
                                          
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div data-uk-filter="posicionador" data-grid-prepared="true" class="project-box" style="top: 264.2px; left: 376.983px;" aria-hidden="false">
                                 <div class="uk-panel project-member">
                                    <figure class="uk-overlay uk-overlay-hover project-member-padding ">
                                       <img alt="FC900" src="images/elements/portfolio5.jpg">
                                       <figcaption class="uk-overlay-panel uk-overlay-background">
                                          <div class="uk-text-left">              
                                             <span class="project-member-tag">Posicionador</span>
                                          </div>
                                          <div class="mosaic-text uk-text-justify">Texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto.</div>
                                          <div class="uk-text-left">              
                                             <a href="#" class="mosaic-read-on uk-text-left"> Saiba mais </a>
                                          </div>
                                          
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                              <div data-uk-filter="valvula" data-grid-prepared="true" class="project-box" style="top: 264.2px; left: 753.966px;" aria-hidden="false">
                                 <div class="uk-panel project-member">
                                    <figure class="uk-overlay uk-overlay-hover project-member-padding ">
                                       <img alt="Válvula de bloquei para transmissor de pressão" src="images/elements/portfolio6.jpg">
                                       <figcaption class="uk-overlay-panel uk-overlay-background">
                                          <div class="uk-text-left">
                                             <span class="project-member-tag">Válvula</span>
                                          </div>
                                          <div class="mosaic-text uk-text-justify">Texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto.</div>
                                          <div class="uk-text-left">
                                             <a href="#" class="mosaic-read-on uk-text-left"> Saiba mais </a>
                                          </div>
                                          
                                       </figcaption>
                                    </figure>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </article>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Portfolio: Recent Projects -->   
         <!-- Ideas -->   
         <section class="parallax-section-1">
            <div class="row">
               <div class="col-sm-12 col-md-12">
                  <div class="module ">
                     <div class="module-content">
                        <div class="uk-flex uk-flex-center uk-flex-middle" data-uk-parallax="{bg: -300}" style="height: 100%; background-color: rgb(0, 51, 157); background-repeat: no-repeat; background-size: 1662px 1385px; background-position: 50% -65.835px;">
                           <div class="uk-width-large-1-1 uk-width-medium-1-1 uk-container-center" style="padding: 0px 70px;">
                              <div class="uk-grid uk-flex-middle">
                                 <div data-uk-scrollspy="{cls:'uk-animation-slide-left', delay:300}" class="uk-width-medium-1-2 uk-scrollspy-init-inview uk-scrollspy-inview">
                                 <?php 
                                  $sql_aniversario="select * from foto_aniversario where id = 1";
                                  $result_aniversario=mysqli_query($con,$sql_aniversario);
                                  $row_aniversario = mysqli_fetch_array($result_aniversario);
                                 ?>
                                    <img src="admin/public/img/<?=$row_aniversario['caminho_es']?>" style="float: right;">
                                 </div>
                                 <div data-uk-scrollspy="{cls:'uk-animation-slide-right', delay:600}" class="uk-width-medium-1-2 uk-scrollspy-init-inview uk-scrollspy-inview">
                                    <h4><strong class="uk-text-contrast">Empresa con tradición</strong></h4>
                                    <h1 style="margin-top: 0px; font-size: 45px; color: rgb(0, 183, 255); text-shadow: 3px 3px rgb(0, 20, 105);" class="uk-hidden-small">Última Tecnología</h1>
                                    <h1 style="margin-top: -5px; font-size: 45px; margin-bottom: 25px;" class="uk-text-contrast big-shadow uk-hidden-small">Calidad del soporte después</h1>
                                    <h1 style="font-size: 22px; margin-top: 0px; color: rgb(0, 183, 255); text-shadow: 3px 3px rgb(0, 20, 105);" class="uk-visible-small">Última Tecnología</h1>
                                    <h1 style="font-size: 22px; margin-top: 0px;margin-bottom: 20px;" class="uk-text-contrast big-shadow uk-visible-small">Calidad del soporte después</h1>
                                    <!--<a href="presupuesto.php" class="uk-button uk-button-success uk-button-small">hacer un presupuesto</a>-->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Ideas -->

         <!-- Latest Construction News -->    
         <section class="main-body position-a">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 col-md-12">
                     <div class="module title1">
                        <h3 class="module-title">Últimas Notícias</h3>
                           <?php 
                             $sql_noticias="select titulo,resumo,slug,imagem from noticia where ativo = 'SIM' and idioma='PT' order by dt_hr desc limit 4";
                             $result_noticias=mysqli_query($con,$sql_noticias);
                             while($row_noticias = mysqli_fetch_array($result_noticias)){
                           ?>
                           <div class="col-md-3">
                              <a class="uk-thumbnail" href="noticia-detalhe.php?id=<?=$row_noticias['slug']?>">
                                  <div style="background: url(&quot;admin/public/img/<?=$row_noticias['imagem']?>&quot;) center center;background-size: cover;width: 100%;height: 104px;background-repeat: no-repeat;margin-bottom: 10px;" class="borda-evento-foto"> </div>
                                  <div style="text-align: center"><h5 style="line-height: 20px;height: 60px;"><?=$row_noticias['titulo']?></h5></div>
                                  <div class="uk-thumbnail-caption" style="height: 128px;"><?=$row_noticias['resumo']?></div>
                              </a>
                           </div>
                           <?php } ?>
                           
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Latest Construction News -->
         <!-- Our Best Clients 
         <section class="position-b">
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 col-md-12">
                     <div class="module title1">
                        <h3 class="module-title">Alguns de nossos melhores clientes</h3>
                        <div class="module-content">
                           <br>
                           <div data-uk-scrollspy="{cls:'uk-animation-scale-up', delay:600}" class="uk-grid uk-scrollspy-init-inview uk-scrollspy-inview">
                              <div class="uk-width-medium-1-5">
                                 <img src="images/elements/logo1-2.png" alt="Our Clients" class="uk-align-center">
                              </div>
                              <div class="uk-width-medium-1-5">
                                 <img src="images/elements/logo2-2.png" alt="Our Clients" class="uk-align-center">
                              </div>
                              <div class="uk-width-medium-1-5">
                                 <img src="images/elements/logo3-2.png" alt="Our Clients" class="uk-align-center">
                              </div>
                              <div class="uk-width-medium-1-5">
                                 <img src="images/elements/logo4-2.png" alt="Our Clients" class="uk-align-center">
                              </div>
                              <div class="uk-width-medium-1-5">
                                 <img src="images/elements/logo5-2.png" alt="Our Clients" class="uk-align-center">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!-- /Our Best Clients -->

         
         <?php include("../include/footer_es.php"); ?>

      </div>
      <!-- Scripts placed at the end of the document so the pages load faster -->
      <!-- Jquery scripts -->
      <script src="assets/js/jquery.min.js"></script>
      <!-- Uikit scripts -->
      <script src="assets/js/uikit.min.js"></script>  
      <script src="assets/js/slideshow.min.js"></script>  
      <script src="assets/js/slideset.min.js"></script>   
      <script src="assets/js/sticky.min.js"></script>
      <script src="assets/js/tooltip.min.js"></script>  
      <script src="assets/js/parallax.min.js"></script>
      <script src="assets/js/lightbox.min.js"></script>
      <script src="assets/js/grid.min.js"></script>
      <!-- WOW scripts -->
      <script src="assets/js/wow.min.js"></script>
      <script> new WOW().init(); </script>
      <!-- Оffcanvas Мenu scripts -->
      <script src="assets/js/offcanvas-menu.js"></script>   
      <!-- Template scripts -->
      <script src="assets/js/template.js"></script>   
      <!-- Bootstrap core JavaScript -->
      <script src="bootstrap/js/bootstrap.min.js"></script>
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
   </body>
</html>