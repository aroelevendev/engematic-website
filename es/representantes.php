<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="">
	<meta name="keywords" content="" />
	
    <title>Representantes | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />
	<link href="assets/css/quotes.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php $menu="representantes";
$link_pt = "/representantes";
$link_en = "/en/representatives";
include("../include/header_es.php");
include("../include/conexao.php"); 
function addhttp($url) {
      if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
          $url = "http://" . $url;
      }
      return $url;
  }
 ?>


      <!-- Page Title -->
      <section class="page-title representate_topo">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Representantes</h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="en/">Home</a></li>
				<li class="active">Representantes</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-12 col-md-12">
			  <div class="module title3">
          <h3 class="module-title">Representantes</h3>
          <div class="module-content">
                  <br>
          <div class="uk-grid uk-margin-bottom">
            <?php 
          $sql_representates="select * from representante where idioma='ES' and ativo='SIM'";
          $result_representates=mysqli_query($con,$sql_representates);
          $i = 0;
          while($row_representates = mysqli_fetch_array($result_representates)){
            if ($i % 3 == 0 && $i>0)
               echo '<div style="text-align: center; margin-bottom: 21px;" class="uk-width-medium-3-3"><hr></div>';
            $i++;

          if($row_representates['imagem']<>"" && $row_representates['site']<>"")
             $img = '<a target="_blank" style="text-align: center;" href="'.addhttp($row_representates['site']).'"><img alt="'.$row_representates['empresa'].'" title="'.$row_representates['empresa'].'" src="admin/public/img/'.$row_representates['imagem'].'" style="max-width: 200px; text-align: center; margin: 0px auto;"></a>';
          elseif($row_representates['imagem']<>"" && $row_representates['site']=="")
             $img = '<img alt="'.$row_representates['empresa'].'" title="'.$row_representates['empresa'].'" alt="" src="admin/public/img/'.$row_representates['imagem'].'" style="max-width: 200px; text-align: center; margin: 0px auto;">';
          else
             $img = '<br>'.$row_representates['empresa'].'<br>';
          ?>
            <div style="text-align: center;" class="uk-width-medium-1-3">
                <div class="bloco_representantes">       
                  <h4><?=$row_representates['cidade']?></h4>

                  <?=$img?>
            
                  <h6><?=$row_representates['nome_representante']?></h6>
                  <p>
                  <?php
                  if($row_representates['telefone1']) echo '<a href="tel:'.$row_representates['telefone1'].'">'.$row_representates['telefone1'].'</a>';
                  if($row_representates['telefone2']) echo '<br><a href="tel:'.$row_representates['telefone2'].'">'.$row_representates['telefone2'].'</a>';
                  if($row_representates['telefone3']) echo '<br><a href="tel:'.$row_representates['telefone3'].'">'.$row_representates['telefone3'].'</a>';
                  if($row_representates['email1']) echo '<br><a href="mailto:'.$row_representates['email1'].'">'.$row_representates['email1'].'</a>';
                  if($row_representates['email2']) echo '<br><a href="mailto:'.$row_representates['email2'].'">'.$row_representates['email2'].'</a>';
                  if($row_representates['email3']) echo '<br><a href="mailto:'.$row_representates['email3'].'">'.$row_representates['email3'].'</a>';
                  if($row_representates['site']) echo '<br><a target="_blank" href="'.addhttp($row_representates['site']).'">'.addhttp($row_representates['site']).'</a>';
                  if($row_representates['extra']) echo '<br>'.nl2br($row_representates['extra']);
                  ?>
                  </p>
                </div>
            </div>
          <?php } ?>


                  </div>
                </div>
        </div>

			</div>
		  </div>
		</div>
	  </section>	  
	  <!-- /Top A -->

	  
<?php include("../include/footer_es.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
