<?php include("../include/conexao.php"); 

$sql_segmento="select fp.caminho,p.*,s.titulo_es as segmento, s.slug_es as segmento_slug, s.slug_en as segmento_slug_en, s.slug_pt as segmento_slug_pt, s.id as segmento_id, s.imagem1 from segmentoproduto s
join produto p on FIND_IN_SET(s.id, p.segmento)
join foto_produto fp on fp.id_produto = p.id
where s.slug_es = '".$_GET['id']."' and s.ativo_es = 'SIM' and fp.caminho<>'' group by p.id";
$result_segmento=mysqli_query($con,$sql_segmento);
$result_segmento2=mysqli_query($con,$sql_segmento);
$row_segmento2 = mysqli_fetch_array($result_segmento2);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="/">
	<!-- Meta -->
    <meta name="description" content="">
	
    <title><?=$row_segmento2['segmento']?> | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />
	<link href="assets/css/quotes.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php 
$link_pt = "/produtos/".$row_segmento2['segmento_slug_pt']; 
$link_en = "/en/products/".$row_segmento2['segmento_slug_en']; 
$menu="produtos";
include("../include/header_es.php");
?>

<style type="text/css">
  .page-title{background-image:url("admin/public/img/<?=$row_segmento2['imagem1']?>");}
</style>
      <!-- Page Title -->
      <section class="page-title">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2><?=$row_segmento2['segmento']?></h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="en/">Home</a></li>
        <li><a class="pathway" href="/es/productos.php">Productos</a></li>
				<li class="active"><?=$row_segmento2['segmento']?></li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-9 col-md-9">
			  <?php 
			  while($row_segmento = mysqli_fetch_array($result_segmento)){ ?>
        <div class="module title3">
          <h3 class="module-title"><a href="es/productos/<?=$row_segmento['segmento_slug']?>/<?=$row_segmento['slug_es']?>"><?=$row_segmento['titulo_es']?></a></h3>
          <div class="module-content">
                  <br>
          <div class="uk-grid uk-margin-bottom">
                    <div class="uk-width-medium-1-3">
                      <a href="es/productos/<?=$row_segmento['segmento_slug']?>/<?=$row_segmento['slug_es']?>">
                          <div style="background: url(&quot;admin/public/img/<?=$row_segmento['caminho']?>&quot;) center bottom;background-size: cover;width: 100%;height: 170px;background-repeat: no-repeat;margin-bottom: 20px;" class="borda-evento-foto"> </div>
                      </a>

                    </div>
                    <div class="uk-width-medium-2-3">
                    <p><?=$row_segmento['resumo_es']?></p>
                    </div>
                  </div>
                </div>
        </div>
        <?php } ?>

			</div>
      <div class="col-sm-3 col-md-3 right side_produto">
        <div class="module _menu">
        <h3>Outros Segmentos</h3>
        <ul class="nav menu">
        <?php 
        $sql_outros=" SELECT * FROM segmentoproduto where slug_es<>'".$_GET['id']."' ORDER BY pos asc";
        $result_outros=mysqli_query($con,$sql_outros);
        $i = 0;
        while($row_outros = mysqli_fetch_array($result_outros)){
        ?>
        <li><a href="es/productos/<?=$row_outros['slug_es']?>"><?=$row_outros['titulo_es']?></a></li>
        <?php } ?>
        </ul>
        </div>
        </div>
		  </div>
		</div>
	  </section>	  
	  <!-- /Top A -->

	  
<?php include("../include/footer_es.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
