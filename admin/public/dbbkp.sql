
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 10, 2014 at 09:51 PM
-- Server version: 5.1.69
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `u105721672_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `academico`
--

CREATE TABLE IF NOT EXISTS `academico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(500) NOT NULL,
  `url_empresa` varchar(500) NOT NULL,
  `curso` varchar(500) NOT NULL,
  `descricao` text NOT NULL,
  `data_entrada` varchar(500) NOT NULL,
  `data_saida` varchar(500) NOT NULL,
  `ativo` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `academico`
--

INSERT INTO `academico` (`id`, `empresa`, `url_empresa`, `curso`, `descricao`, `data_entrada`, `data_saida`, `ativo`) VALUES
(1, 'Microlins', 'www.microlins.com.br', 'INFO Informatica Basica', '<p>Windows Word Power Point Excel Internet</p>\r\n', '05/2005', '11/2005', 'SIM'),
(2, 'Microlins', 'http://www.microlins.com.br/', 'Montagem Manutenção De Micros, Técnico Em Informatica', '<p>Hardware Redes</p>\r\n', '02/2006', '08/2006', 'SIM'),
(3, 'Cultura Inglesa', 'http://www.culturainglesaribeirao.com.br/home/', 'FCE, Língua Inglesa', '', '03/2002', '11/2009', 'SIM'),
(4, 'Microlins', 'http://www.microlins.com.br/', 'Web Designer', '<p>HTML DreamWeaver Coreal Draw PhotoShop Flash</p>\r\n', '06/2007', '11/2007', 'SIM'),
(5, 'Centro Universitário Barão de Mauá', 'http://www.baraodemaua.br/', 'Ciência Da Computação', '<p style="text-align: justify;">O curso abrange uma variedade de T&oacute;picos, Matem&aacute;tica, Eletricidade e Eletr&ocirc;nica Digital, Redes, Infra, e o que eu mais gosto, Programa&ccedil;&atilde;o. Desenvolvemos v&aacute;rios projetos com o decorrer do curso, projetos Desktop (Java / C++), Projetos WEB (PHP), Projetos Mobile (Android) e os projetos para jogos. Particularmente o que mais gostei foi os projetos para WEB (PHP) e o para Mobile (Android).</p>\r\n', '02/2009', '11/2013', 'SIM');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `email1` varchar(255) NOT NULL,
  `email2` varchar(255) NOT NULL,
  `cc1` varchar(255) NOT NULL,
  `cc2` varchar(255) NOT NULL,
  `cco1` varchar(255) NOT NULL,
  `cco2` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email1`, `email2`, `cc1`, `cc2`, `cco1`, `cco2`) VALUES
('lincolnborgesrp@gmail.com', '', '', '', 'lincolnborgesrp@gmail.com', 'l_n_borges@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `idioma`
--

CREATE TABLE IF NOT EXISTS `idioma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `cor` varchar(255) NOT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `idioma`
--

INSERT INTO `idioma` (`id`, `nome`, `valor`, `cor`, `ativo`) VALUES
(1, 'Portugues', '95', '#000084', 'SIM'),
(2, 'Inglês', '71', '#ff0000', 'SIM'),
(3, 'Espanhol', '18', '#ff8000', 'SIM');

-- --------------------------------------------------------

--
-- Table structure for table `imagens`
--

CREATE TABLE IF NOT EXISTS `imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `posicao` varchar(100) NOT NULL,
  `tabela` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=332 ;

--
-- Dumping data for table `imagens`
--

INSERT INTO `imagens` (`id`, `codigo`, `img`, `size`, `posicao`, `tabela`) VALUES
(309, '9', '537e6939aa283.jpg', '640215', '1', 'treinamento'),
(310, '9', '537e693d7b34f.jpg', '732074', '2', 'treinamento'),
(311, '9', '537e694886b5b.jpg', '805510', '3', 'treinamento'),
(312, '11', '537f371767284.jpg', '260564', '5', 'treinamento'),
(313, '11', '537f3717c13ca.jpg', '245301', '6', 'treinamento'),
(314, '11', '537f37190c4df.jpg', '221684', '7', 'treinamento'),
(315, '11', '537f3719767fc.jpg', '201781', '8', 'treinamento'),
(316, '11', '537f37615a704.jpg', '260564', '9', 'treinamento'),
(319, '11', '537f376317433.jpg', '201781', '10', 'treinamento'),
(327, '1', '53b20005ab663.jpg', '198963', '1', 'perfil'),
(331, '2', 'LincolnCV.pdf', '128645', '1', 'arquivo');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `senha_provisoria` varchar(255) DEFAULT NULL,
  `privilegio` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;


-- --------------------------------------------------------

--
-- Table structure for table `perfil`
--

CREATE TABLE IF NOT EXISTS `perfil` (
  `nome` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `profissao` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `nascimento` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `celular` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `perfil`
--

INSERT INTO `perfil` (`nome`, `profissao`, `nascimento`, `cidade`, `estado`, `celular`, `email`, `descricao`) VALUES
('Lincoln Borges', 'PHP BackEnd Programmer', '10 de Junho de 1991', 'Ribeirão Preto', 'sp', '(16) 99281-6271', 'lincolnborgesrp@gmail.com', '<p style="text-align:center"><strong>Ola, meu nome &eacute; Lincoln Nathaniel Borges, formado em Ci&ecirc;ncia da Computa&ccedil;&atilde;o, apaixonado por qualquer mat&eacute;ria que envolva inform&aacute;tica, e atrav&eacute;s desse sistema web irei mostrar um pouco sobre mim e do meu trabalho.</strong></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `profissional`
--

CREATE TABLE IF NOT EXISTS `profissional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(500) NOT NULL,
  `url_empresa` varchar(500) NOT NULL,
  `cargo` varchar(500) NOT NULL,
  `descricao` text NOT NULL,
  `data_entrada` varchar(500) NOT NULL,
  `data_saida` varchar(500) NOT NULL,
  `ativo` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `profissional`
--

INSERT INTO `profissional` (`id`, `empresa`, `url_empresa`, `cargo`, `descricao`, `data_entrada`, `data_saida`, `ativo`) VALUES
(1, 'ARO Eleven - Marketing Digital', 'http://www.aroeleven.com.br/', 'Programador Web Junior', '<ul>\r\n	<li>Programador Back End - PHP</li>\r\n	<li>Customiza&ccedil;&atilde;o de scripts em JavaScript / JQuery</li>\r\n	<li>Customiza&ccedil;&atilde;o de CSS</li>\r\n	<li>Integra&ccedil;&atilde;o do Back End com o Front End, seja ele em Classic ASP ou PHP.</li>\r\n	<li>Cria&ccedil;&atilde;o, Instala&ccedil;&atilde;o, Migra&ccedil;&atilde;o e customiza&ccedil;&atilde;o de lojas virtuais Magento.</li>\r\n</ul>\r\n', '03/2014', 'Presente', 'SIM'),
(2, 'SMARAPD INFORMÁTICA', 'http://www.smarapd.com.br/', 'Programador', '<p style="text-align: justify;">Na SMARAPD, trabalhava no setor Laser, o setor onde faz&iacute;amos impress&atilde;o de dados vari&aacute;veis, em sua maior parte, impress&atilde;o de Multas, Holerites, Carn&ecirc;s de cobran&ccedil;a em geral (IPTU, ISS, TAXAS e etc..), receb&iacute;amos um arquivo contendo a base de dados, onde atrav&eacute;s dele program&aacute;vamos para gerar um arquivo de impress&atilde;o (.PCL) incluindo a gaveta em que o papel iria ser puxado da impressora, se a impress&atilde;o seria frente e verso dentre muitas coisas programadas antes de gerarmos o arquivo para impress&atilde;o. Uma das coisas que aprendi bastante trabalhando nessa empresa, &eacute; como lidar com o cliente, devido a grande demanda que tem-se, constantemente era exigido a aumentar o meu ritmo de trabalho sendo que devo manter a perfei&ccedil;&atilde;o (Sim, imagina se eu erra-se na programa&ccedil;&atilde;o de Santo Andr&eacute;, a empresa iria imprimir 180 mil carn&ecirc;s errados), ent&atilde;o aprendi bastante lidar com clientes de v&aacute;rios tipos, sempre conferir minunciosamente a programa&ccedil;&atilde;o feita, alem de muitas coisas na parte de programa&ccedil;&atilde;o, pois utilizamos VBscript , C , Cobol, alem da Grande ferramenta que temos para a montagem dos carn&ecirc;s / Boletos, o Printnet GMC. Aprendi tamb&eacute;m que tudo pode ser melhorado, entendo o processo que a empresa utiliza, atrav&eacute;s da programa&ccedil;&atilde;o, fiz scripts e programas que facilitam algumas checagens que antes eram feitas manualmente e otimiza&ccedil;&atilde;o onde antes demoravam mais e agora demora menos</p>\r\n', '05/2012', '06/2013', 'SIM'),
(6, 'Faro Capital Comercial Agricola Ltda', 'http://www.farocapital.com.br/', 'Estagiário', '<ul>\r\n	<li>Montagem e Manuten&ccedil;&atilde;o de Micros</li>\r\n	<li>Ajuda ao usu&aacute;rio</li>\r\n	<li>Abertura / Fechamento de chamados (help desk)</li>\r\n	<li>Programa&ccedil;&atilde;o Web (PHP / JavaScript)</li>\r\n</ul>\r\n', '12/2011', '03/2012', 'SIM'),
(7, 'Centro Universitário Barão de Mauá', 'http://www.baraodemaua.br/', 'Estagiário ', '<ul>\r\n	<li>Montagem e Manuten&ccedil;&atilde;o de Micros</li>\r\n	<li>Ajuda ao usu&aacute;rio</li>\r\n	<li>Abertura / Fechamento de chamados (help desk)</li>\r\n</ul>\r\n', '02/2010', '11/2011', 'SIM');

-- --------------------------------------------------------

--
-- Table structure for table `programacao`
--

CREATE TABLE IF NOT EXISTS `programacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `cor` varchar(255) NOT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `programacao`
--

INSERT INTO `programacao` (`id`, `nome`, `valor`, `cor`, `ativo`) VALUES
(1, 'PHP', '89', '#000084', 'SIM'),
(2, 'Magento', '77', '#b300b3', 'SIM'),
(3, 'MySQL', '70', '#00ffff', 'SIM'),
(4, 'HTML', '72', '#000000', 'SIM'),
(5, 'CSS', '62', '#b4c201', 'SIM'),
(6, 'JavaScript / JQuery', '71', '#ff8000', 'SIM'),
(7, 'Classic ASP', '46', '#400040', 'SIM'),
(8, 'VBscript', '31', '#804000', 'SIM');

-- --------------------------------------------------------

--
-- Table structure for table `redesocial`
--

CREATE TABLE IF NOT EXISTS `redesocial` (
  `facebook` varchar(255) NOT NULL,
  `googleplus` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `redesocial`
--

INSERT INTO `redesocial` (`facebook`, `googleplus`, `linkedin`) VALUES
('https://www.facebook.com/lincoln.n.borges', 'https://plus.google.com/+LincolnBorgesrp/', 'http://br.linkedin.com/in/lincolnborges');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
