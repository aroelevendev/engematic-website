
<html>
    <head>
<title>Engematic | Area Administrativa</title>	
<!--[if lt IE 11]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/bootstrap-theme.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/bootstrap.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/jasny-bootstrap.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/template.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/bootstrap-responsive.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/datepicker.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/dropzone.css')?>"   />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/style.css')?>"   />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<script type="text/javascript" src="<?php echo base_url('public/js/dropzone.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/template.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/jasny-bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/formata.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.simplyCountable.js'); ?>"></script>
<script>
	if (top.location != location) {
    top.location.href = document.location.href ;
  }
		$(function(){
			window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'dd/mm/yyyy',
				autoclose: true,
			}).on('changeDate', function(ev){
    $(this).datepicker('hide');
});
			$('#dp2').datepicker();
			$('#dp3').datepicker();
			$('#dp3').datepicker();
			$('#dpYears').datepicker();
			$('#dpMonths').datepicker();
			
			
			var startDate = new Date(2012,1,20);
			var endDate = new Date(2012,1,25);
			$('#dp4').datepicker({
				format: 'dd-mm-yyyy'
			})
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The start date can not be greater then the end date');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker({
				format: 'dd-mm-yyyy'
			})
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The end date can not be less then the start date');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});

        // disabling dates
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#dpd1').datepicker({
          onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
          }
          checkin.hide();
          $('#dpd2')[0].focus();
        }).data('datepicker');
        var checkout = $('#dpd2').datepicker({
          onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
          }
        }).on('changeDate', function(ev) {
          checkout.hide();
        }).data('datepicker');
		});
	</script>
    </head>
    
    <style>
.navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:hover, .navbar-inverse .navbar-nav>.active>a:focus { background: #000; color: #fff}
.navbar-inverse .navbar-nav>li>a {color: #000;}
.navbar-inverse .navbar-brand {color: #f40;}
.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open .dropdown-toggle.btn-primary {
    color: #fff;

}
.nav > li > a:hover, .nav > li > a:focus {
    text-decoration: none;
    background-color: #f40;
}
.nav .open > a, .nav .open > a:hover, .nav .open > a:focus {
    background-color: #f40;
    border-color: #000;
}
body { padding-bottom: 70px; }
.datepicker td.day.disabled {
color: red;
}
.datepicker td.new {
color: #000;
}
.datepicker td.old,
.datepicker td.new {
  color: #000;
}
.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
color: #FFFFFF;
background-color: black;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
color: #FFF;
background-color: #000000;
}
.navbar-default .navbar-nav > li > a {
color: #FFFFFF;
text-transform: uppercase;
font-size: 11px;
letter-spacing: 0.3px;
padding-left: 6px;
padding-right: 6px;
}
.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
color: #000;
background-color: #FD9535;
}
ul.dropdown-menu > li:hover{
background-color: #FD9535;	
}
.dropdown-menu {
background-color: #f4f4f4;
border: 0px solid rgba(0, 0, 0, .15);
}
.navbar-default .navbar-nav .open .dropdown-menu > li > a {
color: black;
}
ul.nav li.dropdown:hover > ul.dropdown-menu {
    display: block;    
}
.sair{
background-color: white !important;
color: black !important;
border-color: white !important;	
}
.sair:hover{
background-color: black !important;
color: white !important;
border-color: black !important;	
}
.alterar{
background-color: #FD9535 !important;
border-color: #FD9535 !important;	
}
.alterar:hover{
background-color: #f0ad4e !important;
border-color: #f0ad4e !important;	
}
.espaco{
margin-right: 10px;	
}
.falink{
vertical-align: 0%;
margin-top: 2px;
}
body{
background:url(<?php echo base_url("public/imagens/bg.jpg"); ?>);	
}
div#success{
display:none; color: #FFF;background-color: #0C0;padding: 4px;font-weight: bold;text-align: center;text-transform: uppercase;margin-top: -5.4%;	
}
div#error{
display:none; color:#FFF;background-color: #F00;padding: 4px;font-weight: bold;text-align: center;text-transform: uppercase;margin-top: -5.4%;	
}
select:disabled {
    background: #EBEBE4;
}
    </style>
<!-- Pegando a sessão para validar o menu --> 
<?php $sessao = $this->session->userdata('logged_in'); 
if($sessao['username']=="")
redirect(base_url());
?>

<center>
<a href="http://www.aroeleven.com.br/" target="_blank"><img src="<?php echo base_url("public/imagens/topo.png"); ?>"></a>
</center>


<nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
  <div class="container-fluid" style="background-color:#3e3e3e">
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="background-color: #D68D6B">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar" style="background-color: black"></span>
            <span class="icon-bar" style="background-color: black"></span>
            <span class="icon-bar" style="background-color: black"></span>
          </button>
</div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav" >

        <?php if($sessao['privilegio'] != 'alterar_senha'):  ?>
         <li class="<?php if(isset($noticia)) echo $noticia; ?>"><a href="<?php echo base_url("index.php/noticiactrl"); ?>"><i class="fa fa-newspaper-o fa-lg espaco" ></i>Notícia</a></li>

        <li class="dropdown <?php if(isset($home)) echo $home; ?>">
          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-arrow-circle-down fa-lg espaco" ></i>Home <b class="caret"></b></a>
          <ul class="dropdown-menu"> 
            <li><a href="<?php echo base_url("index.php/bannerctrl"); ?>"><b>Banner</b></a></li>
            <li><a href="<?php echo base_url("index.php/blococtrl"); ?>"><b>Blocos</b></a></li>
            <li><a href="<?php echo base_url("index.php/aniversarioctrl/EditaImagem/1"); ?>"><b>Aniversário</b></a></li>
          </ul>
        </li>

        <li class="dropdown <?php if(isset($segmentoproduto)) echo $segmentoproduto; ?>">
          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-arrow-circle-down fa-lg espaco" ></i>Produtos <b class="caret"></b></a>
          <ul class="dropdown-menu"> 
            <li><a href="<?php echo base_url("index.php/segmentoprodutoctrl"); ?>"><b>Categoria</b></a></li>
            <li><a href="<?php echo base_url("index.php/produtoctrl"); ?>"><b>Produto</b></a></li>
          </ul>
        </li>


		<li class="<?php if(isset($arquivo)) echo $arquivo; ?>"><a href="<?php echo base_url("index.php/arquivoctrl"); ?>"><i class="fa fa-file-o fa-lg espaco" ></i>Arquivos</a></li>

    <li class="<?php if(isset($case)) echo $case; ?>"><a href="<?php echo base_url("index.php/casectrl"); ?>"><i class="fa fa-share-alt fa-lg espaco" ></i>Artigos Técnicos</a></li>

    <li class="<?php if(isset($areanegocio)) echo $areanegocio; ?>"><a href="<?php echo base_url("index.php/areanegocioctrl"); ?>"><i class="fa fa-diamond fa-lg espaco" ></i>Área de Negócio</a></li>

    <li class="<?php if(isset($empresa)) echo $empresa; ?>"><a href="<?php echo base_url("index.php/historiactrl"); ?>"><i class="fa fa-university fa-lg espaco" ></i>Empresa</a></li>

    <li class="<?php if(isset($servico)) echo $servico; ?>"><a href="<?php echo base_url("index.php/servicoctrl"); ?>"><i class="fa fa-cogs fa-lg espaco" ></i>Serviços</a></li>

    <li class="<?php if(isset($psuporte)) echo $psuporte; ?>"><a href="<?php echo base_url("index.php/psuportectrl"); ?>"><i class="fa fa-wrench fa-lg espaco" ></i>Suporte</a></li>

    <li class="<?php if(isset($pcase)) echo $pcase; ?>"><a href="<?php echo base_url("index.php/pcasectrl"); ?>"><i class="fa fa-briefcase fa-lg espaco" ></i>Cases</a></li>
       
    <li class="<?php if(isset($certificado)) echo $certificado; ?>"><a href="<?php echo base_url("index.php/certificadoctrl"); ?>">Certificado</a></li>   
       
        
       
        
        
        <?php endif; ?>
          </ul> 
           <ul class="nav navbar-nav navbar-right">
             <li class="pull-right">
              
            <div class="btn btn-group" >
                <a href="<?php echo base_url("index.php/alterasenha/"); ?>" class="btn btn-warning alterar"><i class="fa fa-key fa-lg espaco" ></i>Alterar Senha</a>
              </div>
              <div class="btn btn-group" >
                <a href="<?php echo base_url('index.php/verificalogin/logout');?>" class="btn btn-primary sair"><i class="fa fa-sign-out fa-lg espaco" ></i>Sair</a>
              </div>
             </li>
          </ul>
        </div><!-- /.navbar-collapse -->

      </div><!-- /.container-fluid -->
    </nav>

<!-- seta o GTM para qualquer lugar do software que precisar do horario -->
<?php date_default_timezone_set('America/Sao_Paulo'); ?> 
</html>