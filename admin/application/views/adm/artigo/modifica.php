<?php 
$linkAtivo=array(
            'artigo' => 'active',
        );
        $this->load->view('header',$linkAtivo);

?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}		
</style>
<body> 
<h2 align="center">Área de Modificação de Artigo</h2></br>
    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="artigomodifica">
    <div>              
    </div>
    <table align="center">
      
      <tr>
		<td class="style1">
        	<b>Idioma: </b>
        </td>
        <td>
        <?php if($lingua=='_NOVO'):?>
        <select name="idioma" id="idioma">
				<?php if($ativo_pt==""){?><option value="_pt">Portugues</option><?php }?>
                <?php if($ativo_en==""){?><option value="_en">English</option><?php }?>
                <?php if($ativo_es==""){?><option value="_es">Espanhol</option><?php }?>
        </select>
        <?php else: ?>
        <select name="idioma" id="idioma" disabled>
				<option value="pt" <?php if($lingua=='_PT') echo "selected" ?>>Portugues</option>
                <option value="en" <?php if($lingua=='_EN') echo "selected" ?>>English</option>
                <option value="es" <?php if($lingua=='_ES') echo "selected" ?>>Espanhol</option>
            </select>
        <?php endif ?>
        	
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Referencia: </b>
        </td>
        <td>
        	<input type="text" name="referencia" id="referencia" style="width: 400px" autocomplete="off" value="<?php echo $referencia ?>" disabled/>
        </td>
       </tr>
       
        <tr>
		<td class="style1">
        	<b>Titulo: </b>
        </td>
        <td>
        	<input type="text" name="titulo" id="titulo" style="width: 400px" autocomplete="off" value="<?php echo $titulo ?>"/>
        </td>
       </tr>
       <tr>
		<td class="style1">
        	<b>Resumo: </b>
        </td>
        <td>
        	<span id="counter"></span> Caracteres restantes<br>
            <textarea type="text" name="resumo" id="resumo" style="width: 750px; max-width: 750px; height: 42px"/><?php echo $resumo ?></textarea>
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Descrição: </b>
        </td>
        <td>
        	<textarea type="text" name="descricao" id="content" style="width: 750px; max-width: 750px; height: 42px"/><?php echo $descricao ?></textarea>
        </td>
       </tr>
       <tr>
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/salvar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/salvar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/salvar.png');?>';" >
       <?php if($lingua!='_NOVO'): ?>
	   <?php if($ativo=='SIM'): ?>
       <img id="ativo" style="cursor:pointer;" src="<?php echo base_url('public/imagens/desativar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/desativar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/desativar.png');?>';" >
       <? else: ?>
       <img id="ativo" style="cursor:pointer;" src="<?php echo base_url('public/imagens/ativar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/ativar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/ativar.png');?>';" >
       <? endif ?> 
       <input type="hidden" name="status" id="status" value="<?php if($ativo=='SIM') echo 'NAO'; else echo 'SIM' ?>" /> 
       <? endif ?>  
        
        
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">          
        </td>
        </tr>
</table> 
		<div id="error">Erro ao cadastrar!</div>
		<div id="success">Cadastro Efetuado com sucesso!</div> 
        </form>
</body>
<script>
$(document).ready(function(){
	var formData;
	$('#resumo').simplyCountable({
	maxCount: 156,
	strictMax: true	
	});
	$("#salvar").click(function() {
		 CKupdate();
  		 formData = $(this).closest('form').serializeArray();
  		 formData.push({ name: 'salvar', value: 'Salvar' });
  		 console.log(formData);
		$("#salvar").hide();
		$("#ativo").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/artigoctrl/EditaArtigo').$lingua;?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/artigoctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#ativo").delay(1800).fadeIn(1000);
			 }			    
			 },
			error:function(xhr, ajaxOptions, thrownError){
			 $("#loader").hide();
			 console.log(xhr + ajaxOptions + thrownError);
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#ativo").delay(1800).fadeIn(1000);
			 $("#salvar").delay(1800).fadeIn(1000);  
			}
		});
	event.preventDefault();
	});
	
	$("#ativo").click(function() {
		 CKupdate();
  		 formData = $(this).closest('form').serializeArray();
  		 formData.push({ name: 'ativo', value: 'Ativo' });
  		console.log(formData);
		$("#salvar").hide();
		$("#ativo").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/artigoctrl/EditaArtigo').$lingua;?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/artigoctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#ativo").delay(1800).fadeIn(1000); 
			 }			    
			 },
			error:function(data){
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#ativo").delay(1800).fadeIn(1000);    
			}
		});
	event.preventDefault();
	});
});

function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

</script>
<?php
echo display_ckeditor($ckeditor['ckeditor']);
?>