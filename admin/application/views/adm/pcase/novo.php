<?php 
$linkAtivo=array(
            'pcase' => 'active',
        );
        $this->load->view('header',$linkAtivo);

?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}
</style>
<body> 
<h2 align="center">Área de Cadastro de Pcase</h2></br>
    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="pcasenovo">
    <div>              
    </div>
    <table align="center">

      <tr>
    <td class="style1">
          <b>Idioma: </b>
        </td>
        <td>
          <select name="idioma" id="idioma">
              <option value="PT">PT</option>
                <option value="EN">EN</option>
                <option value="ES">ES</option>
            </select>
        </td>
       </tr>

       <tr>
		<td class="style1">
        	<b>Título: </b>
        </td>
        <td>
        	<input type="text" name="titulo" id="titulo" style="width: 500px" autocomplete="off"/>
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Descriçao: </b>
        </td>
        <td>
        	<textarea type="text" name="descricao" id="descricao" style="width: 750px; max-width: 750px; height: 130px"/></textarea>
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Url Vídeo: </b>
        </td>
        <td>
        	<input name="video" id="video" style="width: 500px" autocomplete="off" placeholder="Ex. https://www.youtube.com/watch?v=FHhG6bpbmsk" type="text">
        </td>
       </tr>

       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">
        <div id="error">Erro ao cadastrar!</div>
		<div id="success">Cadastro Efetuado com sucesso!</div>  
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/cadastrar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/cadastrar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/cadastrar.png');?>';">       
        </td>
        </tr>
</table> </form>
</body>
<script>
$(document).ready(function(){
	$('#salvar').on('click',function(event) {
		$("#salvar").hide();
		$("#loader").show();
		CKupdate();
		$.ajax({
			url:'<?php echo base_url('index.php/pcasectrl/NovoPcase');?>',
			data:$('#pcasenovo').serialize(),
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			
			  if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(3000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/pcasectrl');?>";
			 }, 3800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(3000);
			  $("#salvar").delay(3800).fadeIn(3000); 
			 }  
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(3000);
   			 $("#salvar").delay(3800).fadeIn(3000); 
			}
		});
	event.preventDefault();         //To Avoid Page Refresh and Fire the Event "Click"
	});
});

function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

</script>
<?php
echo display_ckeditor($ckeditor['ckeditor']);
?>