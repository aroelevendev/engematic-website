<?php 
$linkAtivo=array(
            'pcase' => 'active',
        );
        $this->load->view('header',$linkAtivo);

?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}		
</style>
<body> 
<h2 align="center">Área de Modificação do Pcase</h2></br>
    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="pcasemodifica">
    <div>              
    </div>
    <table align="center">

       <tr>
		<td class="style1">
        	<b>Idioma: </b>
        </td>
        <td>
        	<select name="idioma2" id="idioma2" disabled>
            	<option value="PT" <?php if($idioma=="PT") echo "selected" ?>>PT</option>
                <option value="EN" <?php if($idioma=="EN") echo "selected" ?>>EN</option>
                <option value="ES" <?php if($idioma=="ES") echo "selected" ?>>ES</option>
            </select>
            <input type="hidden" name="idioma" id="idioma" style="width: 100px" autocomplete="off" value="<?php echo $idioma ?>"/>
        </td>
       </tr>

       <tr>
		<td class="style1">
        	<b>Título: </b>
        </td>
        <td>
        	<input type="text" name="titulo" id="titulo" style="width: 500px" autocomplete="off" value="<?php echo $titulo ?>"/>
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Descriçao: </b>
        </td>
        <td>
        	<textarea type="text" name="descricao" id="descricao" style="width: 750px; max-width: 750px; height: 130px"/><?php echo $descricao ?></textarea>
        	
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Url Vídeo: </b>
        </td>
        <td>
        	<input name="video" id="video" value="<?php echo $video ?>" style="width: 500px" autocomplete="off" placeholder="Ex. https://www.youtube.com/watch?v=FHhG6bpbmsk" type="text">
        </td>
       </tr>
       
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/salvar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/salvar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/salvar.png');?>';" >
      <img id="exclui" style="cursor:pointer;width: 143px;height: 29px;" src="<?php echo base_url('public/imagens/excluir.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/excluir_ativo.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/excluir.png');?>';" >  
        <input type="hidden" name="status" id="status" value="<?php if($ativo=='SIM') echo 'NAO'; else echo 'SIM' ?>" /> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">          
        </td>
        </tr>
</table> 
		<div id="error">Erro ao cadastrar!</div>
		<div id="success">Cadastro Efetuado com sucesso!</div> 
        </form>
</body>
<script>
function AnoSaida(){
	 if (document.getElementById('presente').checked) {
		 document.getElementById('tr_data_saida').style.display = "none";
        } else {
         document.getElementById('tr_data_saida').style.display = "table-row";
     }	
}
</script>
<script>
$(document).ready(function(){
	var formData;
	
	$("#salvar").click(function() {
		 CKupdate();
  		 formData = $(this).closest('form').serializeArray();
  		 formData.push({ name: 'salvar', value: 'Salvar' });
  		 console.log(formData);
		$("#salvar").hide();
		$("#exclui").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/pcasectrl/EditaPcase');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/pcasectrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#exclui").delay(1800).fadeIn(1000);
			 }			    
			 },
			error:function(data){
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#exclui").delay(1800).fadeIn(1000);   
			}
		});
	event.preventDefault();
	});
	
	$("#exclui").click(function() {
		 CKupdate();
  		 formData = $(this).closest('form').serializeArray();
  		 formData.push({ name: 'exclui', value: 'Ativo' });
  		console.log(formData);
		$("#salvar").hide();
		$("#exclui").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/pcasectrl/EditaPcase');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/pcasectrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#exclui").delay(1800).fadeIn(1000); 
			 }			    
			 },
			error:function(data){
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#exclui").delay(1800).fadeIn(1000);    
			}
		});
	event.preventDefault();
	});
	
	
	$('#pcasemodifica').on('submit',function(event) {
		console.log(formData);
		$("#salvar").hide();
		$("#exclui").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/pcasectrl/EditaPcase');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/pcasectrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(data){
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/pcasectrl');?>";
			 }, 1800);    
			}
		});
	event.preventDefault();         //To Avoid Page Refresh and Fire the Event "Click"
	});
});

function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

</script>
<?php
echo display_ckeditor($ckeditor['ckeditor']);
?>