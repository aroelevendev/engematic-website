<?php 
$linkAtivo=array(
            'calendario' => 'active',
        );
        $this->load->view('header',$linkAtivo);

?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}	
</style>
<body> 
<h2 align="center">Área de Modificação do Calendario</h2></br>
    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="calendariomodifica">
    <div>              
    </div>
    <table align="center">
       
        <tr>
		<td class="style1">
        	<b>Treinamento:</b>
        </td>
        <td>
        	<?php echo $treinamento ?>
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Data Inicio:</b>
        </td>
        <td>
        <?php 
			
		?>
        	<input data-date-format="dd/mm/yyyy" style="width: 100px" type="text" name="data_inicio" value="<?php echo $data_inicio; ?>" id="dpd1">
        </td>
       </tr>
       
      <tr>
		<td class="style1">
        	<b>Data Fim: </b>
        </td>
        <td>
        	<input data-date-format="dd/mm/yyyy" style="width: 100px" type="text" name="data_fim" value="<?php echo $data_fim ?>" id="dpd2">
        </td>
       </tr>
       <tr>
		<td class="style1">
        	<b>Horario: </b>
        </td>
        <td>
        	<input type="text" name="hora" id="hora" style="width: 400px" autocomplete="off" value="<?php echo $hora ?>"/>
        </td>
       </tr>
       <tr>
		<td class="style1">
        	<b>Local: </b>
        </td>
        <td>
        	<input type="text" name="local" id="local" style="width: 400px" autocomplete="off" value="<?php echo $local ?>"/>
        </td>
       </tr>
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
       <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/salvar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/salvar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/salvar.png');?>';" >
       <?php if($ativo=='SIM'): ?>
       <img id="ativo" style="cursor:pointer;" src="<?php echo base_url('public/imagens/desativar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/desativar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/desativar.png');?>';" >
       <? else: ?>
       <img id="ativo" style="cursor:pointer;" src="<?php echo base_url('public/imagens/ativar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/ativar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/ativar.png');?>';" >
       <? endif ?>        
        <input type="hidden" name="status" id="status" value="<?php if($ativo=='SIM') echo 'NAO'; else echo 'SIM' ?>" /> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">
          
        </td>
        </tr>
</table> <div id="error">Erro ao cadastrar!</div>
		<div id="success">Cadastro Efetuado com sucesso!</div> 
        </form>
        
</body>
<script>
$(document).ready(function(){
	var formData;
	
	$("#salvar").click(function() {
		formData = $(this).closest('form').serializeArray();
  		formData.push({ name: 'salvar', value: 'Salvar' });
  		console.log(formData);
		$("#salvar").hide();
		$("#ativo").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/calendarioctrl/EditaCalendario');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/calendarioctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000); 
			  $("#ativo").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(data){
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000); 
			 $("#ativo").delay(1800).fadeIn(1000);    
			}
		});
	event.preventDefault();
	});
	
	$("#ativo").click(function() {
  		formData = $(this).closest('form').serializeArray();
  		formData.push({ name: 'ativo', value: 'Ativo' });
  		console.log(formData);
		$("#salvar").hide();
		$("#ativo").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/calendarioctrl/EditaCalendario');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/calendarioctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000); 
			  $("#ativo").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(data){
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000); 
			 $("#ativo").delay(1800).fadeIn(1000);    
			}
		});
	event.preventDefault();
	});
	
	
	$('#calendariomodifica').on('submit',function(event) {
		
	});
});


</script>