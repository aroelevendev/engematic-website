<?php 
$linkAtivo=array(
            'calendario' => 'active',
        );
        $this->load->view('header',$linkAtivo);

?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}
</style>
<body> 
<h2 align="center">Área de Cadastro de Calendario</h2></br>
    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="calendarionovo">
    <div>              
    </div>
    <table align="center">
      
       <tr>
		<td class="style1">
        	<b>Treinamento: </b>
        </td>
        <td>
        	<?php echo $treinamento; ?>
        	
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Data Inicio: </b>
        </td>
        <td>
        	<input data-date-format="dd/mm/yyyy" style="width: 100px" type="text" name="data_inicio" value id="dpd1">
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Data Fim: </b>
        </td>
        <td>
        	<input data-date-format="dd/mm/yyyy" style="width: 100px" type="text" name="data_fim" value id="dpd2">
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Horário: </b>
        </td>
        <td>
        	<input type="text" name="hora" id="hora" style="width: 400px" autocomplete="off"/>
        </td>
       </tr>
		<tr>
		<td class="style1">
        	<b>Local: </b>
        </td>
        <td>
        	<input type="text" name="local" id="local" style="width: 400px" autocomplete="off" />
        </td>
       </tr>
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">
        <div id="error">Erro ao cadastrar!</div>
		<div id="success">Cadastro Efetuado com sucesso!</div> 
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/cadastrar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/cadastrar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/cadastrar.png');?>';">
        <!--<input type="submit" value="Cadastrar" style="width: 300px;" class="btn-warning" id="salvar"/> -->      
        </td>
        </tr>
</table> </form>
</body>
<script>
$(document).ready(function(){
	$('#salvar').on('click',function(event) {
		$("#salvar").hide();
		$("#loader").show();
		CKupdate();
		$.ajax({
			url:'<?php echo base_url('index.php/calendarioctrl/NovoCalendario');?>',
			data:$('#calendarionovo').serialize(),
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			
			  if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(3000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/calendarioctrl');?>";
			 }, 3800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(3000);
			  $("#salvar").delay(3800).fadeIn(3000); 
			 }  
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(3000);
   			 $("#salvar").delay(3800).fadeIn(3000); 
			}
		});
	event.preventDefault();         //To Avoid Page Refresh and Fire the Event "Click"
	});
});

function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

</script>
<?php
echo display_ckeditor($ckeditor['ckeditor']);
?>