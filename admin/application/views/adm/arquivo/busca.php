<?php 
 $linkAtivo=array(
            'arquivo' => 'active',
        );
        $this->load->view('header',$linkAtivo);
?>
<style>
table, th, td
{
padding:3px;
border-color: transparent;
}
table
{
border-spacing:15px;
}
.btn-warning {
color: #000;
}
.mainarea { overflow: hidden; position: relative; }
.mainarea table { width: 100%;  table-layout:fixed !important; margin-top:20px; }
.mainarea table th{ background-color: #FF9536;color: #000; border-color: #000;}
tr:nth-child(even) {
    background-color: rgb(224, 224, 224);
}
a {
color: #EC7100;
text-decoration: none;
}
a:hover, a:focus {
color: #000000;
text-decoration: none;
}
</style>
<!-- como precisa de um valor, se caso não tiver, o mesmo é atribuido -->
<?php if(empty($privilegio)) $privilegio=1; ?>
<body> 
    <div>              
    </div> 
    <table align="center">
       
              <td align="right" ><b></b></td>
        <td><input type="text" name="palavrachave" id="palavrachave" style="width: 250px;" autocomplete="off" placeholder="Busca Arquivo"/>
            <input type="hidden" name="primeiraentrada" value="1"/></td>

    <td align="center" >
    <img style="cursor:pointer;" src="<?php echo base_url('public/imagens/addnovo.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/addnovo select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/addnovo.png');?>';" onClick="window.location.href='<?php echo base_url('index.php/arquivoctrl/NovoArquivo');?>'">
      </td></table> 
    <div class="mainarea">
    <div id="autoSuggestionsList">  
            </div>

    </div>
</body>

<script>
	$(document).ready(function(){
		
		  var post_data = {
                    'palavrachave': $("#palavrachave").val()
                };
		$.ajax({
			type: "post",
			url: "<?php echo base_url('index.php/arquivoctrl/autocomplete');?>",
			cache: false,
			data:post_data,
			success: function(response){
				$('#autoSuggestionsList').html(response);
				console.log(response);
				},
			error:function (jqXHR, textStatus, errorThrown){
      			console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			}
		});

	  
	  $("#palavrachave").keyup(function(){
		  var post_data = {
                    'palavrachave': $("#palavrachave").val(),
                };
		$.ajax({
			type: "post",
			url: "<?php echo base_url('index.php/arquivoctrl/autocomplete');?>",
			cache: false,
			data:post_data,
			beforeSend: function( xhr ) {
     			$('#autoSuggestionsList').html('');
  			},
			success: function(response){
				$('#autoSuggestionsList').html(response);
				},
			error:function (jqXHR, textStatus, errorThrown){
      			console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			}
		});
		
		return false;
	  });
	});
</script>