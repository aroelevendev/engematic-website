<?php 
$linkAtivo=array(
            'trabalheconosco' => 'active',
        );
        $this->load->view('header',$linkAtivo);
$qtd_arq = 0;
?>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.form.min.js'); ?>"></script>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}	
</style>
<body> 
<h2 align="center">Área de Modificação de Profissão</h2></br>
<table align="center" style="max-width: 900px;">

 

    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="profissaomodifica">
    <div>              
    </div>
    <div style="display:none">
    <input type="text" name="qtd_arq" id="qtd_arq" value="<?php echo $qtd_arq; ?>"/>
    <?php 
	echo $input;
	?>
      </div>
      
       <tr>
		<td class="style1">
        	<b>Nome: </b>
        </td>
        <td>
        	<input type="text" name="prf_nome" id="prf_nome" style="width: 400px" autocomplete="off" value="<?php echo $prf_nome ?>"/>
        </td>
       </tr>
       
      
             


       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <input type="hidden" name="prf_codigo" id="prf_codigo" value="<?php echo $prf_codigo ?>" />
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/salvar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/salvar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/salvar.png');?>';" >
       <?php if($prf_ativa=='SIM'): ?>
       <img id="prf_ativa" style="cursor:pointer;" src="<?php echo base_url('public/imagens/desativar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/desativar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/desativar.png');?>';" >
       <? else: ?>
       <img id="prf_ativa" style="cursor:pointer;" src="<?php echo base_url('public/imagens/ativar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/ativar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/ativar.png');?>';" >
       <? endif ?>  
        <input type="hidden" name="status" id="status" value="<?php if($prf_ativa=='SIM') echo 'NAO'; else echo 'SIM' ?>" /> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">
   
        </td>
        </tr>
 </form>
<form action="<?php echo base_url('index.php/profissaoctrl/processupload');?>" method="post" enctype="multipart/form-data" id="MyUploadForm">
<div style="display:none">
<?php 
$i = 0;
while($i<$qtd_arq){ $i++;?>
<input name="imagem<?php echo $i ?>" id="imagem<?php echo $i ?>" type="file" onChange="upload_imagem('imagem<?php echo $i ?>','imagem<?php echo $i ?>_preview','imagem<?php echo $i ?>_input','imagem<?php echo $i ?>_status','imagem<?php echo $i ?>_exclui','imagem<?php echo $i ?>_progressbar');"/>
<?php } ?>
<?php
$Random_Number = rand(0, 9999999999);
?>
<input name="rand" id="rand" type="text" value="<?php echo $Random_Number?>" />
</div>

</form>

       </table>
       <div id="error">Erro ao cadastrar!</div>
	   <div id="success">Cadastro Efetuado com sucesso!</div>   
</body>
<script>
$(document).ready(function(){
	var formData;
	
	$("#salvar").click(function() {
		// CKupdate();
  		 formData = $('#profissaomodifica').serializeArray();
  		 formData.push({ name: 'salvar', value: 'salvar' });
		 console.log(formData);
		$("#salvar").hide();
		$("#prf_ativa").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/profissaoctrl/EditaProfissao');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/profissaoctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#prf_ativa").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000);
			 $("#prf_ativa").delay(1800).fadeIn(1000);			    
			}
		});
	event.preventDefault();
	});
	
	$("#prf_ativa").click(function() {
		// CKupdate();
  		 formData = $('#profissaomodifica').serializeArray();
  		 formData.push({ name: 'prf_ativa', value: 'prf_ativa' });
		 console.log(formData);
		$("#salvar").hide();
		$("#prf_ativa").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/profissaoctrl/EditaProfissao');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/profissaoctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#prf_ativa").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000);
			 $("#prf_ativa").delay(1800).fadeIn(1000);			    
			}
		});
	event.preventDefault();
	});
});
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}
function upload_imagem(id_image,id_preview,id_input,id_status,id_exclui,id_progressbar){
	var options = {
			beforeSubmit:  beforeSubmit,  
			success:       afterSuccess,  
			uploadProgress: OnProgress,
		};
	
	$('#MyUploadForm').ajaxSubmit(options);  			
	return false; 
	
function afterSuccess()
{
	$('#'+id_progressbar).width('0%')
	$('#submit-btn').show(); 
	$('#'+id_status).hide(); 
	var nomearq = $('#'+id_image)[0].files[0].name.split('.');
	var ext = $('#'+id_image)[0].files[0].name.split('.').pop().toLowerCase();
	var bd = '<?php echo base_url('public/img');?>'+'/thumb_'+nomearq[0]+'_'+$('#rand').val()+'.'+ext;
	$('#'+id_preview).css({
        'background-image' : 'url('+bd+')',
        'background-size'  : 'cover'
    });
	$('#'+id_exclui).show();

}

function beforeSubmit(){
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		if( !$('#'+id_image).val()) 
		{
			$("#output").html("Nenhum Arquivo encontrado");
			return false
		}
		$('#'+id_exclui).hide();
		var nomearq = $('#'+id_image)[0].files[0].name.split('.');
		var ext = $('#'+id_image)[0].files[0].name.split('.').pop().toLowerCase();
		var bd = "thumb_"+nomearq[0]+'_'+$('#rand').val()+'.'+ext;
		$('#'+id_preview).css('backgroundImage','url()');
		$('#'+id_preview).css('background-size','auto 30px');
		$('#'+id_input).val(bd)
		var fsize = $('#'+id_image)[0].files[0].size; 
		var ftype = $('#'+id_image)[0].files[0].type; 
		

		switch(ftype)
        {
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html':
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }  
	}
	
}

function OnProgress(event, position, total, percentComplete)
{
    $('#'+id_status).show();
	$('#'+id_progressbar).width(percentComplete + '%')
    $('#'+id_status).html(percentComplete + '%');
    if(percentComplete>1)
        {
            $('#'+id_status).css('color','#000');
        }
}

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
}		
function apaga(id_image,id_preview,id_input,id_status,id_exclui) {
   $('#'+id_exclui).hide();
   $('#'+id_input).val('');
   $('#'+id_image).val('');
   $('#'+id_preview).css('backgroundImage','url(<?php echo base_url('public/camera.png');?>)');
   $('#'+id_preview).css('background-size','auto 30px');
   event.stopPropagation();     
}	
</script>
<?php
//echo display_ckeditor($ckeditor['ckeditor']);
?>