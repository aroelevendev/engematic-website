<?php 
$linkAtivo=array(
            'dropdownativo_p' => 'active',
        );
        $this->load->view('header',$linkAtivo);

?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}
</style>
<body> 
<?php if(isset($id)): ?>
<h2 align="center">Editar Referencia de Revenda</h2></br>
<?php $url = base_url('index.php/prevendactrl/EditaPRevenda'); ?>
<?php else: ?>
<?php $url = base_url('index.php/prevendactrl/NovoPRevenda'); ?>
<h2 align="center">Cadastrar Nova Referencia de Revenda</h2></br>
<?php endif ?>

    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="prevendanovo">
    <div>              
    </div>
    <table align="center">
      <?php if(isset($id)): ?>
      <tr>
		<td class="style1">
        	<b>ID: </b>
        </td>
        <td>
        	<input type="text" style="width: 400px" autocomplete="off" value="<?php echo $id ?>" disabled/>
            <input type="text" name="id" id="id" style="display:none"  value="<?php echo $id ?>"/>
        </td>
       </tr>
     <?php endif ?>  
       <tr>
		<td class="style1">
        	<b>Referencia: </b>
        </td>
        <td>
        	<input type="text" name="referencia" id="referencia" style="width: 400px" autocomplete="off" value="<?php if(isset($referencia)) echo $referencia; ?>"/>
        </td>
       </tr>
       
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">
        <div id="error">Erro ao cadastrar!</div>
		<div id="success">Cadastro Efetuado com sucesso!</div>  
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/cadastrar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/cadastrar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/cadastrar.png');?>';">       
        </td>
        </tr>
</table> </form>
</body>
<script>
$(document).ready(function(){
	$('#salvar').on('click',function(event) {
		$("#salvar").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo $url;?>',
			data:$('#prevendanovo').serialize(),
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			
			  if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(3000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/prevendactrl');?>";
			 }, 3800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(3000);
			  $("#salvar").delay(3800).fadeIn(3000); 
			 }  
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(3000);
   			 $("#salvar").delay(3800).fadeIn(3000); 
			}
		});
	event.preventDefault();         //To Avoid Page Refresh and Fire the Event "Click"
	});
});
</script>