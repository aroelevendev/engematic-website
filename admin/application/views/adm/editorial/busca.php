<?php 
 $linkAtivo=array(
            'ativo' => 'editorial',
        );
        $this->load->view('header',$linkAtivo);
?>
<style>
table, th, td
{

padding:5px;

}
table
{
border-spacing:15px;
}
.btn-warning {
color: #000;
}
</style>
<!-- como precisa de um valor, se caso não tiver, o mesmo é atribuido -->
<?php if(empty($privilegio)) $privilegio=1; ?>
<body> 
    <form action="editorialbusca" method="post" accept-charset="UTF-8">
    <div>              
    </div> 
    <h2 align="center"> Busca Editorial </h2>
    <table align="center">
        
              <td align="right" ><b>Palavra Chave</b></td>
        <td><input type="text" name="palavrachave" id="palavrachave" style="width: 200px;" autocomplete="off"/></td>
        
</table> 
    </form>
   <div id="autoSuggestionsList">  
            </div>
    <?php if(!empty($tabela))
            echo $tabela;
    ?>
    
</body>
<script>
	$(document).ready(function(){
	  $("#palavrachave").keyup(function(){
		  var post_data = {
                    'palavrachave': $("#palavrachave").val(),
					'privilegio': <?php echo $this->session->userdata['logged_in']['privilegio'] ?>,
                };
		$.ajax({
			type: "post",
			url: "<?php echo base_url('index.php/editorialbusca/autocomplete');?>",
			cache: false,
			//data:"palavrachave="+$("#palavrachave").val(),
			data:post_data,
			success: function(response){
				$('#autoSuggestionsList').html(response);
				console.log(response);
				},
			error:function (jqXHR, textStatus, errorThrown){
      			console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			}
		});
		
		return false;
	  });
	});
</script>
