<?php 
$linkAtivo=array(
            'ativo' => 'editorial',
        );
        $this->load->view('header',$linkAtivo);
//echo "<script>alert('".validation_errors()."')</script>"; 

if(!empty($msg)){
    echo "<script>alert('Editorial Inserido com Sucesso')</script>";
}
$total_img=25;
?>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
    }
.btn-warning 
    {
    color: #000;
    }
</style>
<body> 
    <form action="editorialinsere" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
    <div>              
    </div> 
    <h2 align="center"> Editorial </h2>
    <table align="center">
       <tr>
           <td  align="right" class="style1" ><b>Titulo:</b></td>
              <td class="style1"><input type="text" name="titulo" style="width: 750px" /></td>
       </tr>
       
        <tr>
              <td align="right" ><b>Categoria</b></td>
              <td>
        <select name="privilegio">
        <?php if(in_array($this->session->userdata['logged_in']['privilegio'],array(0,1,2))): ?>
            <option value="1">Presidente</option>
            <option value="2">Vice Presidente</option>
            <option value="3">Delegado</option>
            <option value="4">Técnico do Congresso</option>
            <option value="5">Diretora de Membros</option>
            <option value="6">Diretor de e-Marketing</option>
            <option value="7">Diretora da Seção Estudantil</option>
            <option value="8">Advisor da Seção Estudantil</option>
            <option value="9">Diretor de Treinamentos</option>
            <option value="10">Diretor de Eventos e Feiras</option>
            <option value="11">Diretora Executivo Administrativo</option>
            <option value="12">Diretor Acadêmico do Congresso</option>
            <option value="13">Diretor do Conselho</option>
            <option value="14">Diretor de Tecnologia</option>
            <option value="15">Diretor de Inovação e Institucional</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==3): ?>
            <option value="3">Delegado</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==4): ?>
            <option value="4">Técnico do Congresso</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==5): ?>
            <option value="5">Diretora de Membros</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==6): ?>
            <option value="6">Diretor de e-Marketing</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==7): ?>
            <option value="7">Diretora da Seção Estudantil</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==8): ?>
            <option value="8">Advisor da Seção Estudantil</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==9): ?>
            <option value="9">Diretor de Treinamentos</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==10): ?>
            <option value="10">Diretor de Eventos e Feiras</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==11): ?>
            <option value="11">Diretora Executivo Administrativo</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==12): ?>
            <option value="12">Diretor Acadêmico do Congresso</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==13): ?>
            <option value="13">Diretor do Conselho</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==14): ?>
            <option value="14">Diretor de Tecnologia</option>
        <?php endif; ?>
        <?php if($this->session->userdata['logged_in']['privilegio']==15): ?>
            <option value="15">Diretor de Inovação e Institucional</option>
        <?php endif; ?>
        </select>
        </td>
       </tr>
       <tr>
           <td align="right"><b>Data:</b></td>
           <!-- Como não passa o campo disabled via post, tive que colocar um hidden
                Ou seja, um campo apenas para mostrar para o usuario, e outro para passar    
                via post. -->
              <td><input type="text" name="" disabled = "disabled" value="<?php echo date("d/m/Y") ?>" /></td>
              <input type="hidden" name="data" value="<?php echo date("d/m/Y - H:i:s") ?>" />
       </tr>
       <tr>
              <td valign="top" align="right"><b>Resumo:</b></td>
              <td><textarea type="text" name="resumo" style="width: 750px; max-width: 750px; height: 42px"/></textarea></td>
       </tr>

       <tr>
              <td align="right"><b>Conteudo:</b></td>
              <td><textarea name="conteudo" id="content" ></textarea>
        </td>
       </tr>
<tr>
  <td></td>
  <td>
    <div style="display:none">
     <?php 
  	for($i=1;$i<=$total_img;$i++){
	?>	
    <input type='file' name="images[]" id="file<?php echo $i; ?>" onChange="readURL(this,'img_prev<?php echo $i; ?>');"/>
    <?php } ?>
  	</div>
  <table>
  <thead>
        <tr>							
            <th colspan="5" scope="col" align="left">Imagens</th>
        </tr>
</thead>
<tbody>
  <?php 
  	for($i=1;$i<=$total_img;$i++){
		if ($i % 5 === 0) echo "<tr>";
		if($i==1) echo '<td colspan=2 rowspan=1 >'; else echo "<td>";
	?>
    <div id="ximg_prev<?php echo $i; ?>" style="display:none;cursor:pointer;">[Excluir]</div>	
    <img style="float:left" id="img_prev<?php echo $i; ?>" width="<?php if($i==1) echo '200'; else echo '120';?>" height="<?php if($i==1) echo '180'; else echo '100';?>" src="http://thumbs.dreamstime.com/x/digital-camera-icon-9193700.jpg" onclick='document.getElementById("file<?php echo $i; ?>").click()' />
    
    </td>
	<?php
	//if ($i % 5 === 0) echo "</tr>";
    }
	?>
    
    </tr>
    </tbody>
    </table>
  </td>
</tr>
             
        <tr>
        <td> </td>
        <td align="center"> <input type="submit" value="Enviar" style="width: 500px;" class="btn-warning"/>       
        </td>
        </tr>
</table> 
    </form>
    
    
</body>
<script>

function readURL(input,id) {
if (input.files && input.files[0]) {
var reader = new FileReader();

reader.onloadstart = function (e) {
setTimeout(function(){ $('#'+id).attr('src', '<?php echo base_url('public/carregando.gif')?>') }, 1);


//setTimeout("document.getElementById['img_prev1'].src='http://localhost:85/admin/public/loadinfo.net.gif'", 200);
};

reader.onloadend = function (e) {
  //alert( e.total/1300);
setTimeout(function(){ $('#'+id).attr('src', e.target.result) }, e.total/1300);
//$('#'+id).attr('src', e.target.result)
};

reader.readAsDataURL(input.files[0]);
}else{
      var img = input.value;
        $('#'+id).attr('src',img).height(200);
    }
    $("#x"+id).show().css("display","inherit");
}

</script>
<?php 
  	for($i=1;$i<=$total_img;$i++){
	?>	
    <script>
		$(document).ready(function() {
		$("#ximg_prev<?php echo $i; ?>").click(function() {
		  $("#img_prev<?php echo $i; ?>").attr("src","http://thumbs.dreamstime.com/x/digital-camera-icon-9193700.jpg");
		  $("#ximg_prev<?php echo $i; ?>").hide();
		  $("#file<?php echo $i; ?>").replaceWith( $("#file<?php echo $i; ?>").val('').clone( true ) );  
		});
	  });
	</script>
    <?php } ?>
    
<?php echo display_ckeditor($ckeditor); ?>
