<?php 
$linkAtivo=array(
            'empresa' => 'active',
        );
        $this->load->view('header',$linkAtivo);
$qtd_arq = 20;
?>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.form.min.js'); ?>"></script>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}	
.img_inner{ display:none;}
#loadMore {
color: rgb(236, 236, 236);
cursor: pointer;
background-color: rgb(252, 148, 55);
font-weight: bold;
padding-top: 10px;
padding-bottom: 10px;
padding-left: 10px;
padding-right: 10px;
text-align: center;
clear: both;
}
#loadMore:hover {
background-color: black;
color: rgb(252, 148, 55);
}
</style>
<body> 
<h2 align="center">Área de Modificação de Historia</h2></br>
<table align="center" style="max-width: 900px;">

 

    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="historiamodifica">
    <div>              
    </div>
    <div style="display:none">
    <input type="text" name="qtd_arq" id="qtd_arq" value="<?php echo $qtd_arq; ?>"/>
    <?php 
	echo $input;
	?>
	<input name="catalogo_input" id="catalogo_input" type="text" value="<?php echo $catalogo ?>" />
      </div>
      
       <tr>
		<td class="style1">
        	<b>Idioma: </b>
        </td>
        <td>
        	<select name="idioma2" id="idioma2" disabled>
            	<option value="PT" <?php if($idioma=="PT") echo "selected" ?>>PT</option>
                <option value="EN" <?php if($idioma=="EN") echo "selected" ?>>EN</option>
                <option value="ES" <?php if($idioma=="ES") echo "selected" ?>>ES</option>
            </select>
            <input type="hidden" name="idioma" id="idioma" style="width: 100px" autocomplete="off" value="<?php echo $idioma ?>"/>
        </td>
       </tr>

       <tr>
		<td class="style1">
        	<b>Título: </b>
        </td>
        <td>
        	<input type="text" name="titulo" id="titulo" style="width: 400px" autocomplete="off" value="<?php echo $titulo ?>"/>
        </td>
       </tr>

              

       <tr>
		<td class="style1">
        	<b>Resumo: </b>
        </td>
        <td>
        	<span id="counter"></span> Caracteres restantes<br>
        	<textarea type="text" name="resumo" id="resumo" style="width: 750px; max-width: 750px; height: 60px"/><?php echo $resumo ?></textarea>
        	
        </td>
       </tr>
       
       <tr>
		<td class="style1">
        	<b>Descrição: </b>
        </td>
        <td>
        	<textarea type="text" name="descricao" id="content" style="width: 750px; max-width: 750px; height: 42px"/><?php echo $descricao ?></textarea>
        </td>
       </tr>

       <tr style="display: none;">
    <td class="style1">
          <b>Catálogo: </b>
        </td>
        <td>
        <?php 
        if($catalogo==""){
        	$imagem_div_catalogo = "nuvem.png";
        }else{
        	$imagem_div_catalogo = "ok.png";
        	$link_catalogo = "/admin/public/img/".$catalogo;
        }
        ?>
          <div style='background:url(<?php echo base_url('public').'/nuvem.png';?>) center center;
    background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
    cursor:pointer;border: 1px solid black;float: left;margin-bottom:5px;margin-right:5px' class='img_inner' id='catalogo_preview' onClick="document.getElementById('catalogo').click()"><center><label id="catalogo_exclui" style="z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:none" onClick="apaga('catalogo','catalogo_preview','catalogo_input','catalogo_status',catalogo_exclui);">excluir</label><div id="catalogo_progressbar" style="background-color: #FD9534;height: 100%;width: 0%;float: left;"></div ></center></div>
    <?php if(isset($link_catalogo)) { ?><a href="<?=$link_catalogo?>" target="_blank" style="line-height: 150px;" id="catalogo_atual">Link do Catálogo Atual</a><?php }else{ ?><a id="catalogo_atual" href="#" target="_blank" style="line-height: 150px;">Nenhum catálogo cadastrado no momento</a> <?php } ?>
</div>
        </td>
       </tr>

       <tr>
 <td class="style1">
        	<b>Foto: </b><br>
            
        </td>
        <td>     <p class="legenda">Tamanho sugerido 800x600 pixels</p>
			<?php 
			 echo $div;
			?>
            
            <!--<div id="loadMore">Carregar Mais Fotos</div>-->
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
       <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/salvar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/salvar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/salvar.png');?>';" >
      
        <input type="hidden" name="status" id="status" value="<?php if($ativo=='SIM') echo 'NAO'; else echo 'SIM' ?>" /> 
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">        
        </td>
        </tr>
 </form>
<form action="<?php echo base_url('index.php/historiactrl/processupload');?>" method="post" enctype="multipart/form-data" id="MyUploadForm">
<div style="display:none">
<input name="catalogo" id="catalogo" type="file" onChange="upload_imagem('catalogo','catalogo_preview','catalogo_input','catalogo_status','catalogo_exclui','catalogo_progressbar');"/>
<?php 
$i = 0;
while($i<$qtd_arq){ $i++;?>
<input name="imagem<?php echo $i ?>" id="imagem<?php echo $i ?>" type="file" onChange="upload_imagem('imagem<?php echo $i ?>','imagem<?php echo $i ?>_preview','imagem<?php echo $i ?>_input','imagem<?php echo $i ?>_status','imagem<?php echo $i ?>_exclui','imagem<?php echo $i ?>_progressbar');"/>
<?php } ?>
<?php
$Random_Number = rand(0, 9999999999);
?>
<input name="rand" id="rand" type="text" value="<?php echo $Random_Number?>" />
</div>

</form>
       </table>
       <div id="error">Erro ao modificar!</div>
	   <div id="success">Modificação efetuada com sucesso!</div>   
</body>
<script>
$(document).ready(function(){
	size_li = $(".img_inner").size();
    x=20;
    $('.img_inner:lt('+x+')').show();
    $('#loadMore').click(function () {
        x= (x+5 <= size_li) ? x+5 : size_li;
        $('.img_inner:lt('+x+')').show();
        if(x == size_li){
            $('#loadMore').hide();
        }
    });


	$('#resumo').simplyCountable({
	  maxCount: 156,
	  strictMax: true	
	});
	
	var formData;
	
	$("#salvar").click(function() {
		 CKupdate();
  		 formData = $('#historiamodifica').serializeArray();
  		 formData.push({ name: 'salvar', value: 'salvar' });
		 console.log(formData);
		$("#salvar").hide();
		$("#ativo").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/historiactrl/EditaHistoria');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/historiactrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000); 
			  $("#ativo").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000); 
			 $("#ativo").delay(1800).fadeIn(1000);   
			}
		});
	event.preventDefault();
	});
	
	$("#ativo").click(function() {
		 CKupdate();
  		 formData = $('#historiamodifica').serializeArray();
  		 formData.push({ name: 'ativo', value: 'ativo' });
		 console.log(formData);
		$("#salvar").hide();
		$("#ativo").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/historiactrl/EditaHistoria');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();
			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/historiactrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000); 
			  $("#ativo").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000); 
			 $("#ativo").delay(1800).fadeIn(1000);   
			}
		});
	event.preventDefault();
	});
	
});
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}
function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "àáäâãèéëêìíïîòóöôõùúüûñç·/_,:;";
  var to   = "aaaaaeeeeiiiiooooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}
function upload_imagem(id_image,id_preview,id_input,id_status,id_exclui,id_progressbar){
	var options = {
			beforeSubmit:  beforeSubmit,  
			success:       afterSuccess,  
			uploadProgress: OnProgress,
		};
	
	$('#MyUploadForm').ajaxSubmit(options);  			
	return false; 
	
function afterSuccess()
{
	$('#'+id_progressbar).width('0%')
	$('#submit-btn').show(); 
	$('#'+id_status).hide(); 
	var nomearq = $('#'+id_image)[0].files[0].name.split('.');
	var ext = $('#'+id_image)[0].files[0].name.split('.').pop().toLowerCase();
	var bd = '<?php echo base_url('public/img');?>'+'/thumb-'+string_to_slug(nomearq[0])+'-'+$('#rand').val()+'.'+ext;
	if(id_preview=="catalogo_preview"){
    console.log("colocando OK no catalogo");
    console.log('#'+id_preview);
    $('#'+id_preview).css({
        'background-image' : 'url("<?php echo base_url('public').'/ok.png';?>")',
        'background-size'  : 'cover'
    });
    $('#catalogo_atual').attr("href", bd);
  }else{
    console.log("colocando OK na imagem");
    console.log('#'+id_preview);
    $('#'+id_preview).css({
          'background-image' : 'url('+bd+')',
          'background-size'  : 'cover'
      });
  }
	$('#'+id_exclui).show();

}

function beforeSubmit(){
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		if( !$('#'+id_image).val()) 
		{
			$("#output").html("Nenhum Arquivo encontrado");
			return false
		}
		$('#'+id_exclui).hide();
		var nomearq = $('#'+id_image)[0].files[0].name.split('.');
		var ext = $('#'+id_image)[0].files[0].name.split('.').pop().toLowerCase();
		var bd = "thumb-"+string_to_slug(nomearq[0])+'-'+$('#rand').val()+'.'+ext;
		$('#'+id_preview).css('backgroundImage','url()');
		$('#'+id_preview).css('background-size','auto 30px');
		$('#'+id_input).val(bd)
		var fsize = $('#'+id_image)[0].files[0].size; 
		var ftype = $('#'+id_image)[0].files[0].type; 
		


	}
	
}

function OnProgress(event, position, total, percentComplete)
{
    $('#'+id_status).show();
	$('#'+id_progressbar).width(percentComplete + '%')
    $('#'+id_status).html(percentComplete + '%');
    if(percentComplete>1)
        {
            $('#'+id_status).css('color','#000');
        }
}

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
}		
function apaga(id_image,id_preview,id_input,id_status,id_exclui) {
   $('#'+id_exclui).hide();
   $('#'+id_input).val('');
   $('#'+id_image).val('');
   $('#'+id_preview).css('backgroundImage','url(<?php echo base_url('public/camera.png');?>)');
   $('#'+id_preview).css('background-size','auto 30px');
   event.stopPropagation();     
}	
</script>
<?php
echo display_ckeditor($ckeditor['ckeditor']);
?>