<?php 
$linkAtivo=array(
            'certificado' => 'active',
        );
        $this->load->view('header',$linkAtivo);
$qtd_arq = 20;

function slug($string) {
	return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
?>
<script type="text/javascript" src="<?php echo base_url('public/js/jquery.form.min.js'); ?>"></script>
<style>
table, th, td
    {
    border:0px solid #ff9900;
    padding:5px;
    }
table
    {
    border-spacing:15px;
	margin-bottom:50px
    }
.btn-warning 
    {
    color: #000;
    }
img{
width: initial;
height: initial;	
}	
</style>
<body> 
<h2 align="center">Área de Modificação de Certificado</h2></br>
<table align="center" style="max-width: 900px;">

 

    <form method="post" accept-charset="UTF-8" enctype="multipart/form-data" id="certificadomodifica">
    <div>              
    </div>
    <div style="display:none">
    <input type="text" name="qtd_arq" id="qtd_arq" value="<?php echo $qtd_arq; ?>"/>
    <?php 
	echo $input;
	?>
      </div>

    <tr>
	  <td class= "style1">
	        <b>Ativar</b>
          </td>
          <td>
          <input name="ativo" type="checkbox" <?=(!isset($ativo)||$ativo==1)?'checked':''?> />
	      </td>
        </tr>
		<tr>
    <td class="style1">
          <b>Idioma: </b>
        </td>
        <td>
          <select id="idioma" name="idioma">
            <option value="PT" <?php if($idioma == "PT") echo "selected"?>>Português</option>
            <option value="ES" <?php if($idioma == "ES") echo "selected"?>>Espanhol</option>
            <option value="EN" <?php if($idioma == "EN") echo "selected"?>>Inglês</option>
          </select>
        </td>
       </tr>

         <tr>
		<td class="style1">
        	<b>Descrição: </b>
        </td>
        <td>
        	<input type="text" name="descricao" id="descricao" style="width: 400px" autocomplete="off" value="<?=$descricao?>"/>
        </td>
       </tr>
       
 <td class="style1">
        	<b>Arquivo: </b>
        </td>
        <td>     
       <?php 
		echo $div;
		?>
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2">&nbsp;        
        </td>
        </tr>
       <tr>
        <td align="center" colspan="2" style="padding-top: 30px;"> 
        <input type="hidden" name="id" id="id" value="<?php echo $id ?>" />
        <img id="salvar" style="cursor:pointer;" src="<?php echo base_url('public/imagens/salvar.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/salvar select.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/salvar.png');?>';" >
	    <img id="exclui" style="cursor:pointer;width: 143px;height: 29px;" src="<?php echo base_url('public/imagens/excluir.png');?>" width="180" height="24" onMouseOver="this.src = '<?php echo base_url('public/imagens/excluir_ativo.png');?>';" onMouseOut="this.src = '<?php echo base_url('public/imagens/excluir.png');?>';" >  
        <img src="<?php echo base_url('public/loader.GIF');?>" id="loader" style="display:none;">
   
        </td>
        </tr>
 </form>
<form action="<?php echo base_url('index.php/certificadoctrl/processupload');?>" method="post" enctype="multipart/form-data" id="MyUploadForm">
<div style="display:none">
<?php 
$i = 0;
while($i<$qtd_arq){ $i++;?>
<input name="imagem<?php echo $i ?>" id="imagem<?php echo $i ?>" type="file" onChange="upload_imagem('imagem<?php echo $i ?>','imagem<?php echo $i ?>_preview','imagem<?php echo $i ?>_input','imagem<?php echo $i ?>_status','imagem<?php echo $i ?>_exclui','imagem<?php echo $i ?>_progressbar');"/>
<?php } ?>
<?php
$Random_Number = rand(0, 9999999999);
?>
<input name="rand" id="rand" type="text" value="<?php echo $Random_Number?>" />
</div>

</form>

       </table>
       <div id="error">Erro ao modificar!</div>
	   <div id="success">Modificação efetuada com sucesso!</div>   
</body>
<script>
$(document).ready(function(){
	var formData;


	$("#salvar").click(function() {
  		 formData = $('#certificadomodifica').serializeArray();
  		 formData.push({ name: 'salvar', value: 'salvar' });
		 console.log(formData);
		$("#salvar").hide();
		$("#exclui").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/certificadoctrl/EditaCertificado');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/certificadoctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#exclui").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000);
			 $("#exclui").delay(1800).fadeIn(1000);			    
			}
		});
	event.preventDefault();
	});
	
	$("#exclui").click(function() {
  		 formData = $('#certificadomodifica').serializeArray();
  		 formData.push({ name: 'exclui', value: 'exclui' });
		 console.log(formData);
		$("#salvar").hide();
		$("#exclui").hide();
		$("#loader").show();
		$.ajax({
			url:'<?php echo base_url('index.php/certificadoctrl/EditaCertificado');?>',
			data:formData,   
			type:'POST',
			success:function(data){
			 console.log(data);
			 $("#loader").hide();			 
			 if($.trim(data)=="s"){
			  $("#success").delay(1000).show().fadeOut(1000);
			  setTimeout(function() {
  				window.location.href = "<?php echo base_url('index.php/certificadoctrl');?>";
			 }, 1800);
			 }
			 else{
			  $("#error").delay(1000).show().fadeOut(1000);
			  $("#salvar").delay(1800).fadeIn(1000);
			  $("#exclui").delay(1800).fadeIn(1000); 
			 }
			    
			 },
			error:function(jqXHR, textStatus, errorThrown){
			 console.log(JSON.stringify(jqXHR) + ' ' + textStatus +'  '+errorThrown );
			 $("#loader").hide();
			 $("#error").delay(1000).show().fadeOut(1000);
			 $("#salvar").delay(1800).fadeIn(1000);
			 $("#exclui").delay(1800).fadeIn(1000);			    
			}
		});
	event.preventDefault();
	});
});
function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}
function string_to_slug(str) {
  str = str.replace(/^\s+|\s+$/g, ''); // trim
  str = str.toLowerCase();
  
  // remove accents, swap ñ for n, etc
  var from = "àáäâãèéëêìíïîòóöôõùúüûñç·/_,:;";
  var to   = "aaaaaeeeeiiiiooooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

  return str;
}
function upload_imagem(id_image,id_preview,id_input,id_status,id_exclui,id_progressbar){
	var options = {
			beforeSubmit:  beforeSubmit,  
			success:       afterSuccess,  
			uploadProgress: OnProgress,
		};
	
	$('#MyUploadForm').ajaxSubmit(options);  			
	return false; 
	
function afterSuccess()
{
	$('#'+id_progressbar).width('0%')
	$('#submit-btn').show(); 
	$('#'+id_status).hide(); 
	var nomearq = $('#'+id_image)[0].files[0].name.split('.');
	var ext = $('#'+id_image)[0].files[0].name.split('.').pop().toLowerCase();
	var bd = '<?php echo base_url('public/ok.png');?>';
	$('#'+id_preview).css({
        'background-image' : 'url('+bd+')',
        'background-size'  : 'cover'
    });
	$('#'+id_exclui).show();

}

function beforeSubmit(){
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		if( !$('#'+id_image).val()) 
		{
			$("#output").html("Nenhum Arquivo encontrado");
			return false
		}
		$('#'+id_exclui).hide();
		var nomearq = $('#'+id_image)[0].files[0].name.split('.');
		var ext = $('#'+id_image)[0].files[0].name.split('.').pop().toLowerCase();		
		var bd = string_to_slug(nomearq[0])+'.'+ext;
		$('#'+id_preview).css('backgroundImage','url()');
		$('#'+id_preview).css('background-size','auto 30px');
		$('#'+id_input).val(bd)
		var fsize = $('#'+id_image)[0].files[0].size; 
		var ftype = $('#'+id_image)[0].files[0].type; 
		
 
	}
	
}

function OnProgress(event, position, total, percentComplete)
{
    $('#'+id_status).show();
	$('#'+id_progressbar).width(percentComplete + '%')
    $('#'+id_status).html(percentComplete + '%');
    if(percentComplete>1)
        {
            $('#'+id_status).css('color','#000');
        }
}

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
}		
function apaga(id_image,id_preview,id_input,id_status,id_exclui) {
   $('#'+id_exclui).hide();
   $('#'+id_input).val('');
   $('#'+id_image).val('');
   $('#'+id_preview).css('backgroundImage','url(<?php echo base_url('public/nuvem.png');?>)');
   $('#'+id_preview).css('background-size','auto 30px');
   event.stopPropagation();     
}	
</script>
