<style>
       .navbar-inner {
            background: pink;
            background-image: none;
            background-repeat: repeat-x;
            border-color: none;
            filter: none;
       }
       .navbar-inverse {
            background-image: linear-gradient(to bottom,#DD7BEB 0,#E992D8 100%);
            border-color: none;
            filter: none;
       }
       .navbar {
            margin-bottom: 0px;
       }
       body { padding-bottom: 70px; }
</style>
<div class="navbar navbar-static navbar-inverse">

            <div class="navbar-inner">
                <div class="container-fluid"></div>
                <div class="container" style="margin-top: 8px;" >
                    <p class="text-gray pull-left">&copy; <?php echo date('Y'); ?> - Isadora Site</p>
                    <p class="text-gray pull-right">Created by Lincoln Borges</p>
                </div>
                
            </div>
        </div>

    </body>
</html>