<?php
Class Usuario extends CI_Model
{
// ------------------- PRevenda ---------------------
function GetPRevenda($palavra){


    $this->db->select('*');
    $this->db->from('prevenda');
    $this->db->like('referencia',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
function InserePRevenda($data){
     $this->db->insert('prevenda',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
function GetPRevendaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('prevenda',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
function ModificaPRevendaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('prevenda', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
//----------------------------------------------------------
//------------------Funcoes PEmpresa ------------------
 function GetPEmpresa($palavra){


    $this->db->select('*');
    $this->db->from('pempresa');
    $this->db->like('nome',$palavra);
	$this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InserePEmpresa($data){
     $this->db->insert('pempresa',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetPEmpresaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('pempresa',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaPEmpresaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('pempresa', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

  //------------------Funcoes banner ------------------
 function GetBanner($palavra){


    $this->db->select('*');
    $this->db->from('banner');
    $this->db->like('link',$palavra);
  $this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereBanner($data){
     $this->db->insert('banner',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetBannerPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('banner',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaBannerPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('banner', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  function ExcluiBannerPeloID($data){
    $this->db->delete('banner', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

 //------------------Funcoes banner ------------------
 function GetBloco($palavra){


    $this->db->select('*');
    $this->db->from('bloco');
    $this->db->like('descricao',$palavra);
  $this->db->or_like('titulo',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereBloco($data){
     $this->db->insert('bloco',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetBlocoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('bloco',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaBlocoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('bloco', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  function ExcluiBlocoPeloID($data){
    $this->db->delete('bloco', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  
 //----------------------------------------------------------
 function InsereFotoHistoria($data){
     $this->db->insert('foto_historia',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
  return false;
 }
 function GetFotoHistoria($id){
    $query = $this->db->get_where('foto_historia',array('id_historia' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoHistoria($id){
    $this->db->where('id_historia', $id);
  $this->db->delete('foto_historia'); 
 }
 //----------------------------------------------------------
 //------------------Funcoes Historia ------------------
  function GetHistoria($palavra){


    $this->db->select('*');
    $this->db->from('historia');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('resumo',$palavra);
    $this->db->or_like('descricao',$palavra);
  $this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereHistoria($data){
     $this->db->insert('historia',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetHistoriaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('historia',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaHistoriaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('historia', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  


 //----------------------------------------------------------
 function InsereFotoServico($data){
     $this->db->insert('foto_servico',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
  return false;
 }
 function GetFotoServico($id){
    $query = $this->db->get_where('foto_servico',array('id_servico' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoServico($id){
    $this->db->where('id_servico', $id);
  $this->db->delete('foto_servico'); 
 }
 //----------------------------------------------------------

 //------------------Funcoes Serviços ------------------
  function GetServico($palavra){
    $this->db->select('*');
    $this->db->from('servico');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('resumo',$palavra);
    $this->db->or_like('descricao',$palavra);
  $this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereServico($data){
     $this->db->insert('servico',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetServicoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('servico',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaServicoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('servico', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
   function ExcluiServicoPeloID($data){
    $this->db->delete('servico', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

 //----------------------------------------------------------
 function InsereFotoPsuporte($data){
     $this->db->insert('foto_psuporte',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
  return false;
 }
 function GetFotoPsuporte($id){
    $query = $this->db->get_where('foto_psuporte',array('id_psuporte' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoPsuporte($id){
    $this->db->where('id_psuporte', $id);
  $this->db->delete('foto_psuporte'); 
 }
 //----------------------------------------------------------

 //------------------Funcoes Psuporte ------------------
  function GetPsuporte($palavra){
    $this->db->select('*');
    $this->db->from('psuporte');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('resumo',$palavra);
    $this->db->or_like('descricao',$palavra);
  $this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InserePsuporte($data){
     $this->db->insert('psuporte',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetPsuportePeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('psuporte',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaPsuportePeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('psuporte', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
   function ExcluiPsuportePeloID($data){
    $this->db->delete('psuporte', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

//------------------Funcoes Profissao ------------------
 function GetProfissao($palavra){


    $this->db->select('*');
    $this->db->from('profissao');
    $this->db->like('prf_nome',$palavra);
	$this->db->order_by("prf_nome", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereProfissao($data){
     $this->db->insert('profissao',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetProfissaoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('profissao',array('prf_codigo' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaProfissaoPeloID($data){
    $this->db->where('prf_codigo', $data['prf_codigo']);
    $query = $this->db->update('profissao', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  
//------------------Funcoes Curriculo ------------------
 function GetCurriculo($palavra){

    $query = $this->db->query("SELECT cur_codigo,
					  cur_nome,
      			  CASE cur_experiencia1 WHEN '1' THEN 'Sem experiência'
                  			   		   WHEN '2' THEN 'Pouca experiência' 
                  			   		   WHEN '3' THEN 'Média experiência' 
                  			            WHEN '4' THEN 'Muita experiência' 										
					  END experiencia1,
      			  CASE cur_experiencia2 WHEN '1' THEN 'Sem experiência'
                  			   		   WHEN '2' THEN 'Pouca experiência' 
                  			   		   WHEN '3' THEN 'Média experiência' 
                  			            WHEN '4' THEN 'Muita experiência' 										
					  END experiencia2,
      			  CASE cur_experiencia3 WHEN '1' THEN 'Sem experiência'
                  			   		   WHEN '2' THEN 'Pouca experiência' 
                  			   		   WHEN '3' THEN 'Média experiência' 
                  			            WHEN '4' THEN 'Muita experiência' 										
					  END experiencia3,
      			  CASE cur_sexo         WHEN 'M' THEN 'Masculino'
                  			   		   WHEN 'F' THEN 'Feminino' 
					  END sexo,
					  cur_cpf,
      			  CASE cur_estado_civil WHEN '1' THEN 'Solteiro(a)'
                  			   		   WHEN '2' THEN 'Casado(a)' 
                  			   		   WHEN '3' THEN 'Amasiado(a)' 
                  			            WHEN '4' THEN 'Divorciado(a)' 										
                  			            WHEN '5' THEN 'Viúvo(a)' 										
					  END estado_civil,
					  cur_nacionalidade,
                 date_format(cur_data_nasc,'%d/%m/%Y') data_nasc,	
					  cur_endereco,
					  cur_numero_end,
					  cur_complemento,
					  cur_bairro,
					  cur_estado,
					  cur_cidade,
					  cur_cep,				  
					  cur_telefone_res,				  
					  cur_telefone_cel,				  
					  cur_telefone_rec,				  
					  cur_recado,				  
					  cur_email,
					  esc.esc_codigo,
					  esc.esc_nome,
					  cur_curso_tec,				  
					  cur_curso_tec_nome,				  
					  cur_curso_tec_instituicao,				  
      			  CASE cur_curso_tec_situacao WHEN 'C' THEN 'Completo'
                  			   		         WHEN 'E' THEN 'Em Curso' 
                  			   		         WHEN 'I' THEN 'Incompleto' 
					  END cur_curso_tec_situacao,		  
					  cur_curso_tec_conclusao,				  
					  cur_graduacao,				  
					  cur_graduacao_nome,				  
					  cur_graduacao_instituicao,				  
      			  CASE cur_graduacao_situacao WHEN 'C' THEN 'Completo'
                  			   		         WHEN 'E' THEN 'Em Curso' 
                  			   		         WHEN 'I' THEN 'Incompleto' 
					  END cur_graduacao_situacao,		  
					  cur_graduacao_conclusao,				  
					  cur_pos_graduacao,				  
					  cur_pos_graduacao_nome,				  
					  cur_pos_graduacao_instituicao,				  
      			  CASE cur_pos_graduacao_situacao WHEN 'C' THEN 'Completo'
                  			   		             WHEN 'E' THEN 'Em Curso' 
                  			   		             WHEN 'I' THEN 'Incompleto' 
					  END cur_pos_graduacao_situacao,		  
					  cur_pos_graduacao_conclusao,	
					  cur_ingles,
      			  CASE cur_ingles_nivel WHEN 'B' THEN 'Básico'
                  			   		   WHEN 'I' THEN 'Intermediário' 
                  			   		   WHEN 'A' THEN 'Avançado' 
					  END cur_ingles_nivel,		  
					  cur_espanhol,
      			  CASE cur_espanhol_nivel WHEN 'B' THEN 'Básico'
                  			   		     WHEN 'I' THEN 'Intermediário' 
                  			   		     WHEN 'A' THEN 'Avançado' 
					  END cur_espanhol_nivel,		  
					  cur_outros_idiomas,
					  cur_outros_lingua,			  
      			  CASE cur_outros_nivel WHEN 'B' THEN 'Básico'
                  			   		   WHEN 'I' THEN 'Intermediário' 
                  			   		   WHEN 'A' THEN 'Avançado' 
					  END cur_outros_nivel,		  
					  cur_informatica,
					  cur_profis1,		  
					  cur_profis2,		  
					  cur_profis3,		  
					  cur_profis4,	
					  cur_empresa1,
					  cur_empresa1_per1,	  
					  cur_empresa1_per2,	
					  cur_empresa1_atual,	
					  cur_empresa1_ativ,	
					  cur_empresa1_cargo,	
					  cur_empresa2,
					  cur_empresa2_per1,	  
					  cur_empresa2_per2,	
					  cur_empresa2_atual,	
					  cur_empresa2_ativ,	  
					  cur_empresa2_cargo,
					  cur_deficiencia,	  
					  cur_deficiencia1,	  
					  cur_deficiencia2,	  
					  cur_deficiencia3,	  
					  cur_deficiencia4,	  
					  cur_deficiencia5,	  
					  cur_deficiencia6,	  
					  cur_deficiencia7,	  
					  cur_deficiencia_outros,	  
					  cur_autorizacao,
					  cur_senha,
					  cur_cargo_observacao,	  
				     Concat('R$ ',Replace(Replace(Replace(Format(cur_pretensao,2),'.','|'),',','.'),'|',',')) pretensao,
			        prf1.prf_nome prof1,
			        prf2.prf_nome prof2,
			        prf3.prf_nome prof3
			   FROM curriculo cur
			        LEFT JOIN profissao    prf1 ON cur.prf_codigo1 = prf1.prf_codigo
			        LEFT JOIN profissao    prf2 ON cur.prf_codigo2 = prf2.prf_codigo
			        LEFT JOIN profissao    prf3 ON cur.prf_codigo3 = prf3.prf_codigo
			        LEFT JOIN escolaridade esc  ON cur.esc_codigo  = esc.esc_codigo
					where prf1.prf_nome like '%".$palavra."%' or
			        prf2.prf_nome like '%".$palavra."%' or
			        prf3.prf_nome like '%".$palavra."%' or
			        cur_nome like '%".$palavra."%' or
					cur_cidade like '%".utf8_encode($palavra)."%'");
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
//------------------Funcoes Artigo ------------------
 function GetArtigo($palavra){


    $this->db->select('*');
    $this->db->from('artigo');
    $this->db->like('referencia',$palavra);
	$this->db->order_by("pos", "asc"); 
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereArtigo($data){
     $this->db->insert('artigo',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetArtigoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('artigo',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaArtigoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('artigo', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  
// ------------------- Certificacao ---------------------
function GetCertificacao($palavra){


    $this->db->select('*');
    $this->db->from('certificacao');
    $this->db->like('referencia',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
function InsereCertificacao($data){
     $this->db->insert('certificacao',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
function GetCertificacaoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('certificacao',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
function ModificaCertificacaoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('certificacao', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
//----------------------------------------------------------

 //----------------------------------------------------------
 function InsereFotoAacategoriaProduto($data){
     $this->db->insert('foto_aacategoria',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
  return false;
 }
 function GetFotoAacategoriaoProduto($id){
    $query = $this->db->get_where('foto_aacategoria',array('id_aacategoria' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoAacategoriaProduto($id){
    $this->db->where('id_aacategoria', $id);
  $this->db->delete('foto_aacategoria'); 
 }
 //----------------------------------------------------------
//----------------------------------------------------------
 function InsereFotoSegmentoProduto($data){
     $this->db->insert('foto_segmento_produto',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
	return false;
 }
 function GetFotoSegmentoProduto($id){
    $query = $this->db->get_where('foto_segmento_produto',array('id_segmentoproduto' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoSegmentoProduto($id){
    $this->db->where('id_segmentoproduto', $id);
	$this->db->delete('foto_segmento_produto'); 
 }
 //----------------------------------------------------------
//----------------------------------------------------------
 function InsereFotoProduto($data){
     $this->db->insert('foto_produto',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
	return false;
 }
 function GetFotoProduto($id){
    $query = $this->db->get_where('foto_produto',array('id_produto' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoProduto($id){
    $this->db->where('id_produto', $id);
	$this->db->delete('foto_produto'); 
 }
 //----------------------------------------------------------
//------------------Funcoes Case ------------------
 function GetCase($palavra){


    $this->db->select('*');
    $this->db->from('case');
    $this->db->like('referencia',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereCase($data){
     $this->db->insert('case',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetCasePeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('case',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
  function GetCaseImagemPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('foto_caseimagem',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaCasePeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('case', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  function ModificaCaseImagemPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('foto_caseimagem', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

   function GetAniversarioImagemPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('foto_aniversario',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
   function ModificaAniversarioImagemPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('foto_aniversario', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
//----------------------------------------------------------
 function InsereFotoAaproduto($data){
     $this->db->insert('foto_aaproduto',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
  return false;
 }
 function GetFotoAaproduto($id){
    $query = $this->db->get_where('foto_aaproduto',array('id_aaproduto' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFotoAaproduto($id){
    $this->db->where('id_aaproduto', $id);
  $this->db->delete('foto_aaproduto'); 
 }
 //----------------------------------------------------------

 function InsereFoto($data){
     $this->db->insert('foto',$data);
     if ($this->db->affected_rows() > 0) {
       return TRUE;
    }
	return false;
 }
 function GetFoto($id){
    $query = $this->db->get_where('foto',array('id_evento' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
  function DeletaFoto($id){
    $this->db->where('id_evento', $id);
	$this->db->delete('foto'); 
 }
 // ---------------- Seleciona Ultimo ID -------------------
    function GetLastID($tabela){
     $next = $this->db->query("SHOW TABLE STATUS LIKE '".$tabela."'");
     $next = $next->row(0);
     return $next->Auto_increment;
     }
	 function GetLastPosicaoImg($codigo){
	  // $next = $this->db->query("SELECT MAX(  `posicao` )  FROM imagens WHERE  `codigo` =  '".$codigo."'");
//       $next = $next->row(0);
	 //  return $next->result();
	 $this->db->select('MAX(posicao) AS time',false);
	$result = $this->db->get_where('imagens',array('codigo' => $codigo))->row();  
	return $result->time;
     }
 //---------------- Funcao checa login --------------
 function login($username, $password)
 {
   $this -> db -> select('*');
   $this -> db -> from('login');
   $this -> db -> where('login', $username);
   $this -> db -> where('senha', MD5($password));
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
  function GetLogin($username)
 {
   $this -> db -> select('*');
   $this -> db -> from('login');
   $this -> db -> where('login', $username);
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 
 function login_provisorio($username, $password){
   $this -> db -> select('*');
   $this -> db -> from('login');
   $this -> db -> where('login', $username);
   $this -> db -> where('senha_provisoria', MD5($password));
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
    function InsereLogin($data){
     $this->db->insert('login',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 
 //----------------------------------------------------------
 //------------------- Funcao Altera Senha ------------------
 
 function AlteraSenhaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('login', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 // ----------------------- Perfil ---------------------
 function GetPerfil()
 {
   $this -> db -> select('*');
   $this -> db -> from('perfil');
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
  function ModificaPerfil($data){
    $query = $this->db->update('perfil', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
    function GetImgPerfil(){
	  $query = $this->db->get_where('imagens',array('tabela' => 'perfil'));
	  
	  if($query->num_rows >= 1)
	  {
		  return $query->result();
	  }
	  return false;
   }
       function GetCVPerfil(){
	  $query = $this->db->get_where('imagens',array('tabela' => 'arquivo'));
	  
	  if($query->num_rows >= 1)
	  {
		  return $query->result();
	  }
	  return false;
   }
     function ModificaRedeSocial($data){
    $query = $this->db->update('redesocial', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 function GetRedeSocial()
 {
   $this -> db -> select('*');
   $this -> db -> from('redesocial');
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
  function GetEmail()
 {
   $this -> db -> select('*');
   $this -> db -> from('email');
   $this -> db -> limit(1);

   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 function ModificaEmail($data){
    $query = $this->db->update('email', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 //-----------------------------------------------------
 //------------------Funcoes Pcase ------------------
 function GetPcase($palavra){


    $this->db->select('*');
    $this->db->from('pcase');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('descricao',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InserePcase($data){
     $this->db->insert('pcase',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetPcasePeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('pcase',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaPcasePeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('pcase', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
    function ExcluiPcasePeloID($data){
    $this->db->delete('pcase', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------
//------------------Funcoes noticia ------------------
 function GetNoticia($palavra){


    $this->db->select('*');
    $this->db->from('noticia');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('resumo',$palavra);
    $this->db->or_like('descricao',$palavra);
  $this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereNoticia($data){
     $this->db->insert('noticia',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetNoticiaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('noticia',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaNoticiaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('noticia', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

 //------------------Funcoes representante ------------------
 function GetRepresentante($palavra){


    $this->db->select('*');
    $this->db->from('representante');
    $this->db->like('empresa',$palavra);
    $this->db->or_like('nome_representante',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereRepresentante($data){
     $this->db->insert('representante',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetRepresentantePeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('representante',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaRepresentantePeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('representante', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
    function ExcluiRepresentantePeloID($data){
    $this->db->delete('representante', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

//------------------Funcoes tecnologia ------------------
 function GetTecnologia($palavra){


    $this->db->select('*');
    $this->db->from('tecnologia');
    $this->db->like('titulo',$palavra);
	$this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereTecnologia($data){
     $this->db->insert('tecnologia',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetTecnologiaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('tecnologia',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaTecnologiaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('tecnologia', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

//------------------Funcoes aacategoria ------------------
 function GetAacategoria($palavra){


    $this->db->select('*');
    $this->db->from('aacategoria');
    $this->db->like('referencia',$palavra);
    $this->db->order_by("pos", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereAacategoria($data){
     $this->db->insert('aacategoria',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetAacategoriaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('aacategoria',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaAacategoriaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('aacategoria', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 
   function ExcluiAacategoriaPeloID($data){
    $this->db->delete('aacategoria', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------

    //------------------Funcoes segmentoproduto ------------------
    function GetSegmentoproduto($palavra){
        $this->db->select('*');
        $this->db->from('segmentoproduto');
        $this->db->like('referencia',$palavra);
        $this->db->order_by("pos", "asc");
        $query = $this->db->get();
        if($query->num_rows >= 1)
        {
            return $query->result();
        }
        return false;
    }
    function InsereSegmentoproduto($data){
        $this->db->insert('segmentoproduto',$data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
    }
    function GetSegmentoprodutoPeloID($id){
        $this -> db -> limit(1);
        $query = $this->db->get_where('segmentoproduto',array('id' => $id));

        if($query->num_rows == 1)
        {
            return $query->result();
        }
        return false;
    }
    function ModificaSegmentoprodutoPeloID($data){
        $this->db->where('id', $data['id']);
        $query = $this->db->update('segmentoproduto', $data);
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
    }
    function ExcluiSegmentoprodutoPeloID($data){
        $this->db->delete('segmentoproduto', array('id' => $data['id']));
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
    }
    //-----------------------------------------------------

    //------------------Funcoes areanegocio ------------------
    function GetAreanegocio($palavra){
        $this->db->select('*');
        $this->db->from('areanegocio');
        $this->db->like('referencia',$palavra);
        $this->db->order_by("pos", "asc");
        $query = $this->db->get();
        if($query->num_rows >= 1)
        {
            return $query->result();
        }
        return false;
    }
    function InsereAreanegocio($data){
        $this->db->insert('areanegocio',$data);
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
    }
    function GetAreanegocioPeloID($id){
        $this -> db -> limit(1);
        $query = $this->db->get_where('areanegocio',array('id' => $id));

        if($query->num_rows == 1)
        {
            return $query->result();
        }
        return false;
    }
    function ModificaAreanegocioPeloID($data){
        $this->db->where('id', $data['id']);
        $query = $this->db->update('areanegocio', $data);
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
    }
    function ExcluiAreanegocioPeloID($data){
        $this->db->delete('areanegocio', array('id' => $data['id']));
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
    }
    //-----------------------------------------------------

//------------------Funcoes Arquivo Canavieira ------------------
 function GetArquivo($palavra){


    $this->db->select('arquivo.*,produto.referencia as produto_referencia');
    $this->db->from('arquivo');
  $this->db->join('produto', 'produto.id = arquivo.produto', "left");
    $this->db->or_like('arquivo.descricao',$palavra);
    $this->db->or_like('arquivo.arquivo',$palavra);
    $this->db->order_by("produto.referencia", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereArquivo($data){
     $this->db->insert('arquivo',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetArquivoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('arquivo',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaArquivoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('arquivo', $data);

        return true;

 }
   function ExcluiArquivoPeloID($data){
    $this->db->delete('arquivo', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  
  //------------------Funcoes Arquivo Florestal ------------------
 function GetArquivoFlorestal($palavra){


    $this->db->select('arquivo_florestal.*,aaproduto.referencia as aaproduto_referencia');
    $this->db->from('arquivo_florestal');
  $this->db->join('aaproduto', 'aaproduto.id = arquivo_florestal.produto');
    $this->db->or_like('arquivo_florestal.descricao',$palavra);
    $this->db->or_like('arquivo_florestal.arquivo',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereArquivoFlorestal($data){
     $this->db->insert('arquivo_florestal',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetArquivoFlorestalPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('arquivo_florestal',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaArquivoFlorestalPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('arquivo_florestal', $data);

        return true;

 }
    function ExcluiArquivoFlorestalPeloID($data){
    $this->db->delete('arquivo_florestal', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  
//------------------Funcoes produto ------------------
 function GetProduto($palavra){


    $this->db->select('produto.*');
    $this->db->from('produto');
    $this->db->like('produto.referencia',$palavra);
    $this->db->order_by("pos", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }

  function GetProdutoComImagem($palavra){


    $this->db->select('produto.*,foto_produto.caminho,segmentoproduto.id as id_segmento, segmentoproduto.referencia as nome_segmento');
    $this->db->from('produto');
    $this->db->join('foto_produto', 'produto.id = foto_produto.id_produto');
    $this->db->join('segmentoproduto', 'segmentoproduto.id = produto.segmento');
    $this->db->like('produto.referencia',$palavra);
    $this->db->where('foto_produto.caminho <>', ''); 
    $this->db->group_by("produto.id"); 
    $this->db->order_by("pos", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereProduto($data){
     $this->db->insert('produto',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetProdutoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('produto',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaProdutoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('produto', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 
   function ExcluiProdutoPeloID($data){
    $this->db->delete('produto', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  
 //------------------Funcoes produto ------------------
 function GetAaproduto($palavra){


    $this->db->select('aaproduto.*');
    $this->db->from('aaproduto');
    $this->db->like('aaproduto.referencia',$palavra);
    $this->db->order_by("pos", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
   function GetAaprodutoComImagem($palavra){


    $this->db->select('aaproduto.*,foto_aaproduto.caminho');
    $this->db->from('aaproduto');
    $this->db->join('foto_aaproduto', 'aaproduto.id = foto_aaproduto.id_aaproduto');
    $this->db->like('aaproduto.referencia',$palavra);
    $this->db->where('foto_aaproduto.caminho <>', ''); 
    $this->db->group_by("aaproduto.id"); 
    $this->db->order_by("pos", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereAaproduto($data){
     $this->db->insert('aaproduto',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetAaprodutoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('aaproduto',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaAaprodutoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('aaproduto', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 
   function ExcluiAaprodutoPeloID($data){
    $this->db->delete('aaproduto', array('id' => $data['id'])); 
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  


//------------------Funcoes Solucao ------------------
 function GetSolucao($palavra){


    $this->db->select('*');
    $this->db->from('solucao');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('resumo',$palavra);
    $this->db->or_like('descricao',$palavra);
	$this->db->or_like('dt_hr',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
    function InsereSolucao($data){
     $this->db->insert('solucao',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetSolucaoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('solucao',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ModificaSolucaoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('solucao', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //-----------------------------------------------------  

 //------------------Funcoes Academico ------------------
 function GetAcademico($palavra){


    $this->db->select('*');
    $this->db->from('academico');
    $this->db->like('empresa',$palavra);
    $this->db->or_like('curso',$palavra);
    $this->db->or_like('descricao',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
 function InsereAcademico($data){
     $this->db->insert('academico',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetAcademicoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('academico',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
  function ModificaAcademicoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('academico', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 //------------------Funcoes Programacao ------------------
 function GetProgramacao($palavra){


    $this->db->select('*');
    $this->db->from('programacao');
    $this->db->like('nome',$palavra);
    $this->db->or_like('valor',$palavra);
    $this->db->or_like('cor',$palavra);
	$this->db->or_like('ativo',$palavra);
	$this->db->order_by("pos", "asc");
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
 function InsereProgramacao($data){
     $this->db->insert('programacao',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetProgramacaoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('programacao',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
  function ModificaProgramacaoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('programacao', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  //------------------Funcoes Linguas ------------------
 function GetIdioma($palavra){


    $this->db->select('*');
    $this->db->from('idioma');
    $this->db->like('nome',$palavra);
    $this->db->or_like('valor',$palavra);
    $this->db->or_like('cor',$palavra);
	$this->db->or_like('ativo',$palavra);
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
 function InsereIdioma($data){
     $this->db->insert('idioma',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 function GetIdiomaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('idioma',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
  function ModificaIdiomaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('idioma', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
  function OrdenaProgramacao($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('programacao', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }

 //-----------------------------------------------------
 // ---------------- Funcoes da Imagem -------------------
    function InsereImg($data){
	 //$data['posicao'] = (int)$this->GetLastPosicaoImg($data['codigo'])+1;
	 $query = $this->db->get_where('imagens',array('codigo' => $data['codigo'],'posicao' => $data['posicao']));
	  if($query->num_rows == 1)
	  {
		  $query = $this->db->delete('imagens', array('codigo' => $data['codigo'],'posicao' => $data['posicao']));
	  }
     $this->db->insert('imagens',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
   function GetImgPeloCodigo($codigo){
	  $query = $this->db->get_where('imagens',array('codigo' => $codigo));
	  
	  if($query->num_rows >= 1)
	  {
		  return $query->result();
	  }
	  return false;
   }
    function ExcluiFotoPeloNome($id){
    $query = $this->db->delete('imagens', array('img' => $id));
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 //----------------------------------------------------------
 // ---------------- Funcoes do EDITORIAL -------------------
   function InsereEditorial($data){
     $this->db->insert('editorial',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
 
 function GetEditorial($data,$palavra){
	if($data==0)
		$data=1;
    if($data == "1" or $data == "2"){
    $this->db->select('*');
    $this->db->from('editorial');
    $this->db->like('titulo',$palavra);
    $this->db->or_like('resumo',$palavra);
    $this->db->or_like('conteudo',$palavra);
    $query = $this->db->get();
    }
    else{
    $this->db->select('*');
    $this->db->from('editorial');
    $this->db->where('(`titulo` LIKE \'%'.$palavra.'%\' 
                        OR `resumo` LIKE \'%'.$palavra.'%\' 
                        OR `conteudo` LIKE \'%'.$palavra.'%\'
                      )'
                    );
    $this->db->where('privilegio',$data);
    $query = $this->db->get();
    }
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
 
  function GetEditorialPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('editorial',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
   function ChecaURL($tabela,$id){
    //$this -> db -> limit(1);
     $query = $this->db->get_where($tabela,array('id' => $id));
   /* foreach ($query->result() as $row)
	{ 
    	if($row->url1!="")
			return "url1";
		elseif($row->url2!="")
			 return "url2";
		else
			 return "url3";
			 
	}*/
	if ($query->num_rows() > 0)
	{
	   $row = $query->row_array();
	   return $row;
	}
    return false;
 }
 function ExcluiEditorialPeloID($id){
    $query = $this->db->delete('editorial', array('id' => $id));
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 
 function ModificaEditorialPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('editorial', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 // ------------------------------------------------------------
  // ---------------- Funcoes da Cidade -------------------
    function InsereCidade($data){
     $this->db->insert('cidade',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
    function GetCidade($palavra){
  
    if(!empty($palavra)){    
        $this->db->select('*');
        $this->db->from('cidade');
        $this->db->where('(`cidade` LIKE \'%'.$palavra.'%\'
                          )');
        $query = $this->db->get();
    }else{
        $query = $this->db->get('cidade');
    }
    if($this->db->affected_rows() > 0)
    {
        return $query->result();
    }
    return false;
 }
 
    function GetCidadeAtivo(){
  
    
        $this->db->select('*');
        $this->db->from('cidade');
        $this->db->where('(`cidade`.`ativo` = \'SIM\')');
        $query = $this->db->get();
  
    if($this->db->affected_rows() > 0)
    {
        return $query->result();
    }
    return false;
 }
 
  function GetCidadePeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('cidade',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        foreach ($query->result() as $row)
		 {
			return $row->cidade;
		 }
    }
    return false;
 }
 
     function ModificaCidadePeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('cidade', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 
  // ------------------------------------------------------------
  // ---------------- Funcoes da Carga -------------------
    function InsereCarga($data){
     $this->db->insert('carga',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
    function GetCarga($palavra){
  
    if(!empty($palavra)){    
        $this->db->select('*');
        $this->db->from('carga');
        $this->db->where('(`carga` LIKE \'%'.$palavra.'%\'
                          )');
        $query = $this->db->get();
    }else{
        $query = $this->db->get('carga');
    }
    if($this->db->affected_rows() > 0)
    {
        return $query->result();
    }
    return false;
 }
     function GetCargaAtivo(){
  
        $this->db->select('*');
        $this->db->from('carga');
        $this->db->where('(`carga`.`ativo` = \'SIM\')');
        $query = $this->db->get();

    if($this->db->affected_rows() > 0)
    {
        return $query->result();
    }
    return false;
 }
 
  function GetCargaPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('carga',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 
     function ModificaCargaPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('carga', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }

  // ------------------------------------------------------------
  // ---------------- Funcoes do Caminhao -------------------
    function InsereCaminhao($data){
     $this->db->insert('caminhao',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
    function GetCaminhao($palavra){
  
    if(!empty($palavra)){    
        $this->db->select('*');
        $this->db->from('caminhao');
        $this->db->where('(`caminhao` LIKE \'%'.$palavra.'%\'
                          )');
        $query = $this->db->get();
    }else{
        $query = $this->db->get('caminhao');
    }
    if($this->db->affected_rows() > 0)
    {
        return $query->result();
    }
    return false;
 }
 
      function GetCaminhaoAtivo(){
  
        $this->db->select('*');
        $this->db->from('caminhao');
        $this->db->where('(`caminhao`.`ativo` = \'SIM\')');
        $query = $this->db->get();

    if($this->db->affected_rows() > 0)
    {
        return $query->result();
    }
    return false;
 }
 
  function GetCaminhaoPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('caminhao',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 
     function ModificaCaminhaoPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('caminhao', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }

  // ------------------------------------------------------------
  // ---------------- Funcoes da Carga Front End -------------------
    function InsereCargaFrontEnd($data){
     $this->db->insert('cargafrontend',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
    function GetCargaFrontEnd($palavra){
  
    if(!empty($palavra)){    
        $this->db->select('*,c.cidade coleta,d.cidade destino,cargafrontend.id id_frontend,cargafrontend.ativo ativo_frontend');
		$this->db->from('cargafrontend');
		$this->db->join('caminhao', 'caminhao.id = cargafrontend.caminhao');
		$this->db->join('cidade c', 'c.id = cargafrontend.coleta');
		$this->db->join('cidade d', 'd.id = cargafrontend.destino');
		$this->db->join('produto', 'produto.id = cargafrontend.produto');
		$this->db->join('carga', 'carga.id = cargafrontend.carga');
        $this->db->where('(`cargafrontend`.`data` = \''.$palavra.'\')'); 
        $query = $this->db->get();
    }else{
        $this->db->select('*,c.cidade coleta,d.cidade destino,cargafrontend.id id_frontend,cargafrontend.ativo ativo_frontend');
		$this->db->from('cargafrontend');
		$this->db->join('caminhao', 'caminhao.id = cargafrontend.caminhao');
		$this->db->join('cidade c', 'c.id = cargafrontend.coleta');
		$this->db->join('cidade d', 'd.id = cargafrontend.destino');
		$this->db->join('produto', 'produto.id = cargafrontend.produto');
		$this->db->join('carga', 'carga.id = cargafrontend.carga');
		$this->db->order_by("data", "desc");
		$this->db->limit(50);
		$query = $this->db->get();
    }
    if($this->db->affected_rows() > 0)
    {	
        return $query->result();
    }
    return false;
 }
 
  function GetCargaFrontEndPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('cargafrontend',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 
     function ModificaCargaFrontEndPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('cargafrontend', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }

   // ------------------------------------------------------------
  // ---------------- Funcoes do SMS -------------------
  function GetCargaFrontEndSMS($palavra){
  
    if(!empty($palavra)){    
        $this->db->select('*,c.cidade coleta,d.cidade destino,cargafrontend.id id_frontend,cargafrontend.ativo ativo_frontend,m.rota rota');
		$this->db->from('cargafrontend');
		$this->db->join('caminhao', 'caminhao.id = cargafrontend.caminhao');
		$this->db->join('cidade c', 'c.id = cargafrontend.coleta');
		$this->db->join('cidade d', 'd.id = cargafrontend.destino');
		$this->db->join('produto', 'produto.id = cargafrontend.produto');
		$this->db->join('carga', 'carga.id = cargafrontend.carga');
		$this->db->join('caminhoneiro m', 'm.rota = cargafrontend.coleta',inner);
        $this->db->where('(`cargafrontend`.`data` = \''.$palavra.'\')'); 
        $query = $this->db->get();
    }else{
        $this->db->select('*,c.cidade coleta,d.cidade destino,cargafrontend.id id_frontend,cargafrontend.ativo ativo_frontend,m.rota rota');
		$this->db->from('cargafrontend');
		$this->db->join('caminhao', 'caminhao.id = cargafrontend.caminhao');
		$this->db->join('cidade c', 'c.id = cargafrontend.coleta');
		$this->db->join('cidade d', 'd.id = cargafrontend.destino');
		$this->db->join('produto', 'produto.id = cargafrontend.produto');
		$this->db->join('carga', 'carga.id = cargafrontend.carga');
		$this->db->join('caminhoneiro m', 'm.rota = cargafrontend.coleta',inner);
		$this->db->where('(`cargafrontend`.`ativo` = \'SIM\')'); 
		$this->db->order_by("data", "desc");
		$this->db->limit(50);
		$query = $this->db->get();
    }
    if($this->db->affected_rows() > 0)
    {	
        return $query->result();
    }
    return false;
 }
   function GetCaminhoneiroPelaRota($id){
    $query = $this->db->get_where('caminhoneiro',array('rota' => $id));
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
 
 
    function ExcluiTreinamentoPeloID($id){
    $query = $this->db->delete('treinamento', array('id' => $id));
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
    function InsereCalendario($data){
     $this->db->insert('calendario',$data);
     if ($this->db->affected_rows() > 0) {
      return TRUE;
    }
 }
   function GetCalendario($id){

  	$this->db->select('*,calendario.id as id_calendario,t.id as treinamento_id,calendario.ativo as cativo');
    $this->db->from('calendario');
    $this->db->like('id_treinamento',$id);
	$this->db->join('treinamento as t', 'calendario.id_treinamento = t.id', 'inner'); 
    $query = $this->db->get();
    if($query->num_rows >= 1)
    {
        return $query->result();
    }
    return false;
 }
 function GetCalendarioPeloID($id){
    $this -> db -> limit(1);
    $query = $this->db->get_where('calendario',array('id' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function GetCalendarioTreinamentoNome($id){
    //$this -> db -> limit(1);
    $query = $this->db->get_where('treinamento',array('calendario_treinamento.id_treinamento' => $id));
    
    if($query->num_rows == 1)
    {
        return $query->result();
    }
    return false;
 }
 function ExcluiCalendarioPeloID($id){
    $query = $this->db->delete('calendario_treinamento', array('id' => $id));
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
 function ModificaCalendarioPeloID($data){
    $this->db->where('id', $data['id']);
    $query = $this->db->update('calendario', $data);
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    return false;
 }
     // ------------------------------------------------------------
	 
	function DropTables(){
		$this->load->dbforge();
		$this->dbforge->drop_table('academico');
		$this->dbforge->drop_table('email');
		$this->dbforge->drop_table('idioma');
		$this->dbforge->drop_table('imagens');
		$this->dbforge->drop_table('perfil');
		$this->dbforge->drop_table('treinamento');
		$this->dbforge->drop_table('programacao');
		$this->dbforge->drop_table('redesocial');
	}
	function RestoreTables(){
		 $this->db->query("SET FOREIGN_KEY_CHECKS = 0");
	  $sql=file_get_contents(base_url('public/dbbkp.txt'));
      foreach (explode(";\n", $sql) as $sql) 
       {
         $sql = trim($sql);
          //echo  $sql.'<br/>============<br/>';
           if($sql) 
               {
                
				 $this->db->query($sql);
				// $this->db->query("SET FOREIGN_KEY_CHECKS = 1");
               } 
      } 
	}
	
	// ---------------- Funcoes de certificado -------------------
    function GetCertificado($palavra){


        $this->db->select('*');
        $this->db->from('certificado');
        $this->db->like('descricao',$palavra);
        $this->db->or_like('arquivo',$palavra);
        $query = $this->db->get();
        if($query->num_rows >= 1)
        {
            return $query->result();
        }
        return false;
     }

     function GetCertificadoPeloID($id){
        $this -> db -> limit(1);
        $query = $this->db->get_where('certificado',array('id' => $id));
        
        if($query->num_rows == 1)
        {
            return $query->result();
        }
        return false;
     }

     function InsereCertificado($data){
        $this->db->insert('certificado',$data);
        if ($this->db->affected_rows() > 0) {
         return TRUE;
       }
    }
    function ModificaCertificadoPeloID($data){
        $this->db->where('id', $data['id']);
        $query = $this->db->update('certificado', $data);
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
     }

     function ExcluiCertificadoPeloID($data){
        $this->db->delete('certificado', array('id' => $data['id'])); 
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        return false;
     }
    // ------------------------------------------------------------
}
?>
