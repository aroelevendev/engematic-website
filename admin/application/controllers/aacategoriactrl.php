<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AacategoriaCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= './public/img/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	
	switch(strtolower($_FILES[$key]['type']))
		{
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html': //html file
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
				break;
			default:
				die('Não tem Suporte para esse arquivo!'); 
	}
	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $File_Name_No_Ext.'_'.$Random_Number.$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}	
	
	$config['image_library'] = 'gd2';
	$config['source_image'] = $UploadDirectory.$NewFileName;
	$config['new_image'] = $UploadDirectory.'thumb_'.$NewFileName;
	$config['maintain_ratio'] = TRUE;

	
	$this->image_lib->initialize($config); 
	if (!$this->image_lib->resize()) {
    	echo $this->image_lib->display_errors();
	}else{
	 	echo "s";
		unlink($UploadDirectory.$NewFileName);
	}
	$this->image_lib->clear();
	
}
}

function slug($string) {
	  return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_Aacategoria))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetAacategoria($palavrachave);
   if($result)
   {
     $aacategoria['tabela'] = "<table border='1' align='center'>
	 						<col width='30%' />
							<col width='70%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Referencia</th>							  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {		 
			
       $aacategoria['tabela'] .= "<tr>".
	   				"<td align='center'>";
					
					if($row->titulo_pt=="" || $row->titulo_es=="" || $row->titulo_en=="")
			$aacategoria['tabela'] .= "<a href=".base_url('index.php/aacategoriactrl/EditaAacategoria_NOVO/'.$row->id).">
							<img src='".base_url('public/imagens/plus.png')."'>
						</a>";
			if($row->titulo_pt!=""){
				if($row->ativo_pt=="SIM")
				 $flag = base_url('public/imagens/flag_br.png');
				else
				 $flag = base_url('public/imagens/flag_br_n.png');
				 
	  			$aacategoria['tabela'] .= "<a href=".base_url('index.php/aacategoriactrl/EditaAacategoria_PT/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->titulo_en!=""){
				if($row->ativo_en=="SIM")
				 $flag = base_url('public/imagens/flag_en.png');
				else
				 $flag = base_url('public/imagens/flag_en_n.png');
				 
	  			$aacategoria['tabela'] .= "<a href=".base_url('index.php/aacategoriactrl/EditaAacategoria_EN/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->titulo_es!=""){
				if($row->ativo_es=="SIM")
				 $flag = base_url('public/imagens/flag_es.png');
				else
				 $flag = base_url('public/imagens/flag_es_n.png');
				 
	  			$aacategoria['tabela'] .= "<a href=".base_url('index.php/aacategoriactrl/EditaAacategoria_ES/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}
				
					
	   $aacategoria['tabela'] .= "</td>";
	   
	   $aacategoria['tabela'] .= "<td align='center'><a href=".base_url('index.php/aacategoriactrl/EditaReferenciaAacategoria/'.$row->id).">".$row->referencia."</a></td>".				
                    "</tr>";
     }
     $aacategoria['tabela'] .= "</table>";
   }else {
       $aacategoria['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $aacategoria['privilegio'] = $privilegio;
        else
            $aacategoria['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/aacategoria/busca.php',$aacategoria);

        echo $aacategoria['tabela'];
     
 }
 function NovoAacategoria()
 {
   if($this->input->post('referencia')){
   $this->load->helper(array('form'));	
   $info = array(
                 'referencia' => $this->input->post('referencia'),
                 'imagem1' => $this->input->post('imagem1_input'),
                 'imagem2' => $this->input->post('imagem2_input'),
                 );
   $result = $this->usuario->InsereAacategoria($info);

   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aacategoria/referencia_novo',$info);
   }
 }
 
  function EditaReferenciaAacategoria()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAacategoriaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
            'imagem1_input' => $row->imagem1,
            'imagem2_input' => $row->imagem2,
            );
            
        }
       
   }

   if($row->imagem1 != ""){
     $caminho = "img/".$row->imagem1;
     $caminhoinput = $row->imagem1;
     $cover = "background-size: cover;";
     $exclui_display = "block";
     }else{
     $caminho = "camera.png";
     $caminhoinput = "";
     $cover = "";
     $exclui_display = "none";     
     }
     $info['input'] = "<input name='imagem1_input' id='imagem1_input' type='text' value='".$caminhoinput."' />";
       $info['div'] = "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
    background-size: auto 30px;width: 450px;height:150px;background-repeat: no-repeat;
    cursor:pointer;border: 1px solid black;float: left;margin-bottom:5px;margin-right:5px;".$cover."' class='img_inner' 
    id='imagem1_preview'";
    $info['div'].= "onclick='document.getElementById(\"imagem1\").click()'>";
    $info['div'].= "<center>
            <label id='imagem1_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
    $info['div'].= "onClick='apaga(\"imagem1\",\"imagem1_preview\",\"imagem1_input\",\"imagem1_status\",\"imagem1_exclui\")'>";   
    $info['div'] .= "excluir</label>
            <div id='imagem1_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";

  if($row->imagem2 != ""){
     $caminho = "img/".$row->imagem2;
     $caminhoinput = $row->imagem2;
     $cover = "background-size: cover;";
     $exclui_display = "block";
     }else{
     $caminho = "camera.png";
     $caminhoinput = "";
     $cover = "";
     $exclui_display = "none";     
     }
     $info['input'] .= "<input name='imagem2_input' id='imagem2_input' type='text' value='".$caminhoinput."' />";
       $info['div'] .= "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
    background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
    cursor:pointer;border: 1px solid black;float: left;margin-bottom:5px;margin-right:5px;".$cover."' class='img_inner' 
    id='imagem2_preview'";
    $info['div'].= "onclick='document.getElementById(\"imagem2\").click()'>";
    $info['div'].= "<center>
            <label id='imagem2_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
    $info['div'].= "onClick='apaga(\"imagem2\",\"imagem2_preview\",\"imagem2_input\",\"imagem2_status\",\"imagem2_exclui\")'>";   
    $info['div'] .= "excluir</label>
            <div id='imagem2_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";

   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aacategoria/referencia_modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
                 'referencia' => $this->input->post('referencia'),
                 'imagem1' => $this->input->post('imagem1_input'),
                 'imagem2' => $this->input->post('imagem2_input'),
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
      if($result){
        echo "s";
     }else{
          echo "n: ";
    }
   } 
   
	if($this->input->post('exclui')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 );
   $result = $this->usuario->ExcluiAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }


 }
 
 function EditaAacategoria_NOVO()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAacategoriaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => '',
			'descricao' => '',
			'resumo' => '',
			'lingua' => '_NOVO',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aacategoria/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo'.$this->input->post('idioma') => $this->input->post('titulo'),
				 'descricao'.$this->input->post('idioma') => $this->input->post('descricao'),
				 'resumo'.$this->input->post('idioma') => $this->input->post('resumo'),				 
				 'slug'.$this->input->post('idioma') => $this->slug($this->input->post('titulo')),
				 'ativo'.$this->input->post('idioma') => 'SIM',
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 }
 
  function EditaAacategoria_PT()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAacategoriaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => $row->titulo_pt,
			'resumo' => $row->resumo_pt,
			'descricao' => $row->descricao_pt,
			'lingua' => '_PT',
			'ativo' => $row->ativo_pt,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aacategoria/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo_pt' => $this->input->post('titulo'),
				 'resumo_pt' => $this->input->post('resumo'),
				 'descricao_pt' => $this->input->post('descricao'),
				 'slug_pt' => $this->slug($this->input->post('titulo')),
				 'ativo_pt' => 'SIM',
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_pt' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
 
 function EditaAacategoria_ES()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAacategoriaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => $row->titulo_es,
			'resumo' => $row->resumo_es,
			'descricao' => $row->descricao_es,
			'lingua' => '_ES',
			'ativo' => $row->ativo_es,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aacategoria/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo_es' => $this->input->post('titulo'),
				 'resumo_es' => $this->input->post('resumo'),
				 'descricao_es' => $this->input->post('descricao'),
				 'slug_es' => $this->slug($this->input->post('titulo')),
				 'ativo_es' => 'SIM',
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_es' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }

function EditaAacategoria_EN()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAacategoriaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => $row->titulo_en,
			'resumo' => $row->resumo_en,
			'descricao' => $row->descricao_en,
			'lingua' => '_EN',
			'ativo' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aacategoria/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo_en' => $this->input->post('titulo'),
				 'resumo_en' => $this->input->post('resumo'),
				 'descricao_en' => $this->input->post('descricao'),
				 'slug_en' => $this->slug($this->input->post('titulo')),
				 'ativo_en' => 'SIM',
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_en' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaAacategoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
 
   function AjaxOrdenar(){
     
     $menu = $this->input->post('menu');
    var_dump($menu);
    for ($i = 0; $i < count($menu); $i++) {
      $info = array(
           'id'  => $menu[$i],
                 'pos' => $i,
                 );
      $this->usuario->ModificaAacategoriaPeloID($info);
    }
  
 }



    function Ordenar(){
  $result = $this->usuario->GetAacategoria('');
   if($result)
   {
     $depoimento['tabela'] = "<div class='full_w'><ul id='sortme' style='width: 100%;'>";
     foreach($result as $row)
     {
      $depoimento['tabela'] .= "<li id='menu_".$row->id."' class='ui-state-default' style='cursor: pointer;margin-bottom: 5px;'>
      <img src='".base_url('public/img').'/'.$row->imagem2."' style='width:100%'>
<div class='legenda'>".$row->referencia."</div></li>";
     }
     $depoimento['tabela'] .= "</ul></div>";
   }
       
      $this->load->view('adm/aacategoria/ordena.php',$depoimento);
 }

 function index(){

	$this->load->view('adm/aacategoria/busca.php');
     
 }
}
?>
