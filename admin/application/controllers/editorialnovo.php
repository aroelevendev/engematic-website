<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EditorialNovo extends CI_Controller {

    
       public $data 	= 	array();
    
 function __construct()
 {
   parent::__construct();
 
}
    
 function index()
 {
   $this->load->library('../controllers/ckeditor');
   $this->load->helper(array('form'));
   $this->load->view('adm/editorial/novo',$this->ckeditor->index());
 }

}

?>
