<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EditorialInsere extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->load->helper('url');

    
 }

     private function upload_files($path, $files)
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png',
            'overwrite'     => 1,                       
        );

        $this->load->library('upload', $config);

        $images = array();
        
        $posicao=1;    
        foreach ($files['name'] as $key => $image) {
			
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = uniqid() .'_'. $image;

            $images[] = $fileName;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
				$info = array(
                 'codigo' => $this->usuario->GetLastID('editorial'),
                 'img' => $config['file_name'],
				 'posicao' => $posicao,
				 'tabela' => 'editorial',
				  );
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
            } else {
                //echo $path."<br>".$this->upload->display_errors();
                //return false;
            }
			$posicao++;
            
        }
		 //die();
        //return $images;
    }
 
 function index(){
     
     $this->form_validation->set_rules('titulo', 'Titulo do Editorial', 'trim|required');
     $this->form_validation->set_rules('resumo', 'Resumo do Editorial', 'trim|required');
     $this->form_validation->set_rules('conteudo', 'Conteudo do Editorial', 'trim|required');
     
     if($this->form_validation->run() == FALSE) {
         echo "<script>alert('Preencha corretamente todos os campos')</script>";
         $this->load->library('../controllers/ckeditor');
         $this->load->view('adm/editorial/novo.php',$this->ckeditor->index());
     }else{
		if($_FILES['images']['error'] === 4){ //se caso n�o for selecionado nenhum arquivo, faz o upload do default
			$imagempadrao = 1;
		}else{
		$imagempadrao = 0;
                    if ($this->upload_files('./public/img/', $_FILES['images']) === FALSE) {

                    }

 
		}
        
		if($imagempadrao == 1){ //gravar a imagem padr�o
		
		$info = array(
         'titulo' => $this->input->post('titulo'),
         'foto_titulo' => base_url('/public/img/titulo/default/default.jpg'),
         'data' => $this->input->post('data'),
         'resumo' => $this->input->post('resumo'),
         'conteudo' => $this->input->post('conteudo'),
         'privilegio' => intval($this->input->post('privilegio')),
		);
		}else{ //se n�o grava a imagem que foi setada
		//echo "<script>alert('Gravando no banco com imagem personalizada')</script>";
		$info = array(
         'titulo' => $this->input->post('titulo'),
         //'foto_titulo' => base_url($config['upload_path'].'/'.$tempdata['raw_name'].'_thumb'.$tempdata['file_ext']),
         'data' => $this->input->post('data'),
         'resumo' => $this->input->post('resumo'),
         'conteudo' => $this->input->post('conteudo'),
         'privilegio' => intval($this->input->post('privilegio')),
		);
		
		}
     $result = $this->usuario->InsereEditorial($info);
     if($result){
        echo "<script>alert('Editorial Salvo com Sucesso'); window.location = '".base_url('index.php/verificalogin')."';</script>";
     }else{
        echo "<script>alert('Erro ao Salvar o Editorial'); window.location = '".base_url('index.php/verificalogin')."';</script>";
     }
     $this->load->library('../controllers/ckeditor');
     $this->load->view('adm/editorial/novo.php',$this->ckeditor->index());
     }  
 }
}
?>
