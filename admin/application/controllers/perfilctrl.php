<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PerfilCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
    $this->load->library('form_validation');
    $this->load->helper('url');
    
 }
 
 function slug($string) {
	  $string = strtr(utf8_decode($string), 
        utf8_decode(
        'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
        'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
	  
	  $string = strtolower(trim($string));
	  $string = preg_replace("/[^a-z0-9-]/", "-", $string);
	  $string = preg_replace("/-+/", "-", $string);
	  if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
  		$string = substr($string, 0, strlen($string) - 1);
	  }
	  return $string;
}

 function salva(){
    $data = array(
         'nome' => $this->input->post('nome'),
         'profissao' => $this->input->post('profissao'),
         'nascimento' => $this->input->post('nascimento'),
         'cidade' => $this->input->post('cidade'),
         'estado' => $this->input->post('estado'),
		 'celular' => $this->input->post('celular'),
		 'email' => $this->input->post('email'),
		 'descricao' => $this->input->post('descricao'),
     );
        if($this->usuario->ModificaPerfil($data))
            echo "s";
        else
            echo "n";
}
     

 
 function index(){
  
   $result = $this->usuario->GetPerfil();
   $this->load->library('../controllers/ckeditor');
   if($result)
   {
        foreach($result as $row)
        {
            $info = array(
            'ckeditor'=>$this->ckeditor->index(),
            'nome' => $row->nome,
            'profissao' => $row->profissao,
            'nascimento' => $row->nascimento,
            'cidade' => $row->cidade,
            'estado' => $row->estado,
            'celular' => $row->celular,
            'email' => $row->email,
            'descricao' => $row->descricao,
            );
        }
       
   }
   $this->load->view('adm/perfil/perfil.php',$info);
 }
 }
?>
