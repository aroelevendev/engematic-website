<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class NoticiaCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= './public/img/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	
	switch(strtolower($_FILES[$key]['type']))
		{
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html': //html file
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
				break;
			default:
				die('Não tem Suporte para esse arquivo!'); 
	}
	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $this->slug($File_Name_No_Ext).'-'.$Random_Number.$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}	
	
	$config['image_library'] = 'gd2';
	$config['source_image'] = $UploadDirectory.$NewFileName;
	$config['new_image'] = $UploadDirectory.'thumb-'.$NewFileName;
	$config['maintain_ratio'] = TRUE;
	$config['width']    = 800;
	$config['height']    = 600;

	
	$this->image_lib->initialize($config); 
	if (!$this->image_lib->resize()) {
    	echo $this->image_lib->display_errors();
	}else{
	 	echo "s";
		unlink($UploadDirectory.$NewFileName);
	}
	$this->image_lib->clear();
	
}
}
function slug($string) {
	return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_Noticia))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetNoticia($palavrachave);
   if($result)
   {
     $noticia['tabela'] = "<table border='1' align='center'>
	 						<col width='5%' />
							<col width='5%' />
							<col width='40%' />							
							<col width='45%' />
							<col width='5%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
							  <th style='text-align: center;'>Idioma</th>
                              <th style='text-align: center;'>Noticia</th>
							  <th style='text-align: center;'>Resumo</th>
                              <th style='text-align: center;'>Ativo</th>							  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 if($row->ativo=='NAO')
		 	$background = "class='nativo'";
		 else
			$background = "";
			
	     if($row->idioma=="PT")
		   $flag = base_url('public/imagens/flag_br.png');
		 elseif($row->idioma=="EN")
		   $flag = base_url('public/imagens/flag_en.png');
		 else
		   $flag = base_url('public/imagens/flag_es.png');
			
       $noticia['tabela'] .= "<tr $background>".
	   				"<td align='center'><div style='height:20px; overflow:hidden'><a href=".base_url('index.php/noticiactrl/EditaNoticia/'.$row->id)."><i class='fa fa-pencil-square fa-lg falink'></i></a></div></td>".
                    "<td align='center'><div style='height:20px; overflow:hidden'><img src='".$flag."'></div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->titulo."</div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->resumo."</div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->ativo."</div></td>".					
                    "</tr>";
     }
     $noticia['tabela'] .= "</table>";
   }else {
       $noticia['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $noticia['privilegio'] = $privilegio;
        else
            $noticia['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/noticia/busca.php',$noticia);

        echo $noticia['tabela'];
     
 }
 function NovoNoticia()
 {
   if($this->input->post('titulo')){
   $this->load->helper(array('form'));
   date_default_timezone_set('America/Sao_Paulo');	
   $info = array(
                 'idioma' => $this->input->post('idioma'),
				 'titulo' => $this->input->post('titulo'),
				 'resumo' => $this->input->post('resumo'),
				 'descricao' => $this->input->post('descricao'),
				 'imagem' => $this->input->post('imagem1_input'),
				 'slug' => $this->slug($this->input->post('titulo')),
				 'ativo' => 'SIM',
				 'dt_hr' => date('Y-m-d H:i:s'),
                 );
   $result = $this->usuario->InsereNoticia($info);    
	
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/noticia/novo',$info);
   }
 }
 
  function EditaNoticia()
 {
	 
  if($this->uri->segment(3)){
   $result = $this->usuario->GetNoticiaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'idioma' => $row->idioma,
			'titulo' => $row->titulo,
			'resumo' => $row->resumo,
			'descricao' => $row->descricao,
			'imagem' => $row->imagem,
            'ativo' => $row->ativo,
            );
            
	   if($row->imagem != ""){
	   $caminho = "img/".$row->imagem;
	   $caminhoinput = $row->imagem;
	   $cover = "background-size: cover;";
	   $exclui_display = "block";
	   }else{
	   $caminho = "camera.png";
	   $caminhoinput = "";
	   $cover = "";
	   $exclui_display = "none";
	   }
	   
	   $info['input'] = "<input name='imagem1_input' id='imagem1_input' type='text' value='".$caminhoinput."' />";
       $info['div'] = "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
		background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
		cursor:pointer;border: 1px solid black;float: left;".$cover."' class='img_inner' 
		id='imagem1_preview'";
		$info['div'].= "onclick='document.getElementById(\"imagem1\").click()'>";
		$info['div'].= "<center>
						<label id='imagem1_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
		$info['div'].= "onClick='apaga(\"imagem1\",\"imagem1_preview\",\"imagem1_input\",\"imagem1_status\",\"imagem1_exclui\")'>";		
		$info['div'] .= "excluir</label>
						<div id='imagem1_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";
     
        }
       
   }

   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/noticia/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'idioma' => $this->input->post('idioma'),
                 'titulo' => $this->input->post('titulo'),
				 'resumo' => $this->input->post('resumo'),
				 'descricao' => $this->input->post('descricao'),
				 'imagem' => $this->input->post('imagem1_input'),
				 'slug' => $this->slug($this->input->post('titulo')),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->ModificaNoticiaPeloID($info);
   if($result)
        echo "s";
   else
        echo "n";
		
     				 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaNoticiaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
 
 function index(){

	$this->load->view('adm/noticia/busca.php');
     
 }
}
?>
