<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HistoriaCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= './public/img/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	
	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $this->slug($File_Name_No_Ext).'-'.$Random_Number.$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.'thumb-'.$NewFileName))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}	
	if($File_Ext<>"pdf"){
		$config['image_library'] = 'gd2';
		$config['source_image'] = $UploadDirectory.$NewFileName;
		$config['new_image'] = $UploadDirectory.'thumb-'.$NewFileName;
		$config['maintain_ratio'] = TRUE;
		//$config['width']    = 800;
		//$config['height']    = 600;

		
		$this->image_lib->initialize($config); 
		if (!$this->image_lib->resize()) {
	    	echo $this->image_lib->display_errors();
		}else{
		 	echo $config['new_image'];
			unlink($UploadDirectory.$NewFileName);
		}
		$this->image_lib->clear();
	}
	
}
}

function slug($string) {
	  return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_Historia))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetHistoria($palavrachave);
   if($result)
   {
     $historia['tabela'] = "<table border='1' align='center'>
	 						<col width='5%' />
	 						<col width='5%' />
							<col width='58%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
							  <th style='text-align: center;'>Idioma</th>
                              <th style='text-align: center;'>Título</th>
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 if($row->ativo=='NAO')
		 	$background = "class='nativo'";
		 else
			$background = "";

		if($row->idioma=="PT")
		   $flag = base_url('public/imagens/flag_br.png');
		 elseif($row->idioma=="EN")
		   $flag = base_url('public/imagens/flag_en.png');
		 else
		   $flag = base_url('public/imagens/flag_es.png');
			
       $historia['tabela'] .= "<tr $background>".
	   				"<td align='center'><div style='height:20px; overflow:hidden'><a href=".base_url('index.php/historiactrl/EditaHistoria/'.$row->id)."><i class='fa fa-pencil-square fa-lg falink'></i></a></div></td>".
	   				"<td align='center'><div style='height:20px; overflow:hidden'><img src='".$flag."'></div></td>".
                    "<td><div style='height:20px; overflow:hidden'>".$row->titulo."</div></td>".
                    "</tr>";
     }
     $historia['tabela'] .= "</table>";
   }else {
       $historia['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $historia['privilegio'] = $privilegio;
        else
            $historia['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/historia/busca.php',$historia);

        echo $historia['tabela'];
     
 }
 function NovoHistoria()
 {
   if($this->input->post('titulo')){
   $this->load->helper(array('form'));	
   $info = array(
                 'titulo' => $this->input->post('titulo'),
                 'idioma' => $this->input->post('idioma'),
				 'catalogo' => $this->input->post('catalogo_input'),
				 'resumo' => $this->input->post('resumo'),
				 'descricao' => $this->input->post('descricao'),
				 'slug' => $this->slug($this->input->post('titulo')),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->InsereHistoria($info);
    
	$cid = $this->usuario->GetLastID('historia');
    
	$data2['id_historia'] = $cid-1;
	for($id=1;$id<=$this->input->post('qtd_arq');$id++){
		$data2['caminho'] = $this->input->post('imagem'.$id.'_input');
    	$this->usuario->InsereFotoHistoria($data2);
		//echo "Data: ".$data2['id_historia'];
	}
	
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/historia/novo',$info);
   }
 }
 
  function EditaHistoria()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetHistoriaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'titulo' => $row->titulo,
            'idioma' => $row->idioma,
			'catalogo' => $row->catalogo,
			'resumo' => $row->resumo,
			'descricao' => $row->descricao,
            'ativo' => $row->ativo,
            );
            
        }
       
   }
   $result2 = $this->usuario->GetFotoHistoria($this->uri->segment(3));
   if($result2)
   {
     $i = 0;
	 $info['div'] = "";
	 $info['input'] = "";
     foreach($result2 as $row)
     {
	   $i++;
	   if($row->caminho != ""){
	   $caminho = "img/".$row->caminho;
	   $caminhoinput = $row->caminho;
	   $cover = "background-size: cover;";
	   $exclui_display = "block";
	   }else{
	   $caminho = "camera.png";
	   $caminhoinput = "";
	   $cover = "";
	   $exclui_display = "none";	   
	   }
	   $info['input'] .= "<input name='imagem".$i."_input' id='imagem".$i."_input' type='text' value='".$caminhoinput."' />";
       $info['div'] .= "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
		background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
		cursor:pointer;border: 1px solid black;float: left;margin-bottom:5px;margin-right:5px;".$cover."' class='img_inner' 
		id='imagem".$i."_preview'";
		$info['div'].= "onclick='document.getElementById(\"imagem$i\").click()'>";
		$info['div'].= "<center>
						<label id='imagem".$i."_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
		$info['div'].= "onClick='apaga(\"imagem$i\",\"imagem".$i."_preview\",\"imagem".$i."_input\",\"imagem".$i."_status\",\"imagem".$i."_exclui\")'>";		
		$info['div'] .= "excluir</label>
						<div id='imagem".$i."_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";
     }
   }
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/historia/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
                 'titulo' => $this->input->post('titulo'),
                 'idioma' => $this->input->post('idioma'),
				 'catalogo' => $this->input->post('catalogo_input'),
				 'resumo' => $this->input->post('resumo'),
				 'descricao' => $this->input->post('descricao'),
				 'slug' => $this->slug($this->input->post('titulo')),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->ModificaHistoriaPeloID($info);
   $data2['id_historia'] = $this->input->post('id');
   $this->usuario->DeletaFotoHistoria($this->input->post('id'));
	for($id=1;$id<=$this->input->post('qtd_arq');$id++){
		$data2['caminho'] = $this->input->post('imagem'.$id.'_input');
    	$result2 = $this->usuario->InsereFotoHistoria($data2);
	}
   if($result){
        echo "s";
     }else{
		if($result2)
			echo "s";
		else{
        	echo "n: ";
			echo "";
			var_dump($_POST);
		}
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaHistoriaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
 
 function index(){

	$this->load->view('adm/historia/busca.php');
     
 }
}
?>
