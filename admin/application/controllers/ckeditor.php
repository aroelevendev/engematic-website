<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CKeditor extends CI_Controller {

    
       public $data = array();
    
 function __construct()
 {
   parent::__construct();
   $this->load->helper('url');
   $this->load->helper('ckeditor');
 
}
    
 function UploadFoto()
	{
               


		$config['upload_path'] = './public/img/';
		$config['allowed_types'] = 'png|pdf|jpg|jpeg';
               // $config['file_name'] = 'carousel1.jpg';
              // $config['overwrite'] = 'TRUE';                
		//$config['max_size']	= '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';

		$this->load->library('upload', $config);
                
		if ( ! $this->upload->do_upload("upload"))
		{
                    echo "<script>alert('Erro no Upload: ".$this->upload->display_errors('','')."')</script>";
		}
		else
		{
                   $CKEditorFuncNum = $this->input->get('CKEditorFuncNum');

                    $data = $this->upload->data();            
                    $filename = $data['file_name'];
            
                    $url = base_url('/public/img/'.$filename);
            
                    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction('".$CKEditorFuncNum."', '".$url."', 'Upload Efetuado com sucesso')</script>";          
                        
                }
	}
 function index()
 {
   //Ckeditor's configuration
    $data['ckeditor'] = array(

            //ID of the textarea that will be replaced
            'id' 	=> 	'content',
            'path'	=>	'public/js/ckeditor',

            //Optionnal values
            'config' => array(
                    'toolbar' 	=> 	"Full", 	//Using the Full toolbar
                    'width' 	=> 	"750px",	//Setting a custom width
                    'height' 	=> 	'300px',	//Setting a custom height
                    'filebrowserUploadUrl' => base_url('index.php/ckeditor/UploadFoto')
            ),

            //Replacing styles from the "Styles tool"
            'styles' => array(

                    //Creating a new style named "style 1"
                    'style 1' => array (
                            'name' 		=> 	'Blue Title',
                            'element' 	=> 	'h2',
                            'styles' => array(
                                    'color' 	=> 	'Blue',
                                    'font-weight' 	=> 	'bold'
                            )
                    ),

                    //Creating a new style named "style 2"
                    'style 2' => array (
                            'name' 	=> 	'Red Title',
                            'element' 	=> 	'h2',
                            'styles' => array(
                                    'color' 		=> 	'Red',
                                    'font-weight' 		=> 	'bold',
                                    'text-decoration'	=> 	'underline'
                            )
                    )
            )
    );
	
	$data['ckeditor2'] = array(

            //ID of the textarea that will be replaced
            'id' 	=> 	'content2',
            'path'	=>	'public/js/ckeditor',

            //Optionnal values
            'config' => array(
                    'toolbar' 	=> 	"Full", 	//Using the Full toolbar
                    'width' 	=> 	"750px",	//Setting a custom width
                    'height' 	=> 	'300px',	//Setting a custom height
                    'filebrowserUploadUrl' => base_url('index.php/ckeditor/UploadFoto')
            ),

            //Replacing styles from the "Styles tool"
            'styles' => array(

                    //Creating a new style named "style 1"
                    'style 1' => array (
                            'name' 		=> 	'Blue Title',
                            'element' 	=> 	'h2',
                            'styles' => array(
                                    'color' 	=> 	'Blue',
                                    'font-weight' 	=> 	'bold'
                            )
                    ),

                    //Creating a new style named "style 2"
                    'style 2' => array (
                            'name' 	=> 	'Red Title',
                            'element' 	=> 	'h2',
                            'styles' => array(
                                    'color' 		=> 	'Red',
                                    'font-weight' 		=> 	'bold',
                                    'text-decoration'	=> 	'underline'
                            )
                    )
            )
    );

    return $data;
 }

}

?>
