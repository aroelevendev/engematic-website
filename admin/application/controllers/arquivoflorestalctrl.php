<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ArquivoFlorestaLCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= './public/img/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	

	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $this->slug($File_Name_No_Ext).$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}		
	
}
}
function slug($string) {
	return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_ArquivoFlorestaL))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada');
   
  
   $result = $this->usuario->GetArquivoFlorestaL($palavrachave);
   if($result)
   {
     $arquivoflorestal['tabela'] = "<table border='1' align='center'>
	 						<col width='5%' />
	 						<col width='5%' />
							<col width='45%' />
							<col width='45%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Idioma</th>
							  <th style='text-align: center;'>Produto</th>
                              <th style='text-align: center;'>Descrição</th>
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 if($row->ativo=='NAO')
		 	$background = "class='nativo'";
		 else
			$background = "";
			
       $arquivoflorestal['tabela'] .= "<tr $background>".
	   				"<td align='center'><div style='height:20px; overflow:hidden'><a href=".base_url('index.php/arquivoflorestalctrl/EditaArquivoFlorestaL/'.$row->id)."><i class='fa fa-pencil-square fa-lg falink'></i></a></div></td>".
	   				"<td><div style='height:20px; overflow:hidden'>".$row->idioma."</div></td>".
                    "<td><div style='height:20px; overflow:hidden'>".$row->aaproduto_referencia."</div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->descricao."</div></td>".
                    "</tr>";
     }
     $arquivoflorestal['tabela'] .= "</table>";
   }else {
       $arquivoflorestal['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $arquivoflorestal['privilegio'] = $privilegio;
        else
            $arquivoflorestal['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/arquivoflorestal/busca.php',$arquivoflorestal);

        echo $arquivoflorestal['tabela'];
     
 }
 function NovoArquivoFlorestaL()
 {
   if($this->input->post('descricao')){
   $this->load->helper(array('form'));	
   $info = array(
                 'produto' => $this->input->post('produto'),
				 'descricao' => $this->input->post('descricao'),
				 'idioma' => $this->input->post('idioma'),
				 'arquivo' => $this->input->post('imagem1_input'),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->InsereArquivoFlorestaL($info);    
	
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{

   	   $result = $this->usuario->GetAaproduto('');
	   if($result)
	   {
		 $info['produto'] = "<select name='produto' id='produto'>";
		 foreach($result as $row)
		 {

		   $info['produto'] .= 
						"<option value='".$row->id."'>".$row->referencia."</option>";
		   
		 }
		 $info['produto'] .= "</select>";
	   }
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/arquivoflorestal/novo',$info);
   }
 }
 
  function EditaArquivoFlorestaL()
 {
	 
  if($this->uri->segment(3)){
   $result = $this->usuario->GetArquivoFlorestaLPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'produto_id' => $row->produto,
            'idioma' => $row->idioma,
			'descricao' => $row->descricao,
			'imagem' => $row->arquivo,
            'ativo' => $row->ativo,
            );
            
	   if($row->arquivo != ""){
	   $caminho = "ok.png";
	   $caminhoinput = $row->arquivo;
	   $cover = "background-size: cover;";
	   $exclui_display = "block";
	   }else{
	   $caminho = "nuvem.png";
	   $caminhoinput = "";
	   $cover = "";
	   $exclui_display = "none";
	   }
	   
	   $info['input'] = "<input name='imagem1_input' id='imagem1_input' type='text' value='".$caminhoinput."' />";
       $info['div'] = "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
		background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
		cursor:pointer;border: 1px solid black;float: left;".$cover."' class='img_inner' 
		id='imagem1_preview'";
		$info['div'].= "onclick='document.getElementById(\"imagem1\").click()'>";
		$info['div'].= "<center>
						<label id='imagem1_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
		$info['div'].= "onClick='apaga(\"imagem1\",\"imagem1_preview\",\"imagem1_input\",\"imagem1_status\",\"imagem1_exclui\")'>";		
		$info['div'] .= "excluir</label>
						<div id='imagem1_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";
     
        }
       
   }
	
	$result = $this->usuario->GetAaproduto('');
	 if($result)
	 {
	   $info['produto'] = "<select name='produto' id='produto'>";
	   foreach($result as $row)
	   {
		 if($row->id == $info['produto_id']){
			 $info['produto'] .= 
					  "<option value='".$row->id."' selected>".$row->referencia."</option>";
		 }else{
		 $info['produto'] .= 
					  "<option value='".$row->id."'>".$row->referencia."</option>";
		 }
	   }
	   $info['produto'] .= "</select>";
	 }
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/arquivoflorestal/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'produto' => $this->input->post('produto'),
				 'descricao' => $this->input->post('descricao'),
				 'idioma' => $this->input->post('idioma'),
				 'arquivo' => $this->input->post('imagem1_input'),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->ModificaArquivoFlorestaLPeloID($info);
   if($result)
        echo "s";
   else
        echo "n";
		
     				 
   } 
   
 if($this->input->post('exclui')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 );
   $result = $this->usuario->ExcluiArquivoFlorestaLPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }
 }
 
 function index(){

	$this->load->view('adm/arquivoflorestal/busca.php');
     
 }
}
?>
