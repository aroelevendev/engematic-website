<script language="javascript">
function goBack(){
  window.history.back()
}
</script>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EditorialEditaExclui extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
    $this->load->library('form_validation');
    $this->load->helper('url');
    
 }
  function upload_files($path, $files)
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|gif|png',
            'overwrite'     => 1,                       
        );

        $this->load->library('upload', $config);

        $images = array();
        
        $posicao=1;    
        foreach ($files['name'] as $key => $image) {
			
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = uniqid() .'_'. $image;

            $images[] = $fileName;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
				$info = array(
                 'codigo' => $this->input->post('id'),
                 'img' => $config['file_name'],
				 'posicao' => $posicao,
				  );
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
            } else {
                //echo $path."<br>".$this->upload->display_errors();
                //return false;
            }
			$posicao++;
            
        }
		 //die();
        //return $images;
    }

 function excluiFoto(){	 
	 $this->usuario->ExcluiFotoPeloID($_POST["id_img"]);
 }
 function editaexclui(){
     
     $this->form_validation->set_rules('titulo', 'Titulo do Editorial', 'trim|required');
     $this->form_validation->set_rules('resumo', 'Resumo do Editorial', 'trim|required');
     $this->form_validation->set_rules('conteudo', 'Conteudo do Editorial', 'trim|required');
     
     if($this->form_validation->run() == FALSE) {
         echo "<script>alert('Preencha corretamente todos os campos')</script>";
         echo "<script>alert('Erro ao Modificar Editorial'); window.location = '".base_url('index.php/editorialbusca')."';</script>";
     }else{
	$this->upload_files('./public/img/', $_FILES['images']);
    $data = array(
         'id' => $this->input->post('id'),
         'titulo' => $this->input->post('titulo'),
         'data' => $this->input->post('data'),
         'resumo' => $this->input->post('resumo'),
         'conteudo' => $this->input->post('conteudo'),
         'privilegio' => intval($this->input->post('privilegio')),
     );
	 
    switch(strtolower($_POST['submit'])){
    case "excluir":
        if($this->usuario->ExcluiEditorialPeloID($data['id']))
            echo "<script>alert('Editorial Excluido com Sucesso'); window.location.href = '".base_url('index.php/verificalogin')."';</script>";
        else
            echo "<script>alert('Erro ao Excluir Editorial'); window.history.back();</script>";
    break;

    case "modificar":
        if($this->usuario->ModificaEditorialPeloID($data))
            echo "<script>alert('Editorial Modificado com Sucesso'); window.location = '".base_url('index.php/verificalogin')."';</script>";
        else
            echo "<script>alert('Erro ao Modificar Editorial'); window.location = '".base_url('index.php/verificalogin')."';</script>";
    break;
}
     }
     
 }
 
 function index(){
   if($this->uri->segment(3)){//gambito para não executar automaticamente o index
   $result = $this->usuario->GetEditorialPeloID($this->uri->segment(3));
   $this->load->library('../controllers/ckeditor');
   if($result)
   {
        foreach($result as $row)
        {
            $info = array(
            'ckeditor'=>$this->ckeditor->index(),
            'id' => $row->id,
            'titulo' => $row->titulo,
            'resumo' => $row->resumo,
            'conteudo' => $row->conteudo,
            'privilegio' => $row->privilegio
            );
            
        }
       
   }
   $result = $this->usuario->GetImgPeloCodigo($this->uri->segment(3));
    if($result)
   {
	   
        foreach($result as $row)
        {
			$info['img_nome'.$row->posicao] = $row->img;
			$info['img_id'.$row->posicao] = $row->id;
			$info['img_posicao'.$row->posicao] = $row->posicao;
					
		}
		
   }
   //var_dump($info);
   $this->load->view('adm/editorial/modifica.php',$info);
     
 }
 }
}
?>
