<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PRevendaCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
   // $this->load->library('../controllers/editorialnovo');
 }
 
 function autocomplete(){
    
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetPRevenda($palavrachave);
   if($result)
   {
     $prevenda['tabela'] = "<table border='1' align='center'>
	 						<col width='30%' />
							<col width='70%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Referencia</th>							  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 /*if($row->ativo_pt=='NAO')
		 	$background = "class='nativo'";
		 else
			$background = "";*/
			
       $prevenda['tabela'] .= "<tr>".
                    "<td align='center'>";
					
			if($row->nome_pt=="" || $row->nome_es=="" || $row->nome_en=="")
			$prevenda['tabela'] .= "<a href=".base_url('index.php/prevendactrl/EditaPRevenda_NOVA/'.$row->id).">
							<img src='".base_url('public/imagens/plus.png')."'>
						</a>";
			if($row->nome_pt!=""){
				if($row->ativo_pt=="SIM")
				 $flag = base_url('public/imagens/flag_br.png');
				else
				 $flag = base_url('public/imagens/flag_br_n.png');
				 
	  			$prevenda['tabela'] .= "<a href=".base_url('index.php/prevendactrl/EditaPRevenda_PT/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->nome_en!=""){
				if($row->ativo_en=="SIM")
				 $flag = base_url('public/imagens/flag_en.png');
				else
				 $flag = base_url('public/imagens/flag_en_n.png');
				 
	  			$prevenda['tabela'] .= "<a href=".base_url('index.php/prevendactrl/EditaPRevenda_EN/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->nome_es!=""){
				if($row->ativo_es=="SIM")
				 $flag = base_url('public/imagens/flag_es.png');
				else
				 $flag = base_url('public/imagens/flag_es_n.png');
				 
	  			$prevenda['tabela'] .= "<a href=".base_url('index.php/prevendactrl/EditaPRevenda_ES/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}
				
					
	   $prevenda['tabela'] .= "</td>";
	   
	    $prevenda['tabela'] .= "<td align='center'><a href=".base_url('index.php/prevendactrl/EditaPRevenda/'.$row->id).">".$row->referencia."</a></td>".				
                    "</tr>";
     }
     $prevenda['tabela'] .= "</table>";
   }else {
       $prevenda['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $prevenda['privilegio'] = $privilegio;
        else
            $prevenda['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/prevenda/busca.php',$prevenda);

        echo $prevenda['tabela'];
     
 }
 function NovoPRevenda()
 {
   if($this->input->post('referencia')){
   $this->load->helper(array('form'));	
   $info = array(
                 'referencia' => $this->input->post('referencia'),
                 );
   $result = $this->usuario->InserePRevenda($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->view('adm/prevenda/referencia.php');
   }
 }
 function EditaPRevenda()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetPRevendaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
            );
            
        }
       
   }   
   $this->load->view('adm/prevenda/referencia.php',$info);
 }
 if($this->input->post('referencia')){	 	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'referencia' => $this->input->post('referencia'),
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 

 }
 
 function EditaPRevenda_NOVA()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetPRevendaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'nome' => '',
			'lingua' => '_NOVA',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/prevenda/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'nome'.$this->input->post('idioma') => $this->input->post('nome'),
				 'ativo'.$this->input->post('idioma') => 'SIM',
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 }
 
 
  function EditaPRevenda_PT()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetPRevendaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'nome' => $row->nome_pt,
			'ativo' => $row->ativo_pt,
			'lingua' => '_PT',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/prevenda/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'nome_pt' => $this->input->post('nome'),
				 'ativo_pt' => 'SIM',
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_pt' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }

  function EditaPRevenda_EN()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetPRevendaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'nome' => $row->nome_en,
			'ativo' => $row->ativo_en,
			'lingua' => '_EN',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/prevenda/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'nome_en' => $this->input->post('nome'),
				 'ativo_en' => 'SIM',
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_en' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
}

function EditaPRevenda_ES()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetPRevendaPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'nome' => $row->nome_es,
			'ativo' => $row->ativo_es,
			'lingua' => '_ES',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/prevenda/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'nome_es' => $this->input->post('nome'),
				 'ativo_es' => 'SIM',
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_es' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaPRevendaPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
}
 
 function index(){

	$this->load->view('adm/prevenda/busca.php');
     
 }
}
?>
