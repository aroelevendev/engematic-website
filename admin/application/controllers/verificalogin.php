<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

class VerificaLogin extends CI_Controller {   
    
 function __construct()
 {
   parent::__construct();
   $this->load->model('usuario','',TRUE);
   $this->load->library('form_validation');
   $this->load->library('email');
 }
 
 function EsqueciSenha()
 {
   $username = $this->input->post('username');
   $novasenha=uniqid();
   //query the database
   $result = $this->usuario->GetLogin($username);
   if($result)
   {
    
     foreach($result as $row)
     {
     $data = array(
         'id' => $row->id,
         'senha' => md5($novasenha),
     );
   
       if($this->usuario->AlteraSenhaPeloID($data)){
      $config['wordwrap'] = TRUE;
      $config['mailtype'] = 'html';
	  $config['smtp_host'] = 'aroeleven.com.br';
	  $config['smtp_user'] = 'site@aroeleven.com.br';
	  $config['smtp_pass'] = 'aroeleven2016%';		
	  $config['smtp_port'] = '587';
	  $config['protocol'] = 'smtp';
    
        $this->email->initialize($config);
      $this->email->from('nova_senha@aroeleven.com.br','Nova Senha');
      $this->email->to($username);
      $this->email->mailtype = 'html';
      $this->email->subject('Nova Senha Painel ADM [ARO ELEVEN]');
      $this->email->message('Ola,<br>
                   Recebemos um pedido para gerar uma nova senha para você <br><br>
                   <strong>Nova Senha</strong>: '.$novasenha.''); 
    
      
      if ( ! $this->email->send())
      {
        echo $this->email->print_debugger();
      }else{
        $info['senha_msg'] = "Uma nova senha foi enviada para o seu Email (".$username.")"; 
      }
      
     }else{
       $info['senha_msg'] = "Ocorreu um erro durante a atualização, por favor tente novamente";
     }
       
     }
    
   }else{
      $info['senha_msg'] = "O usuario (".$username.") Não consta em nossa base de dados, entre em contato com o Administrador do Sistema";
   }
   
  $this->load->view('adm/login/index_view.php',$info);
 } 

 function index()
 {
	 
  if($this->input->post('esqueci')=='SIM')
  	$this->EsqueciSenha();
  else{		 
  //se caso o usuario ja estiver logado   
  if($this->session->userdata('logged_in')){

    //vai até a area adm
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = ucfirst($session_data['username']);
     $this->load->view('adm/menu/index_view.php',$data);
     return true;
 }elseif ($this->session->userdata('logged_out')) {
     redirect(base_url());
        }
   //This method will have the credentials validation
   

   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

   if($this->form_validation->run() == FALSE)
   {
     
    $this->load->view('adm/login/index_view.php');
   }
   else
   {
     //Go to private area
       $data_template = array(
            'username' => ucfirst($this->input->post('username')),
        );
     $this->load->view('adm/menu/index_view.php',$data_template);
   }
  }
 }

 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');

   //query the database
   $result = $this->usuario->login($username, $password);
   $result_provisorio = $this->usuario->login_provisorio($username, $password);
   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->login,
         'privilegio' => $row->privilegio,
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   elseif($result_provisorio)
   {
     
     $sess_array = array();
     foreach($result_provisorio as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->login,
         'privilegio' => 'alterar_senha',
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
   }else{  
     $this->form_validation->set_message('check_database', 'Usu&aacute;rio inv&aacute;lido');
     return false;
  
 }
 }
  function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect(base_url());
 }
}

?>