<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CertificadoCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= '../certificado/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	

	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $this->slug($File_Name_No_Ext).$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}		
	
}
}
function slug($string) {
	return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   =>'../certificado/',
            'allowed_types' => 'jpg|jpeg|pdf',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada');
   
  
   $result = $this->usuario->GetCertificado($palavrachave);
   if($result)
   {
     $certificado['tabela'] = "<table border='1' align='center'>
	 						<col width='5%' />
	 						<col width='5%' />
							<col width='45%' />
							<col width='45%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Idioma</th>
							  <th style='text-align: center;'>Descrição</th>
                              <th style='text-align: center;'>Arquivo</th>
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 if($row->ativo=='')
		 	$background = "class='nativo'";
		 else
			$background = "";
			
       $certificado['tabela'] .= "<tr $background>".
	   				"<td align='center'><div style='height:20px; overflow:hidden'><a href=".base_url('index.php/certificadoctrl/EditaCertificado/'.$row->id)."><i class='fa fa-pencil-square fa-lg falink'></i></a></div></td>".
	   				"<td><div style='height:20px; overflow:hidden'>".$row->idioma."</div></td>".
                    "<td><div style='height:20px; overflow:hidden'>".$row->descricao."</div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->arquivo."</div></td>".
										
                    "</tr>";
     }
     $certificado['tabela'] .= "</table>";
   }else {
       $certificado['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $certificado['privilegio'] = $privilegio;
        else
            $certificado['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/arquivo/busca.php',$arquivo);

        echo $certificado['tabela'];
     
 }
 function NovoCertificado()
 {
   if($this->input->post('imagem1_input')){

   $this->load->helper(array('form'));	
   $info = array(
         'descricao' => $this->input->post('descricao'),
         'arquivo' => $this->input->post('imagem1_input'),
         'idioma' => $this->input->post('idioma'),
         'ativo' => $this->input->post('ativo'),
         'dt_hr' => date('Y-m-d H:i:s'),
                 );
   $result = $this->usuario->InsereCertificado($info);    
	
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{

   	  
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/certificado/novo',$info);
   }
 }
 
  function EditaCertificado()
 {
	 
  if($this->uri->segment(3)){
   $result = $this->usuario->GetCertificadoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
            $info = array(
            'id' => $row->id,
			'descricao' => $row->descricao,
            'imagem' => $row->arquivo,
            'idioma' => $row->idioma,
            'ativo' => $row->ativo,
            );
            
	   if($row->arquivo != ""){
	   $caminho = "ok.png";
	   $caminhoinput = $row->arquivo;
	   $cover = "background-size: cover;";
	   $exclui_display = "block";
	   }else{
	   $caminho = "nuvem.png";
	   $caminhoinput = "";
	   $cover = "";
	   $exclui_display = "none";
	   }
	   
	   $info['input'] = "<input name='imagem1_input' id='imagem1_input' type='text' value='".$caminhoinput."' />";
       $info['div'] = "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
		background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
		cursor:pointer;border: 1px solid black;float: left;".$cover."' class='img_inner' 
		id='imagem1_preview'";
		$info['div'].= "onclick='document.getElementById(\"imagem1\").click()'>";
		$info['div'].= "<center>
						<label id='imagem1_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
		$info['div'].= "onClick='apaga(\"imagem1\",\"imagem1_preview\",\"imagem1_input\",\"imagem1_status\",\"imagem1_exclui\")'>";		
		$info['div'] .= "excluir</label>
						<div id='imagem1_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";
     
        }
       
   }
	
	
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/certificado/modifica.php',$info);
 }
 if($this->input->post('salvar') && $this->input->post('imagem1_input')){
  $posts = $this->input->post();
  $row_ativo = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
  
   $info = array(
   		'id'  => $this->input->post('id'),
		'descricao' => $this->input->post('descricao'),
        'arquivo' => $this->input->post('imagem1_input'),
        'idioma'=> $this->input->post('idioma'),
		'ativo' => $row_ativo,
    );
   $result = $this->usuario->ModificaCertificadoPeloID($info);
   if($result)
        echo "s";
   else
        echo "n";
		
     				 
   } 
   
 if($this->input->post('exclui')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 );
    $arquivo = $this->input->post('imagem1_input');
    $string = $_SERVER['DOCUMENT_ROOT'] . "/certificado/". $arquivo;
    unlink($string);
   $result = $this->usuario->ExcluiCertificadoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }
 }
 
 function index(){

	$this->load->view('adm/certificado/busca.php');
     
 }
}
?>