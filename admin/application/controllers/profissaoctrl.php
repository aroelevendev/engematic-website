<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProfissaoCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= './public/img/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	
	switch(strtolower($_FILES[$key]['type']))
		{
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html': //html file
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
				break;
			default:
				die('Não tem Suporte para esse arquivo!'); 
	}
	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $File_Name_No_Ext.'_'.$Random_Number.$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}	
	
	$config['image_library'] = 'gd2';
	$config['source_image'] = $UploadDirectory.$NewFileName;
	$config['new_image'] = $UploadDirectory.'thumb_'.$NewFileName;
	$config['maintain_ratio'] = TRUE;
	$config['width']    = 800;
	$config['height']    = 600;

	
	$this->image_lib->initialize($config); 
	if (!$this->image_lib->resize()) {
    	echo $this->image_lib->display_errors();
	}else{
	 	echo "s";
		unlink($UploadDirectory.$NewFileName);
	}
	$this->image_lib->clear();
	
}
}
function slug($string) {
	return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_Profissao))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetProfissao($palavrachave);
   if($result)
   {
     $profissao['tabela'] = "<table border='1' align='center'>
	 						<col width='5%' />
							<col width='90%' />
							<col width='5%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Nome</th>
                              <th style='text-align: center;'>Ativo</th>							  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 if($row->prf_ativa=='NAO')
		 	$background = "class='nativo'";
		 else
			$background = "";
			
       $profissao['tabela'] .= "<tr $background>".
	   				"<td align='center'><div style='height:20px; overflow:hidden'><a href=".base_url('index.php/profissaoctrl/EditaProfissao/'.$row->prf_codigo)."><i class='fa fa-pencil-square fa-lg falink'></i></a></div></td>".
                    "<td><div style='height:20px; overflow:hidden'>".$row->prf_nome."</div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->prf_ativa."</div></td>".					
                    "</tr>";
     }
     $profissao['tabela'] .= "</table>";
   }else {
       $profissao['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $profissao['privilegio'] = $privilegio;
        else
            $profissao['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/profissao/busca.php',$profissao);

        echo $profissao['tabela'];
     
 }
 function NovoProfissao()
 {
   if($this->input->post('prf_nome')){
   $this->load->helper(array('form'));	
   $info = array(
                 'prf_nome' => $this->input->post('prf_nome'),
				 'prf_ativa' => 'SIM',
                 );
   $result = $this->usuario->InsereProfissao($info);    
	
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/profissao/novo',$info);
   }
 }
 
  function EditaProfissao()
 {
	 
  if($this->uri->segment(3)){
   $result = $this->usuario->GetProfissaoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {			
			$info = array(
            'prf_codigo' => $row->prf_codigo,
            'prf_nome' => $row->prf_nome,
            'prf_ativa' => $row->prf_ativa,
            );                 
        }
       
   }

   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/profissao/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'prf_codigo'  => $this->input->post('prf_codigo'),
                 'prf_nome' => $this->input->post('prf_nome'),
				 'prf_ativa' => 'SIM',
                 );
   $result = $this->usuario->ModificaProfissaoPeloID($info);
   if($result)
        echo "s";
   else
        echo "n";
		
     				 
   } 
   
 if($this->input->post('prf_ativa')){
   $info = array(
   				 'prf_codigo' => $this->input->post('prf_codigo'),
                 'prf_ativa' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaProfissaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
 
 function index(){

	$this->load->view('adm/profissao/busca.php');
     
 }
}
?>
