<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AlteraSenha extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
    $this->load->library('form_validation');
   // $this->load->library('../controllers/editorialnovo');
 }
 
  function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
 
 function index(){
    
   $this->form_validation->set_rules('senha', 'Nova Senha', 'trim|required');
   if ($this->form_validation->run() == FALSE){
       $this->load->view('adm/login/altera_senha.php');
   }else{
   $session_data = $this->session->userdata('logged_in');
   $data = array(
         'id' => $session_data['id'],
         'senha' => md5($this->input->post('senha')),
         'senha_provisoria' => md5($this->randomPassword()),
     );
   
     if($this->usuario->AlteraSenhaPeloID($data))
            echo "<script>alert('Senha Alterada com Sucesso, POR FAVOR LOGUE NOVAMENTE'); window.location = '".base_url('index.php/verificalogin/logout')."';</script>";
     else
            echo "<script>alert('Erro ao Alterar a Senha'); window.location = '".base_url('index.php/verificalogin')."';</script>";
        
        $this->load->view('adm/login/altera_senha.php',$this);

   }
     
 }
 

}
?>
