<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bkpdb extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
   // $this->load->library('../controllers/editorialnovo');
 }
 

 function droptable(){
    $this->usuario->DropTables();
  	$this->usuario->RestoreTables();
	echo "s";
 }

 
 function index(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetAcademico(/*$privilegio,*/$palavrachave);
   if($result)
   {
     $treinamento['tabela'] = "<table border='1' align='center'>
                            <tr>
                              <th>Empresa</th>
                              <th>Curso</th>		
                              <th>Descricao</th>
							  <th>DT Entrada</th>
							  <th>DT Saida</th>
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
       $treinamento['tabela'] .= "<tr>".
                    "<td><a href=".base_url('index.php/academicactrl/EditaAcademico/'.$row->id).">".$row->empresa."</td>".
                    "<td>".$row->curso."</td>".
					"<td>".$row->descricao."</td>".
					"<td>".$row->data_entrada."</td>".
					"<td>".$row->data_saida."</td>".
                    "</tr>";
      /* $sess_array = array(
         'id' => $row->id,
         'titulo' => $row->titulo,
         'resumo' => $row->resumo,
        // 'privilegio' => $row->privilegio, por enquanto nao vai aparecer o 
       );*/
     }
     $treinamento['tabela'] .= "</table>";
   }else {
       $treinamento['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $treinamento['privilegio'] = $privilegio;
        else
            $treinamento['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/treinamento/busca.php',$treinamento);
			$this->load->view('adm/academico/busca.php');
        //echo $treinamento['tabela'];
     
 }
}
?>
