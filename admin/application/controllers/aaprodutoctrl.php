<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AaprodutoCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
	$this->load->library('image_lib');
 }
function processupload(){

	$UploadDirectory	= './public/img/';
	foreach ($_FILES as $key => $value) {
	
	
	if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
		die();
	}
	
	
	switch(strtolower($_FILES[$key]['type']))
		{
            case 'image/png':
			case 'image/bmp': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			case 'text/plain':
			case 'text/html': //html file
			case 'application/x-zip-compressed':
			case 'application/pdf':
			case 'application/msword':
			case 'application/vnd.ms-excel':
			case 'video/mp4':
				break;
			default:
				die('Não tem Suporte para esse arquivo!'); 
	}
	
	$File_Name          = strtolower($_FILES[$key]['name']);
	$parts = explode( ".", $_FILES[$key]['name'] );
	$File_Name_No_Ext = $parts[0];
	$File_Ext           = substr($File_Name, strrpos($File_Name, '.')); 
	$Random_Number      = $_POST['rand']; 
	$NewFileName 		= $File_Name_No_Ext.'_'.$Random_Number.$File_Ext; 
	
	
		
	 
	if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
	   {
		echo "s";
	}else{
		die('error durante o upload!');
	}	
	
	$config['image_library'] = 'gd2';
	$config['source_image'] = $UploadDirectory.$NewFileName;
	$config['new_image'] = $UploadDirectory.'thumb_'.$NewFileName;
	$config['maintain_ratio'] = TRUE;

	
	$this->image_lib->initialize($config); 
	if (!$this->image_lib->resize()) {
    	echo $this->image_lib->display_errors();
	}else{
	 	echo "s";
		unlink($UploadDirectory.$NewFileName);
	}
	$this->image_lib->clear();
	
}
}

function slug($string) {
	  return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}

function id_segmento_para_nome($id_segmento){

	
   $segmento_array = explode(',',$id_segmento);
   if($segmento_array<>""){
	$len = count($segmento_array);
	$segmento = "";
	$i = 0;
	foreach ($segmento_array as $selectedOption){
	  

		$result = $this->usuario->GetAacategoriaPeloID($selectedOption);
		   if($result)
		   {
			   
				
		        foreach($result as $row)
		        {
					
						
		            $info = array(
		            'id' => $row->id,
		            'referencia' => $row->referencia,
		            );
		            
		        }
		       
		   }

	  if ($i == $len - 1) {
		  
		   $segmento .= $info['referencia'];
	  }else{
		  $segmento .= $info['referencia']."<br>";
	  }

	  
	  $i++;
	}
   }else{
	 $segmento = "";  
   }

   return $segmento;
}
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_Aaproduto))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetAaproduto($palavrachave);
   if($result)
   {
     $produto['tabela'] = "<table border='1' align='center'>
	 						<col width='10%' />
							<col width='60%' />
							<col width='30%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Referencia</th>
							  <th style='text-align: center;'>Segmento</th>							  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {		 
			
       $produto['tabela'] .= "<tr>".
	   				"<td align='center'>";
					
					if($row->titulo_pt=="" || $row->titulo_es=="" || $row->titulo_en=="")
			$produto['tabela'] .= "<a href=".base_url('index.php/aaprodutoctrl/EditaAaproduto_NOVO/'.$row->id).">
							<img src='".base_url('public/imagens/plus.png')."'>
						</a>";
			if($row->titulo_pt!=""){
				if($row->ativo_pt=="SIM")
				 $flag = base_url('public/imagens/flag_br.png');
				else
				 $flag = base_url('public/imagens/flag_br_n.png');
				 
	  			$produto['tabela'] .= "<a href=".base_url('index.php/aaprodutoctrl/EditaAaproduto_PT/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->titulo_en!=""){
				if($row->ativo_en=="SIM")
				 $flag = base_url('public/imagens/flag_en.png');
				else
				 $flag = base_url('public/imagens/flag_en_n.png');
				 
	  			$produto['tabela'] .= "<a href=".base_url('index.php/aaprodutoctrl/EditaAaproduto_EN/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->titulo_es!=""){
				if($row->ativo_es=="SIM")
				 $flag = base_url('public/imagens/flag_es.png');
				else
				 $flag = base_url('public/imagens/flag_es_n.png');
				 
	  			$produto['tabela'] .= "<a href=".base_url('index.php/aaprodutoctrl/EditaAaproduto_ES/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}
				
					
	   $produto['tabela'] .= "</td>";
	   
	   $produto['tabela'] .= "<td align='center'><a href=".base_url('index.php/aaprodutoctrl/EditaReferenciaAaproduto/'.$row->id).">".$row->referencia."</a></td>".
	   "<td align='center'>".$this->id_segmento_para_nome($row->segmento)."</td>".				
                    "</tr>";
     }
     $produto['tabela'] .= "</table>";
   }else {
       $produto['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $produto['privilegio'] = $privilegio;
        else
            $produto['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/produto/busca.php',$produto);

        echo $produto['tabela'];
     
 }
 function NovoAaproduto()
 {
   if($this->input->post('referencia')){
   $this->load->helper(array('form'));	

   $segmento_array = $this->input->post('segmento');
   
   if($segmento_array<>""){
	$len = count($segmento_array);
	$segmento = "";
	$i = 0;
	foreach ($segmento_array as $selectedOption){
	  
	  if ($i == $len - 1) {
		  $segmento .= $selectedOption;
	  }else{
		  $segmento .= $selectedOption.",";
	  }

	  
	  $i++;
	}
   }else{
	 $segmento = "";  
   }
   
   $info = array(
                 'referencia' => $this->input->post('referencia'),
				 'segmento' => $segmento,
                 );
   $result = $this->usuario->InsereAaproduto($info);
    
	$cid = $this->usuario->GetLastID('aaproduto');
    
	$data2['id_aaproduto'] = $cid-1;
	for($id=1;$id<=$this->input->post('qtd_arq');$id++){
		$data2['caminho'] = $this->input->post('imagem'.$id.'_input');
    	$this->usuario->InsereFotoAaproduto($data2);
	}
	
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
	   
	   $result = $this->usuario->GetAacategoria('');
	   if($result)
	   {
		 $info['segmento'] = "<select name='segmento[]' id='segmento' size=6 multiple>";
		 foreach($result as $row)
		 {

		   $info['segmento'] .= 
						"<option value='".$row->id."'>".$row->referencia."</option>";
		   
		 }
		 $info['segmento'] .= "</select>";
	   }
	   
	   

	   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aaproduto/referencia_novo',$info);
   }
 }
 
  function EditaReferenciaAaproduto()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAaprodutoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'segmentoid' => $row->segmento,
            );
            
        }
       
   }
   $result2 = $this->usuario->GetFotoAaproduto($this->uri->segment(3));
   if($result2)
   {
     $i = 0;
	 $info['div'] = "";
	 $info['input'] = "";
     foreach($result2 as $row)
     {
	   $i++;
	   if($row->caminho != ""){
	   $caminho = "img/".$row->caminho;
	   $caminhoinput = $row->caminho;
	   $cover = "background-size: cover;";
	   $exclui_display = "block";
	   }else{
	   $caminho = "camera.png";
	   $caminhoinput = "";
	   $cover = "";
	   $exclui_display = "none";	   
	   }
	   $info['input'] .= "<input name='imagem".$i."_input' id='imagem".$i."_input' type='text' value='".$caminhoinput."' />";
       $info['div'] .= "<div style='background:url(".base_url('public/'.$caminho.'').") center center;
		background-size: auto 30px;width: 150px;height:150px;background-repeat: no-repeat;
		cursor:pointer;border: 1px solid black;float: left;margin-bottom:5px;margin-right:5px;".$cover."' class='img_inner' 
		id='imagem".$i."_preview'";
		$info['div'].= "onclick='document.getElementById(\"imagem$i\").click()'>";
		$info['div'].= "<center>
						<label id='imagem".$i."_exclui' style='z-index: 9999;color: white;background-color: rgba(0, 0, 0, 0.45);width: 100%;font-weight: 100;display:".$exclui_display."' ";
		$info['div'].= "onClick='apaga(\"imagem$i\",\"imagem".$i."_preview\",\"imagem".$i."_input\",\"imagem".$i."_status\",\"imagem".$i."_exclui\")'>";		
		$info['div'] .= "excluir</label>
						<div id='imagem".$i."_progressbar' style='background-color: #FD9534;height: 100%;width: 0%;float: left;'></div ></center></div>";
     }
   }
   $result = $this->usuario->GetAacategoria('');
   $segmento_array = explode(',',$info['segmentoid']);
   if($result)
   {
	 $info['segmento'] = "<select name='segmento[]' id='segmento' size=6 multiple>";
	 foreach($result as $row)
	 {
	   
	   	if (in_array($row->id, $segmento_array)) {
		   $info['segmento'] .= 
					"<option value='".$row->id."' selected>".$row->referencia."</option>";
	   }else{
	   $info['segmento'] .= 
					"<option value='".$row->id."'>".$row->referencia."</option>";
	   }
	 }
	 $info['segmento'] .= "</select>";
   }
   
   

   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aaproduto/referencia_modifica.php',$info);
 }
 if($this->input->post('salvar')){
   
   $segmento_array = $this->input->post('segmento');
   
   if($segmento_array<>""){
	$len = count($segmento_array);
	$segmento = "";
	$i = 0;
	foreach ($segmento_array as $selectedOption){
	  
	  if ($i == $len - 1) {
		  $segmento .= $selectedOption;
	  }else{
		  $segmento .= $selectedOption.",";
	  }

	  
	  $i++;
	}
   }else{
	 $segmento = "";  
   }
	
   $info = array(
   				 'id'  => $this->input->post('id'),
                 'referencia' => $this->input->post('referencia'),
				 'segmento' => $segmento,
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   $data2['id_aaproduto'] = $this->input->post('id');
   $this->usuario->DeletaFotoAaproduto($this->input->post('id'));
	for($id=1;$id<=$this->input->post('qtd_arq');$id++){
		$data2['caminho'] = $this->input->post('imagem'.$id.'_input');
    	$result2 = $this->usuario->InsereFotoAaproduto($data2);
	}
   if($result){
        echo "s";
     }else{
		if($result2)
			echo "s";
		else{
        	echo "n: ";
			echo "";
			var_dump($_POST);
		}
     }					 
   } 
   
	if($this->input->post('exclui')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 );
   $result = $this->usuario->ExcluiAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }


 }
 
 function EditaAaproduto_NOVO()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAaprodutoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => '',
			'descricao' => '',
			'especificacao_tecnica' => '',
			'resumo' => '',
			'lingua' => '_NOVO',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aaproduto/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo'.$this->input->post('idioma') => $this->input->post('titulo'),
				 'descricao'.$this->input->post('idioma') => $this->input->post('descricao'),
				 'especificacao_tecnica'.$this->input->post('idioma') => $this->input->post('especificacao_tecnica'),
				 'resumo'.$this->input->post('idioma') => $this->input->post('resumo'),				 
				 'slug'.$this->input->post('idioma') => $this->slug($this->input->post('titulo')),
				 'ativo'.$this->input->post('idioma') => 'SIM',
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 }
 
  function EditaAaproduto_PT()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAaprodutoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => $row->titulo_pt,
			'resumo' => $row->resumo_pt,
			'descricao' => $row->descricao_pt,
			'especificacao_tecnica' => $row->especificacao_tecnica_pt,
			'lingua' => '_PT',
			'ativo' => $row->ativo_pt,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aaproduto/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo_pt' => $this->input->post('titulo'),
				 'resumo_pt' => $this->input->post('resumo'),
				 'descricao_pt' => $this->input->post('descricao'),
				 'especificacao_tecnica_pt' => $this->input->post('especificacao_tecnica'),
				 'slug_pt' => $this->slug($this->input->post('titulo')),
				 'ativo_pt' => 'SIM',
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_pt' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
 
 function EditaAaproduto_ES()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAaprodutoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => $row->titulo_es,
			'resumo' => $row->resumo_es,
			'descricao' => $row->descricao_es,
			'especificacao_tecnica' => $row->especificacao_tecnica_es,
			'lingua' => '_ES',
			'ativo' => $row->ativo_es,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aaproduto/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo_es' => $this->input->post('titulo'),
				 'resumo_es' => $this->input->post('resumo'),
				 'descricao_es' => $this->input->post('descricao'),
				 'especificacao_tecnica_es' => $this->input->post('especificacao_tecnica'),
				 'slug_es' => $this->slug($this->input->post('titulo')),
				 'ativo_es' => 'SIM',
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_es' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }

function EditaAaproduto_EN()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetAaprodutoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'titulo' => $row->titulo_en,
			'resumo' => $row->resumo_en,
			'descricao' => $row->descricao_en,
			'especificacao_tecnica' => $row->especificacao_tecnica_en,
			'lingua' => '_EN',
			'ativo' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/aaproduto/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'titulo_en' => $this->input->post('titulo'),
				 'resumo_en' => $this->input->post('resumo'),
				 'descricao_en' => $this->input->post('descricao'),
				 'especificacao_tecnica_en' => $this->input->post('especificacao_tecnica'),
				 'slug_en' => $this->slug($this->input->post('titulo')),
				 'ativo_en' => 'SIM',
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_en' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaAaprodutoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }
    function AjaxOrdenar(){
	   
	   $menu = $this->input->post('menu');
		var_dump($menu);
		for ($i = 0; $i < count($menu); $i++) {
			$info = array(
   				 'id'  => $menu[$i],
                 'pos' => $i,
                 );
			$this->usuario->ModificaAaprodutoPeloID($info);
		}
	
 }



    function Ordenar(){
	$result = $this->usuario->GetAaprodutoComImagem('');
   if($result)
   {
     $depoimento['tabela'] = "<div class='full_w'><ul id='sortme' style='width: 100%;'>";
     foreach($result as $row)
     {
		  $depoimento['tabela'] .= "<li id='menu_".$row->id."' class='ui-state-default' style='cursor: pointer;margin-bottom: 5px;'>
		  <img src='".base_url('public/img').'/'.$row->caminho."' style='width:100%;max-height:150px'>
<div class='legenda'>".$row->referencia."</div></li>";
     }
     $depoimento['tabela'] .= "</ul></div>";
   }
       
			$this->load->view('adm/aaproduto/ordena.php',$depoimento);
 }
 function index(){

	$this->load->view('adm/aaproduto/busca.php');
     
 }
}
?>
