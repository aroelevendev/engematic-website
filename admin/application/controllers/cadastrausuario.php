<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CadastraUsuario extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
    $this->load->library('form_validation');
   // $this->load->library('../controllers/editorialnovo');
 }
 
 function index(){
    
   $this->form_validation->set_rules('senha', 'Senha', 'trim|required');
   $this->form_validation->set_rules('login', 'Senha', 'trim|required');
   if ($this->form_validation->run() == FALSE){
       $this->load->view('adm/login/cadastra_usuario.php');
   }else{
   $data = array(
         'senha_provisoria' => md5($this->input->post('senha')),
         'login' => $this->input->post('login'),
         'privilegio' => '1',
     );
   
     if($this->usuario->InsereLogin($data))
            echo "<script>alert('Usuario Cadastrado Corretamente')</script>";
     else
            echo "<script>alert('Erro ao Cadastrar Usuario')</script>";
        
        $this->load->view('adm/login/cadastra_usuario.php',$this);

   }
     
 }
 

}
?>
