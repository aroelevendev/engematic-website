<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CertificacaoCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
   // $this->load->library('../controllers/editorialnovo');
 }
 
 function autocomplete(){
    
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetCertificacao($palavrachave);
   if($result)
   {
     $certificacao['tabela'] = "<table border='1' align='center'>
	 						<col width='30%' />
							<col width='70%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                              <th style='text-align: center;'>Referencia</th>							  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {		
			
       $certificacao['tabela'] .= "<tr>".
                    "<td align='center'>";
					
			if($row->texto_pt=="" || $row->texto_es=="" || $row->texto_en=="")
			$certificacao['tabela'] .= "<a href=".base_url('index.php/certificacaoctrl/EditaCertificacao_NOVA/'.$row->id).">
							<img src='".base_url('public/imagens/plus.png')."'>
						</a>";
			if($row->texto_pt!=""){
				if($row->ativo_pt=="SIM")
				 $flag = base_url('public/imagens/flag_br.png');
				else
				 $flag = base_url('public/imagens/flag_br_n.png');
				 
	  			$certificacao['tabela'] .= "<a href=".base_url('index.php/certificacaoctrl/EditaCertificacao_PT/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->texto_en!=""){
				if($row->ativo_en=="SIM")
				 $flag = base_url('public/imagens/flag_en.png');
				else
				 $flag = base_url('public/imagens/flag_en_n.png');
				 
	  			$certificacao['tabela'] .= "<a href=".base_url('index.php/certificacaoctrl/EditaCertificacao_EN/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}if($row->texto_es!=""){
				if($row->ativo_es=="SIM")
				 $flag = base_url('public/imagens/flag_es.png');
				else
				 $flag = base_url('public/imagens/flag_es_n.png');
				 
	  			$certificacao['tabela'] .= "<a href=".base_url('index.php/certificacaoctrl/EditaCertificacao_ES/'.$row->id).">
							   			 <img src='".$flag."'>
										</a>";
			}
				
					
	   $certificacao['tabela'] .= "</td>";
	   
	    $certificacao['tabela'] .= "<td align='center'>".$row->referencia."</td>".				
                    "</tr>";
     }
     $certificacao['tabela'] .= "</table>";
   }else {
       $certificacao['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $certificacao['privilegio'] = $privilegio;
        else
            $certificacao['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/certificacao/busca.php',$certificacao);

        echo $certificacao['tabela'];
     
 }
 function NovoCertificacao()
 {
   if($this->input->post('referencia')){
   $this->load->helper(array('form'));	
   $info = array(
                 'referencia' => $this->input->post('referencia'),
                 );
   $result = $this->usuario->InsereCertificacao($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->view('adm/certificacao/referencia.php');
   }
 }
 function EditaCertificacao()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetCertificacaoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
            );
            
        }
       
   }   
   $this->load->view('adm/certificacao/referencia.php',$info);
 }
 if($this->input->post('referencia')){	 	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'referencia' => $this->input->post('referencia'),
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 

 }
 
 function EditaCertificacao_NOVA()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetCertificacaoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'texto' => '',
			'lingua' => '_NOVA',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/certificacao/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'texto'.$this->input->post('idioma') => $this->input->post('texto'),
				 'ativo'.$this->input->post('idioma') => 'SIM',
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 }
 
 
  function EditaCertificacao_PT()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetCertificacaoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'texto' => $row->texto_pt,
			'ativo' => $row->ativo_pt,
			'lingua' => '_PT',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/certificacao/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'texto_pt' => $this->input->post('texto'),
				 'ativo_pt' => 'SIM',
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_pt' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
 }

  function EditaCertificacao_EN()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetCertificacaoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'texto' => $row->texto_en,
			'ativo' => $row->ativo_en,
			'lingua' => '_EN',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/certificacao/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'texto_en' => $this->input->post('texto'),
				 'ativo_en' => 'SIM',
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_en' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
}

function EditaCertificacao_ES()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetCertificacaoPeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'referencia' => $row->referencia,
			'texto' => $row->texto_es,
			'ativo' => $row->ativo_es,
			'lingua' => '_ES',
			'ativo_pt' => $row->ativo_pt,
			'ativo_es' => $row->ativo_es,
			'ativo_en' => $row->ativo_en,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/certificacao/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
				 'texto_es' => $this->input->post('texto'),
				 'ativo_es' => 'SIM',
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
 if($this->input->post('ativo')){
   $info = array(
   				 'id' => $this->input->post('id'),
                 'ativo_es' => $this->input->post('status'),
                 );
   $result = $this->usuario->ModificaCertificacaoPeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
}
 
 function index(){

	$this->load->view('adm/certificacao/busca.php');
     
 }
}
?>
