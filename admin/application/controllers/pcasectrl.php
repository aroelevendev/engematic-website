<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PcaseCtrl extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
   // $this->load->library('../controllers/editorialnovo');
 }
 
 function get_files()
 {
  if (!empty($_FILES)) {
		 $config = array(
            'upload_path'   => './public/img/',
            'allowed_types' => 'jpg|gif|png|jpeg|rar|zip',
            'overwrite'     => 1,
			'file_name'     => uniqid()                  
        );
		$this->load->library('upload', $config);
		foreach ($_FILES as $key => $value) {
			
            if (!empty($value['tmp_name'])) {
				
				//$ext = $this->upload->data();
                if ( ! $this->upload->do_upload($key)) {
                    $error = array('error' => $this->upload->display_errors());
                    //failed display the errors
					echo $error['error'];
                } else {
					$ext = $this->upload->data();
					$codigo = $this->input->post('codigo');
					$tabela = $this->input->post('tabela');
					//$posicao = intval($this->usuario->GetLastPosicaoImg($codigo_pcase))+1;
                    $info = array(
					   'codigo' => $codigo,
					   'img' => $config['file_name'].$ext['file_ext'],
					   'size' => filesize($value['tmp_name']),
					   'tabela' => $tabela,
					   'posicao' => '1',
					);
				 // var_dump($config['file_name']);
				  $this->usuario->InsereImg($info);
				  echo $config['file_name'].$ext['file_ext'];
                }

            }
        }
	}else{
   $result = $this->usuario->GetImgPerfil();
   $img  = array();
   if($result)
   {
     
     foreach($result as $row)
     {
		  $obj['name'] = $row->img;
		  $obj['size'] = $row->size;
		  $img[] = $obj;

	 }
   }
    header('Content-type: text/json');              
    header('Content-type: application/json');
    echo json_encode($img);
	}
 }
 function autocomplete(){
    
   // $privilegio = $this->input->post('privilegio'); por enquanto nao vai ter busca por categoria
   $palavrachave = $this->input->post('palavrachave');
   $primeiraentrada = $this->input->post('primeiraentrada'); //variavel que verifica se esta entrando na tela ou ja buscando
   
  
   $result = $this->usuario->GetPcase($palavrachave);
   if($result)
   {
     $pcase['tabela'] = "<table border='1' align='center'>
	 						<col width='5%' />
              <col width='5%' />
							<col width='40%' />
							<col width='50%' />
                            <tr>
							  <th style='text-align: center;'>Editar</th>
                <th style='text-align: center;'>Idioma</th>
                              <th style='text-align: center;'>Título</th>
							  <th style='text-align: center;'>Texto</th>						  
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
		 if($row->ativo=='NAO')
		 	$background = "class='nativo'";
		 else
			$background = "";

        if($row->idioma=="PT")
       $flag = base_url('public/imagens/flag_br.png');
     elseif($row->idioma=="EN")
       $flag = base_url('public/imagens/flag_en.png');
     else
       $flag = base_url('public/imagens/flag_es.png');
			
       $pcase['tabela'] .= "<tr $background>".
                    "<td align='center'><div style='height:20px; overflow:hidden'><a href=".base_url('index.php/pcasectrl/EditaPcase/'.$row->id)."><i class='fa fa-pencil-square fa-lg falink'></i></a></div></td>".
          "<td align='center'><div style='height:20px; overflow:hidden'><img src='".$flag."'></div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->titulo."</div></td>".
					"<td><div style='height:20px; overflow:hidden'>".$row->descricao."</div></td>".					
                    "</tr>";
     }
     $pcase['tabela'] .= "</table>";
   }else {
       $pcase['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $pcase['privilegio'] = $privilegio;
        else
            $pcase['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //$this->load->view('adm/pcase/busca.php',$pcase);

        echo $pcase['tabela'];
     
 }
 function NovoPcase()
 {
   if($this->input->post('titulo')){
   $this->load->helper(array('form'));	
   $info = array(
                 'titulo' => $this->input->post('titulo'),
                 'idioma' => $this->input->post('idioma'),
				 'descricao' => $this->input->post('descricao'),
				 'video' => $this->input->post('video'),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->InserePcase($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   }else{
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/pcase/novo',$info);
   }
 }
 
  function EditaPcase()
 {
  if($this->uri->segment(3)){
   $result = $this->usuario->GetPcasePeloID($this->uri->segment(3));
   if($result)
   {
	   
		
        foreach($result as $row)
        {
			
				
            $info = array(
            'id' => $row->id,
            'titulo' => $row->titulo,
            'idioma' => $row->idioma,
			'descricao' => $row->descricao,
			'video' => $row->video,
            'ativo' => $row->ativo,
            );
            
        }
       
   }
   
   $this->load->library('../controllers/ckeditor');
   $info['ckeditor']=$this->ckeditor->index();
   $this->load->view('adm/pcase/modifica.php',$info);
 }
 if($this->input->post('salvar')){
	 
	
   $info = array(
   				 'id'  => $this->input->post('id'),
                 'titulo' => $this->input->post('titulo'),
                 'idioma' => $this->input->post('idioma'),
				 'descricao' => $this->input->post('descricao'),
				 'video' => $this->input->post('video'),
				 'ativo' => 'SIM',
                 );
   $result = $this->usuario->ModificaPcasePeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }					 
   } 
   
     if($this->input->post('exclui')){
   $info = array(
           'id' => $this->input->post('id'),
                 );
   $result = $this->usuario->ExcluiPcasePeloID($info);
   if($result){
        echo "s";
     }else{
        echo "n";
     }           
   }
 }
 
 function index(){

	$this->load->view('adm/pcase/busca.php');
     
 }
}
?>
