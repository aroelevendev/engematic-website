<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EditorialBusca extends CI_Controller {
function __construct()
 {
   parent::__construct();
    $this->load->model('usuario','',TRUE);
    $this->load->helper(array('form'));
   // $this->load->library('../controllers/editorialnovo');
 }
 public function autocomplete() {
        $palavrachave = $this->input->post('palavrachave');
		$privilegio = $this->input->post('privilegio');
		//echo "Privilegio: ".$privilegio."</br>";
		//echo "palavrachave: ".$palavrachave."</br>";
		if(	$palavrachave != "" ){	
        $query = $this->usuario->GetEditorial($privilegio,$palavrachave);
		if($query){
        $editorial['tabela'] = "<table border='1' align='center'>
                            <tr>
                              <th>Titulo</th>
                              <th>Resumo</th>		
                              <th>Data</th>
                            </tr>";
     $sess_array = array();
     foreach($query as $row)
     {
       $editorial['tabela'] .= "<tr>".
                    "<td><a href=".base_url('index.php/editorialeditaexclui/index/'.$row->id).">".$row->titulo."</td>".
                    "<td>".$row->resumo."</td>".
                    "<td>".substr($row->data,0,10)."</td>".
                    "</tr>";
       $sess_array = array(
         'id' => $row->id,
         'titulo' => $row->titulo,
         'resumo' => $row->resumo,
         'privilegio' => $row->privilegio,
       );
     }
     $editorial['tabela'] .= "</table>";
   }else {
       $editorial['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
    echo $editorial['tabela'];
		}
		
 }
 
 function index(){
   $privilegio = $this->input->post('privilegio');
   $palavrachave = $this->input->post('palavrachave');
   echo ($palavrachave)."</br>";
   echo ($privilegio)."</br>";
   $result = $this->usuario->GetEditorial($privilegio,$palavrachave);
   if(!empty($privilegio)){
   if($result)
   {
     $editorial['tabela'] = "<table border='1' align='center'>
                            <tr>
                              <th>Titulo</th>
                              <th>Resumo</th>		
                              <th>Data</th>
                            </tr>";
     $sess_array = array();
     foreach($result as $row)
     {
       $editorial['tabela'] .= "<tr>".
                    "<td><a href=".base_url('index.php/editorialeditaexclui/index/'.$row->id).">".$row->titulo."</td>".
                    "<td>".$row->resumo."</td>".
                    "<td>".substr($row->data,0,10)."</td>".
                    "</tr>";
       $sess_array = array(
         'id' => $row->id,
         'titulo' => $row->titulo,
         'resumo' => $row->resumo,
         'privilegio' => $row->privilegio,
       );
     }
     $editorial['tabela'] .= "</table>";
   }else {
       $editorial['tabela'] = "<h2 align='center'>nenhuma busca foi encontrada </h2>";
   }
        if(!empty($privilegio)) //passar o privilegio atual para continuar selecionado depois de buscar
            $editorial['privilegio'] = $privilegio;
        else
            $editorial['privilegio'] = 1; //se não tiver passado, atribui um valor qualquer.
        //echo json_encode ($editorial);
		$this->load->view('adm/editorial/busca.php',$editorial);
	   
   }else{
        //echo json_encode ($editorial);
	   $this->load->view('adm/editorial/busca.php');
	 
   } 
        
     
 }
}
?>
