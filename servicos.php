<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Meta -->
    <meta name="description" content="">
	<meta name="keywords" content="" />
	
    <title>Serviços | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />
	<link href="assets/css/quotes.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php $menu="servicos";
$link_en = "/en/services";
$link_es = "/es/servicios";

include("include/header.php"); 
include("include/conexao.php"); ?>


      <!-- Page Title -->
      <section class="page-title servico_topo">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
			  <h2>Serviços</h2>
              <ol class="breadcrumb">
				<li><a class="pathway" href="index.php">Home</a></li>
				<li class="active">Serviços</li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-8 col-md-8">
			            <?php 
          $sql_servico="select * from servico where idioma='PT'";
          $result_servico=mysqli_query($con,$sql_servico);
          $i = 0;
          while($row_servico = mysqli_fetch_array($result_servico)){ ?>
            <div class="module title3">
              <h3 class="module-title"><?=$row_servico['titulo']?></h3>
              <div class="module-content">
                      <br>
              <div class="uk-grid uk-margin-bottom">
                        <div class="uk-width-medium-1-3">
                          <?php 
                          $sql_fotos = "select * from foto_servico where caminho<>'' and id_servico='".$row_servico['id']."'";
                          $result_fotos=mysqli_query($con,$sql_fotos);
                          $i=0;
                          while($row_fotos = mysqli_fetch_array($result_fotos)){
                            if(++$i>1)
                              $display = 'display:none';
                            else
                              $display = '';

                          ?>
                          <a href="admin/public/img/<?=$row_fotos['caminho']?>" data-uk-lightbox="{group:'<?=$row_servico['id']?>'}" title="<?=$row_servico['titulo']?>" style="<?=$display?>">
                              <img src="admin/public/img/<?=$row_fotos['caminho']?>">
                          </a>
                          <?php } ?>
                        </div>
                        <div class="uk-width-medium-2-3" style="text-align: justify;">
                        <p><?=$row_servico['resumo']?></p>
                        </div>
                      </div>
                    </div>
            </div>
          <?php } ?>



			</div>

      <div class="col-sm-4 col-md-4">
          <?php
          $sql_arquivo="select * from arquivo a where a.idioma = 'PT' and a.pag_servico=1 and a.arquivo <>'' ";
          $result_arquivo=mysqli_query($con,$sql_arquivo);
          $i = 0;
          while($row_arquivo = mysqli_fetch_array($result_arquivo)){ ?>
              <div class="bt_10">
                  <a href="/admin/public/img/<?=$row_arquivo['arquivo']?>" target="_blank"><button class="uk-button uk-button-small" type="button" style="padding: 5px 20px;width: 100%;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;<?=$row_arquivo['descricao']?></button></a>
              </div>
          <?php } ?>
      </div>
		  </div>
		</div>
	  </section>	  
	  <!-- /Top A -->

	  
<?php include("include/footer.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	
	
	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
