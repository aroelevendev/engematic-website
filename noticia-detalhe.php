<?php include("include/conexao.php"); 

$sql_segmento="select * from noticia where ativo = 'SIM' and idioma='PT' and slug = '".$_GET['id']."'";
$result_segmento=mysqli_query($con,$sql_segmento);
$row_segmento = mysqli_fetch_array($result_segmento);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<base href="/">
	<!-- Meta -->
    <meta name="description" content="<?=$row_segmento['resumo_pt']?>">
	
    <title><?=$row_segmento['titulo']?> | Enginstrel Engematic</title>

    <link href="/ee.png"  rel="icon" sizes="32x32">
	
	<!-- Styles -->			
    <!-- Uikit CSS -->
    <link href="assets/css/uikit.min.css" rel="stylesheet">	
	<link href="assets/css/progress.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slidenav.almost-flat.css" rel="stylesheet">
    <link href="assets/css/slideshow.almost-flat.css" rel="stylesheet">
    <link href="assets/css/sticky.almost-flat.css" rel="stylesheet">
    <link href="assets/css/tooltip.almost-flat.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">	
    <!-- Animate CSS -->
	<link href="assets/css/animate.css" rel="stylesheet" />	
    <!-- Sprocket CSS -->
	<link href="assets/css/product.css" rel="stylesheet" />
	<link href="assets/css/strips.css" rel="stylesheet" />
	<link href="assets/css/quotes.css" rel="stylesheet" />	
    <!-- Font Awesome -->	
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Pe-icon-7-stroke Fonts -->	
    <link href="assets/css/helper.css" rel="stylesheet">	
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet">	
    <!-- Template CSS -->
    <link href="assets/css/template.css" rel="stylesheet">	
    <link href="assets/color/color1.css" rel="stylesheet" type="text/css" title="color1">		
    <link href="assets/css/magnific-popup.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->	
	
  </head>

  <body>
    <!-- Wrap all page content -->
    <div class="body-innerwrapper" id="page-top">
<?php $menu="noticias";include("include/header.php"); ?>

<style type="text/css">
em, b, strong{color: inherit;}
  tr:nth-child(even) {
             background-color: rgb(244, 244, 244);
            }

            tr:nth-child(1){
             background-color:rgb(231, 230, 230);
            }
            table,th,td {
            border: 1px solid #f2f2f2;
            padding: 10px;
            }
}
</style>
      <!-- Page Title -->
      <section  style="background-color: #f5f5f5;">
		<div class="container">	  
	      <div class="row">
		    <div class="col-sm-12 col-md-12 title">
              <ol class="breadcrumb" style="padding: 20px 20px 20px 0px; margin-bottom: 0px;">
				<li><a class="pathway" href="index.php">Home</a></li>
        <li><a class="pathway" href="noticias.php">Notícias</a></li>
				<li class="active"><?=$row_segmento['titulo']?></li>				  
		      </ol>
			</div>
		  </div>
		</div>
	  </section>
	  <!-- /Page Title -->
	  
	  <!-- Top A -->
      <section class="top-a">
        <div class="container">
		  <div class="row">
		    <div class="col-sm-9 col-md-9">

        <div class="module title3">
          
          <div class="module-content missao_visao_valores">
          <div style="background: url(&quot;admin/public/img/<?=$row_segmento['imagem']?>&quot;) center center;background-size: cover;width: 100%;height: 230px;background-repeat: no-repeat;margin-bottom: 20px;" class="borda-evento-foto"> </div>

                  <h3 class="module-title" style="display: inline-block;"><?=$row_segmento['titulo']?></h3>
                  <br><br>
          <?=$row_segmento['descricao']?>
                </div>
        </div>


			</div>
      <div class="col-sm-3 col-md-3 right side_produto">

        <div class="module _menu">
        <h3>Outras Notícias</h3>
        <ul class="nav menu">
        <?php 
        $sql_outros="select * from noticia where ativo = 'SIM' and idioma='PT' and slug <> '".$_GET['id']."' order by dt_hr limit 5";
        $result_outros=mysqli_query($con,$sql_outros);
        $i = 0;
        while($row_outros = mysqli_fetch_array($result_outros)){
        ?>
        <li><a href="noticias/<?=$row_outros['slug']?>"><?=$row_outros['titulo']?></a></li>
        <?php } ?>
        </ul>
        </div>

        
        </div>
		  </div>
		</div>
	  </section>	  
	  <!-- /Top A -->

	  
<?php include("include/footer.php"); ?>

    <!-- Scripts placed at the end of the document so the pages load faster -->
	
    <!-- Jquery scripts -->
    <script src="assets/js/jquery.min.js"></script>
	
    <!-- Uikit scripts -->
	<script src="assets/js/uikit.min.js"></script>	
	<script src="assets/js/slideshow.min.js"></script> 
	<script src="assets/js/slideshow-fx.min.js"></script> 
	<script src="assets/js/slideset.min.js"></script> 	
	<script src="assets/js/sticky.min.js"></script>
	<script src="assets/js/tooltip.min.js"></script>	
	<script src="assets/js/parallax.min.js"></script>
	<script src="assets/js/lightbox.min.js"></script>
	<script src="assets/js/grid.min.js"></script>
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<!-- WOW scripts -->
	<script src="assets/js/wow.min.js"></script>
    <script> new WOW().init(); </script>

    <!-- Оffcanvas Мenu scripts -->
	<script src="assets/js/offcanvas-menu.js"></script> 	
	
    <!-- Template scripts -->
	<script src="assets/js/template.js"></script> 	

	<!-- Bootstrap core JavaScript -->
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>


  </body>
</html>
